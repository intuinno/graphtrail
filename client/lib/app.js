angular.module('graphtrailApp', [
    'angular-meteor',
    'ui.router',
    'ui.bootstrap',
    'angularUtils.directives.dirPagination',
    'cgBusy',
    'ui.slider',
    'gridster',
    'ui.tree',
    'hc.marked',
    'cfp.hotkeys',
    'jsonFormatter',
    'ngFileUpload'

]).config(['markedProvider', function(markedProvider) {
    markedProvider.setOptions({
        gfm: true,
        tables: true,
        breakes: true
    });
}]).run(["$rootScope", "$location", "$window", function($rootScope, $state, $window) {
    $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
        // We can catch the error thrown when the $requireUser promise is rejected
        // and redirect the user back to the main page
        if (error === "AUTH_REQUIRED") {
            //$state.go("/loginrequired");
            $window.location.href = "/loginrequired";
        }
    });
}]);




onReady = function () {
    angular.bootstrap(document, ['graphtrailApp']);
};

if (Meteor.isCordova) {
    angular.element(document).on('deviceready', onReady);
} else {
    angular.element(document).ready(onReady);
}