Meteor.utilitiesHelper = {
    getEntityById : (entities, id) => {
        if (entities && (entities.length > 0)) {
            return entities.find((d) => {
                return d._id === id;
            });
        }

        return false;
    },
    /*
    * Return a list of documents given a list of document ids and an array of entity data
    * */
    fromIdListToObjectList : (idList, entityData) => {
        // create a hashmap of the entity data for faster search
        var entityDataHash = {};
        var result = [];

        if (entityData && entityData.length > 0) {
            entityData.forEach((doc) => {
                entityDataHash[doc._id] = angular.copy(doc);
            });

            if (entityDataHash) {
                result = idList.map((id) => { return entityDataHash[id]; });
            }
        }

        return result;
    },
    // check if all the fields in an object are true
    checkAllProp : (obj) => {
        // if not empty
        if (!obj) {
            return false;
        }

        if (Object.keys(obj).length === 0 && obj.constructor === Object) {
            return false;
        }

        for (let key in obj) {
            if (!obj[key]) {
                return false;
            }
        }

        return true;
    },
    createBrokenCard : (state, type) => {
        let stateMissingData = {};

        if ((type === 'trail') || (type === 'dashboard')) {
            stateMissingData.id = state.id;
            stateMissingData.name = state.name;
            stateMissingData.sources = state.sources;
            stateMissingData.targets = state.targets;
            stateMissingData.x = state.x;
            stateMissingData.y = state.y;
            stateMissingData.type = state.type;

            if (type === 'dashboard') {
                stateMissingData.gridster = state.gridster;
            }
        }

        return stateMissingData;
    },
    // truncate text that exceeds length max
    truncSubtitle : (text, max) => {
        if (text && (text.length > 0) && (text.length > max)) {
            return text.substr(0, max-4)+'...';
        }
        return text;
    }
};
