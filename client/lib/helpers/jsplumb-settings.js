JSPLUMB_TARGET_ENDPOINT_STYLE = {
    endpoint: "Dot",
    paintStyle: {
        fillStyle: "#7AB02C",
        radius: 11
    },
    maxConnections: -1,
    isTarget: true
};

JSPLUMB_SRC_ENDPOINT_STYLE = {
    endpoint: "Dot",
    paintStyle: {
        strokeStyle: "#7AB02C",
        fillStyle: "transparent",
        radius: 7,
        lineWidth: 3
    },
    isSource: true,
    maxConnections: -1,
    connector: ["Bezier", {
        curviness: 30
    }],
    connectorStyle: {
        lineWidth: 4,
        strokeStyle: "#61B7CF",
        joinstyle: "round",
        outlineColor: "white",
        outlineWidth: 2
    },
    connectorHoverStyle: {
        fillStyle: "#216477",
        strokeStyle: "#216477"
    }
};
