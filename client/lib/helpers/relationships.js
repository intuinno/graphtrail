Meteor.relationshipsHelper = {
    /* Remove a relationship
     * Undirected: remove link objects, embedded references, and reference ids of both entities
     * Directed:
     * 1. self relationship, remove link object and embedded reference of the selected entity;
     * 2. non-self relationship, remove link object and embedded reference of the selected entity;
     * remove reference id from the other entity.
     *
     * For directed relationship, the current entity is the one on the left hand side and the other entity
     * is the one on the right hand side.
     * */
    removeRelationship : (entities, entity, link, index) => {
        var fromEntityArgs = {};
        fromEntityArgs.entity = entity;
        fromEntityArgs.link = link;

        // find the other entity that the current entity links to
        let linkToEntity = entities.find((d) => {
            return d._id === link.linksToEntityId;
        });

        if (linkToEntity) {
            // for undirected relationship, remove references and relationship of the other entity
            if (!link.directed) {
                // find the link from the other entity to the current entity
                let linkedEntityRelation = linkToEntity.links.find((d) => {
                    return d.linksToEntityId == entity._id;
                });

                let toEntityArgs = {};
                toEntityArgs.entity = linkToEntity; // the other entity
                toEntityArgs.link = linkedEntityRelation; // the link object

                // remove the embedded reference field
                Meteor.call( 'removeReferencesFromEntity', toEntityArgs, ( error ) => {
                    if ( error ) {
                        Bert.alert( 'Failed to remove link!', 'danger', 'growl-top-right', 'fa-frown-o' );
                        console.log( error.reason );
                    } else {
                        toEntityArgs.linkIndex = linkToEntity.links.indexOf(linkedEntityRelation);

                        // remove the link object in the other entity
                        Meteor.call( 'removeLinkFromEntity', toEntityArgs, ( error ) => {
                            if ( error ) {
                                Bert.alert( 'Failed to remove link!', 'danger', 'growl-top-right', 'fa-frown-o' );
                                console.log( error.reason );
                            } else {
                                // remove the reference (linkedBy) id from the current entity
                                let linkedByArgs = {};
                                linkedByArgs.entity = entity;
                                linkedByArgs.linkedByEntityIdIndex = entity.linkedBy.indexOf(linkToEntity._id);

                                Meteor.call( 'removeIdFromLinkedBy', linkedByArgs, ( error ) => {
                                    if ( error ) {
                                        Bert.alert( 'Failed to remove link!', 'danger', 'growl-top-right', 'fa-frown-o' );
                                        console.log( error.reason );
                                    }
                                });
                            }
                        });
                    }
                });
            }

            // remove the embedded reference field and the link object of the current entity
            Meteor.call( 'removeReferencesFromEntity', fromEntityArgs, ( error ) => {
                if ( error ) {
                    Bert.alert( 'Failed to remove relationship!', 'danger', 'growl-top-right', 'fa-frown-o' );
                    console.log( error.reason );
                } else {
                    // remove the link object of the current entity
                    fromEntityArgs.linkIndex = index;

                    Meteor.call( 'removeLinkFromEntity', fromEntityArgs, ( error ) => {
                        if ( error ) {
                            Bert.alert( 'Failed to remove link!', 'danger', 'growl-top-right', 'fa-frown-o' );
                            console.log( error.reason );
                        } else {
                            // remove the reference id of the other entity, does not apply for self relationship
                            if (!link.selfJoinKey) {
                                let linkedByArgs = {};
                                linkedByArgs.entity = linkToEntity;
                                linkedByArgs.linkedByEntityIdIndex = linkToEntity.linkedBy.indexOf(entity._id);

                                Meteor.call( 'removeIdFromLinkedBy', linkedByArgs, ( error ) => {
                                    if ( error ) {
                                        Bert.alert( 'Failed to remove link!', 'danger', 'growl-top-right', 'fa-frown-o' );
                                        console.log( error.reason );
                                    } else {
                                        Bert.alert( 'Relationship removed!', 'success', 'growl-top-right', 'fa-check' );
                                    }
                                });
                            } else {
                                /* if the link just removed is in a self relationship, need to remove the other link
                                * that joined on the reversed attributes */
                                let linkWithReverseAttrIndex;
                                let linkWithReverseAttr = entity.links.find((l, i) => {
                                    linkWithReverseAttrIndex = i;
                                    return l.selfJoinKey === link.selfJoinKey;
                                });

                                fromEntityArgs.link = linkWithReverseAttr;

                                Meteor.call( 'removeReferencesFromEntity', fromEntityArgs, ( error ) => {
                                    if (error) {
                                        Bert.alert('Failed to remove relationship!', 'danger', 'growl-top-right', 'fa-frown-o');
                                        console.log(error.reason);
                                    } else {
                                        // remove the link object of the current entity
                                        fromEntityArgs.linkIndex = linkWithReverseAttrIndex;

                                        Meteor.call('removeLinkFromEntity', fromEntityArgs, (error) => {
                                            if (error) {
                                                Bert.alert('Failed to remove link!', 'danger', 'growl-top-right', 'fa-frown-o');
                                                console.log(error.reason);
                                            } else {
                                                Bert.alert( 'Relationship removed!', 'success', 'growl-top-right', 'fa-check' );
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            Bert.alert( 'Failed to remove link! Joining entity not found...', 'danger', 'growl-top-right', 'fa-frown-o' );
        }
    }
};