BAR_CHART = 'bar';
PIE_CHART = 'pie';
GATHERPLOT = 'gp';

CHART_HEIGHT_MIN = 700; // the initial height of a chart in trail
BASE_HEIGHT_BAR = 200; // the minimum height for bar chart in dashboard
BASE_HEIGHT_PIE = 200; // the minimum height for pie chart in dashboard

GATHERPLOT_OPT = {
    isGather: 'gather',
    relativeMode: 'absolute',
    binSize: 10,
    SVGAspectRatio: 1.4,
    matrixMode: false,
    isInteractiveAxis: false,
    relativeModes: [false, true],
    gatherplotId: -1,
    colorCoding: null,
    dims: []
};

NVD3_OPT_PIE = {
    chart: {
        type: 'pieChart',
        height: 720,
        x: function(d){return d.x;},
        y: function(d){return d.y;},
        showLabels: true,
        growOnHover: true,
        sliceSelect: false, // false by default
        duration: 0,
        showLegend: true, // true by default
        legend: {
            align: true, // true by default
            rightAlign: true, // true by default
            maxKeyLength: 20, // 20 by default
            padding: 32, // 32 by default
            margin: {
                top: 5,
                right: 35,
                bottom: 5,
                left: 0
            },
            updateState: false
        }
    }
};

NVD3_OPT_DISCRETEBAR = {
    chart: {
        type: 'discreteBarChart',
        height: 700,
        x: function(d){return d.x;},
        y: function(d){return d.y;},
        showValues: false,
        barSelect: false,
        duration: 0,
        xAxis: {
            tickPadding: 7,
            tickFormat: function(d){return d;}
        },
        yAxis: {
            axisLabel: '',
            axisLabelDistance: -10,
            tickFormat: function(d){return d;}
        }
    }
};

NVD3_CONFIG = {
    visible: true, // default: true
    extended: true, // default: false
    disabled: false, // default: false
    refreshDataOnly: true, // default: true
    deepWatchOptions: true, // default: true
    deepWatchData: true, // default: true
    deepWatchDataDepth: 2, // default: 2
    debounce: 10 // default: 10
};
