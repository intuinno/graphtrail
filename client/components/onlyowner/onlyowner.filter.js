'use strict';

angular.module('graphtrailApp')
.filter('onlyowner', function() {
  return function(items, user) {

  	if (!user) 
  		return false; 

  	return _.filter(items, function(item) {
  		if (item.ownerId == user._id || _.contains(item.invited, user._id))
  			return true;
  		else 
  			return false;
  	});
  };
});