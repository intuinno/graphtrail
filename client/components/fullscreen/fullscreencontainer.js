'use strict';

/**
 * @ngdoc directive
 * @name graphtrailApp.directive:fullscreenContainer
 * @description
 * # fullscreenContainer
 */
angular.module('graphtrailApp')
    .directive('fullscreenContainer', ['$document', function($document) {
        //Notice that we're not doing an isolate scope here - there's really no reason to.

        var d = window.document;

        return {
            restrict: 'EA',
            controller: function($scope, $element, $attrs) {
                //get a handle to the native DOM element
                var el = $element[0];
                //alias "this" for use in event handlers, etc...
                var thisCtrl = this;

                var fullScreenState = {
                    isFullScreen: false
                };



                //add in a hook in case the child directive wants to react
                //when this element enters/exits fullscreen state
                //note we're adding the property to "this" and not $scope
                thisCtrl.onFullScreenChange = null;

                //populate whether the fullscreen API is even supported
                fullScreenState.canDoFullScreen = d.fullscreenEnabled ||
                    d.webkitFullscreenEnabled ||
                    d.mozFullScreenEnabled ||
                    d.msFullscreenEnabled;

                if (fullScreenState.canDoFullScreen) {
                    //get the function on the element for making it fullscreen
                    //currently, still varied by vendor
                    var fullScreenFn =
                        (el.requestFullscreen && 'requestFullscreen') ||
                        (el.mozRequestFullScreen && 'mozRequestFullScreen') ||
                        (el.webkitRequestFullscreen && 'webkitRequestFullscreen') ||
                        (el.msRequestFullscreen && 'msRequestFullscreen');

                    //again, expose a method so that the child directive can access it
                    thisCtrl.makeElementFullScreen = function() {


                        el[fullScreenFn]();
                    };

                    //document emits an event when fullscreen state changes
                    //also still varied by vendor
                    var fsChangeFns = [
                        'fullscreenchange',
                        'webkitfullscreenchange',
                        'mozfullscreenchange',
                        'MSFullscreenChange'
                    ];

                    //event handler
                    function fsChanged(evt) {
                        //Obtain the fullscreen element (if there is one)
                        //again, varies by vendor
                        var fullScreenEl = d.fullscreenElement ||
                            d.webkitFullscreenElement ||
                            d.mozFullScreenElement ||
                            d.msFullscreenElement;

                        var oldState = fullScreenState.isFullScreen;

                        //fullScreenState.isFullScreen = (fullScreenEl && fullScreenEl === el);

                        fullScreenState.isFullScreen = d.webkitIsFullScreen ||
                            d.fullscreen ||
                            d.mozFullScreen ||
                            d.msFullscreenElement;


                        if (oldState !== fullScreenState.isFullScreen && typeof thisCtrl.onFullScreenChange === 'function') {
                            //if the child directive provided a callback, invoke it and provide the new state

                            thisCtrl.onFullScreenChange(fullScreenState.isFullScreen);
                        }

                        // console.log("fullScreen changed",evt);

                    }

                    //add event listener for each of those events
                    fsChangeFns.forEach(function addFullScreenChangeEvtListener(fsEvtName) {
                        d.addEventListener(fsEvtName, fsChanged);
                    });
                } else {
                    //here, we could have a fallback for when the FullScreen API isn't supported
                    //maybe absolutely position the element, add a backdrop, w/e
                }
            }


        };
    }]);
