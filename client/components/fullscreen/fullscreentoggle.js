'use strict';

/**
 * @ngdoc directive
 * @name graphtrailApp.directive:fullscreenToggle
 * @description
 * # fullscreenToggle
 */
angular.module('graphtrailApp')
    .directive('fullscreenToggle', function() {
        return {
            restrict: 'EA',
            scope: {
                zoom: '=',
                fullflag: '=',
                onFull: '&'
            },
            //here's where we "require" the controller of the fullscreenContainer directive!
            //prefixed symbols, such as "^", are essentially telling angular where to look for the required controller 
            //i.e. on same element, on a parent element, etc...
            require: '^fullscreenContainer',
            //Now, if the required controller is found, it's passed into the arguments for the link function!
            link: function(scope, element, attrs, fullscreenContainerCtrl) {
                //we only care to attach event handlers, etc.. 
                //if the parent ctrl has the "makeElementFullScreen" function defined

                var oldZoom; 
                scope.full = false; 

                if (typeof fullscreenContainerCtrl.makeElementFullScreen === 'function') {
                    //wire up event handler for when the element is clicked
                    element.on('click', function fullscreenToggleClicked() {
                        fullscreenContainerCtrl.makeElementFullScreen();
                    });
                    //also populate the callback function for when fullscreen state changes
                    fullscreenContainerCtrl.onFullScreenChange = function fullscreenStateChanged(isFullScreen) {
                        //if this element's ancestor is fullscreen, hide the toggle
                        if (isFullScreen) {
                            element.hide();
                            oldZoom = scope.zoom;
                            scope.zoom = 100;
                            scope.onFull({isFullScreen:true});
                            scope.fullflag = true;
                            scope.$apply();

                        } else {
                            element.show();
                            scope.zoom = oldZoom;
                            scope.onFull({isFullScreen:false});
                            scope.fullflag = false;
                            scope.$apply();
                        }
                    };
                } else {
                    //then there is no method for making the ancestor fullscreen - either API or fallback...
                    element.hide();
                }
            }
        };
    });
