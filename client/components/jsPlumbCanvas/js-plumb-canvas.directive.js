'use strict';

/**
 * @ngdoc directive
 * @name graphtrailApp.directive:plumber
 * @description
 * # plumber
 */

var myDirective = angular.module('graphtrailApp');

// detect available wheel event
var support = "onwheel" in document.createElement("div") ? "wheel" : // Modern browsers support "wheel"
          document.onmousewheel !== undefined ? "mousewheel" : // Webkit and IE support at least "mousewheel"
          "DOMMouseScroll"; // let's assume that remaining browsers are older Firefox

myDirective.directive('jsPlumbCanvas', function () {
    var jsPlumbZoomCanvas = function (instance, zoom, el, transformOrigin) {
        //transformOrigin = transformOrigin || [0.5, 0.5];
        //var p = ["webkit", "moz", "ms", "o"],
        //    s = "scale(" + zoom + ")",
        //    oString = (transformOrigin[0] * 100) + "% " + (transformOrigin[1] * 100) + "%";
        //for (var i = 0; i < p.length; i++) {
        //    el.style[p[i] + "Transform"] = s;
        //    el.style[p[i] + "TransformOrigin"] = oString;
        //}
        //el.style["transform"] = s;
        //el.style["transformOrigin"] = oString;
        //console.log("SetZoomlevel: ", zoom)
        instance.setZoom(zoom);

    };

    var def = {
        restrict: 'E',
        scope: {
            onConnection: '=onConnection',
            zoom: '=',
            x: '=',
            y: '=',
            drag: '='
        },
        controller: ['$scope', function ($scope) {
            this.scope = $scope;
        }],
        transclude: true,
        template: '<div ng-transclude></div>',
        link: function (scope, element, attr) {

            var instance = jsPlumb.getInstance({
                PaintStyle: {
                    lineWidth: 6,
                    strokeStyle: "#567567",
                    outlineColor: "black",
                    outlineWidth: 1
                },
                Connector: ["Bezier", {
                    curviness: 30
                }]
            });

            scope.jsPlumbInstance = instance;

            // instance.ready(function() {

            instance.bind("connectionDrag", function (connection, originalEvent) {
                // console.log("connectionDrag " + connection.id + " is being dragged. suspendedElement is ", connection.suspendedElement, " of type ", connection.suspendedElementType);
                //console.log("connectionDrag", connection, originalEvent);
            });

            instance.bind("connectionMoved", function (params) {
                //console.log("connection " + params.connection.id + " was moved");
            });

            instance.bind("connection", function (info, origEvent) {
                if (typeof origEvent !== 'undefined' && origEvent.type == 'drop') {
                    //console.log("[connection] event in jsPlumbCanvas Directive [DRAG & DROP]", info, origEvent);
                    var targetUUID = $(info.target).attr('uuid');
                    var sourceUUID = $(info.source).attr('uuid');
                    console.log("Calling onConnection...");
                    scope.onConnection(instance, info.connection, targetUUID, sourceUUID);
                    instance.detach(info.connection);
                }
            });


            $(element).css({
                minWidth: '1000px',
                minHeight: '1000px',
                display: 'block',
            });

            var $section = $('#focal');

            var $panzoom = $section.find('.panzoom').panzoom({
                minScale: 0.01,
                maxScale: 1,
            });

            $panzoom.panzoom('pan', -52050, -50050, {
                relative: false
            });

            $panzoom.on("panzoomzoom", function (e, panzoom) {

                var resultMatrix = panzoom.getMatrix();
                var zoom = resultMatrix[0];
                scope.zoom = zoom * 100;
                jsPlumbZoomCanvas(instance, zoom, $(element)[0]);
            });

            if ( support == "mousewheel" ) {
                $panzoom.parent().on('mousewheel.focal', onMouseWheel);
            } else {
                $panzoom.parent().on('wheel', onMouseWheel);
            }

            function onMouseWheel(e) {
                e.preventDefault();
                var delta = e.delta || e.originalEvent.wheelDelta;
                var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
                var focalEvent = support == "mousewheel" ? e : e.originalEvent;
                //  console.log('focal',e);
                $panzoom.panzoom('zoom', zoomOut, {
                    increment: 0.02,
                    animate: false,
                    focal: focalEvent
                });
            }

            instance.setContainer($(element));

            var zoom = (typeof scope.zoom === 'undefined') ? 1 : scope.zoom / 100;
            jsPlumbZoomCanvas(instance, zoom, $(element)[0]);

            $('.panzoom select').on('mousedown touchend', function (e) {
                // console.log('Fixing the panzoom button');
                e.stopPropagation();
            });

        }
    };

    return def;
});


myDirective.directive('jsPlumbObject', function () {
    var def = {
        restrict: 'E',
        require: '^jsPlumbCanvas',
        scope: {
            stateObject: '=stateObject',
            cards: '=cards', // expose this to jsPlumbConnection for updating chart
            updateController: "&updateFn" // call controller method from directive scope
        },
        transclude: true,
        template: '<div ng-transclude></div>',
        link: function (scope, element, attrs, jsPlumbCanvas) {
            var instance = jsPlumbCanvas.scope.jsPlumbInstance;

            instance.draggable(element, {

                //start: function (event,ui) {
                //    event.stopPropagation();
                //},
                //
                //
                //drag: function (event, ui) {
                //    event.stopPropagation();
                //},

                stop: function (event, ui) {
                    scope.stateObject.x = ui.position.left;
                    scope.stateObject.y = ui.position.top;
                    event.stopPropagation();

                    scope.$apply(function() {
                        scope.updateController(scope.stateObject);
                    });
                }
            });

            element.resizable({
                resize: doResize
            });

            function doResize(event, ui) {
                if (ui.element.scope().card.type) {
                    var type = ui.element.scope().card.type;
                    var zoomScale = $('js-plumb-canvas').scope().zoomlevel/100;

                    var changeWidth = ui.size.width - ui.originalSize.width; // find change in width
                    var newWidth = ui.originalSize.width + changeWidth / zoomScale; // adjust new width by our zoomScale

                    var changeHeight = ui.size.height - ui.originalSize.height; // find change in height
                    var newHeight = ui.originalSize.height + changeHeight / zoomScale; // adjust new height by our zoomScale

                    ui.size.width = newWidth;
                    ui.size.height = newHeight;

                    if (ui.size.width < 800) {
                        ui.size.width = 800;
                    }

                    if (ui.size.height < 600) {
                        ui.size.height = 600;
                    }

                    if (type === "md") {
                        var $content = $(this).find('.md-content');
                        var contentHeight = $content.outerHeight();
                        var contentWidth = $content.outerWidth();

                        var scaleFactor = 0;

                        scaleFactor = Math.min(
                            ui.size.width / contentWidth,
                            ui.size.height / contentHeight
                        );

                        scope.stateObject.scaleFactor = scaleFactor;
                    } else if (type === "trail") {
                        var $trailTopUI = $(this).find('.trail-ui-top');
                        var $trailBotUI = $(this).find('.chart-simple-interface');
                        var trailTopUIHeight = $trailTopUI.outerHeight(true);
                        var trailBotUIHeight = $trailBotUI.outerHeight(true);
                        var botMargin = 40;
                        var availChartHeight = ui.size.height - trailTopUIHeight - trailBotUIHeight - botMargin;
                        scope.stateObject.nvd3ChartHeight = availChartHeight;

                        var nvd3ChartConfigs = ui.element.scope().nvd3ChartConfigs;
                        if (nvd3ChartConfigs && nvd3ChartConfigs[scope.stateObject.id]) {
                            nvd3ChartConfigs[scope.stateObject.id].bar.chart.height = availChartHeight;
                            nvd3ChartConfigs[scope.stateObject.id].pie.chart.height = availChartHeight;
                        }
                    }
                    scope.stateObject.width = ui.size.width;
                    scope.stateObject.height = ui.size.height;
                }

                scope.$apply(function() {
                    scope.updateController(scope.stateObject);
                });

                jsPlumb.repaint(ui.helper);
            }

            // use pointer event for chrome 55+
            $('.panzoom js-plumb-object').on('pointerdown mousedown touchend', function (e) {
                e.stopPropagation();
            });

            $('.panzoom select').on('pointerdown mousedown touchend', function (e) {
                e.stopPropagation();
            });

            // $('.panzoom').on('pointermove mousemove touchmove', function (e) {
            //     console.log("x: " + e.offsetX + " y: " + e.offsetY);
            //     e.stopPropagation();
            // });

            scope.$on('$destroy', function (e) {
                //console.log(scope.stateObject);
            });

            // scope.stateObject.isScatter = false;
            //
            // scope.$watch(function () {
            //
            //     if (scope.stateObject)
            //         if (scope.gatherConfig)
            //             return scope.stateObject.gatherConfig.isGather;
            // }, function (newVals, oldVals) {
            //     // debugger;
            //
            //     if (angular.equals(newVals, oldVals))
            //         return;
            //
            //
            //     if (newVals === 'scatter') {
            //
            //         scope.stateObject.isScatter = true;
            //     } else {
            //
            //         scope.stateObject.isScatter = false;
            //     }
            // }, true);


        }
    };
    return def;
});

myDirective.directive('jsPlumbEndpoint', function () {

    var def = {
        restrict: 'E',
        require: '^jsPlumbCanvas',
        scope: {
            settings: '=settings'
        },
        controller: ['$scope', function ($scope) {
            this.scope = $scope;
            this.connectionObjects = {};
        }],
        transclude: true,
        template: '<div ng-transclude></div>',
        link: function (scope, element, attrs, jsPlumbCanvas) {
            var instance = jsPlumbCanvas.scope.jsPlumbInstance;
            scope.jsPlumbInstance = jsPlumbCanvas.scope.jsPlumbInstance;
            scope.uuid = attrs.uuid;
            var options = {
                anchor: attrs.anchor,
                uuid: attrs.uuid
            };

            // scope.jsPlumbInstance.ready(function() {

            //console.log('rigging up endpoint');
            $(element).addClass('_jsPlumb_endpoint');
            $(element).addClass('endpoint_' + attrs.anchor);

            var ep = instance.addEndpoint(element, scope.settings, options);


            scope.$on('$destroy', function () {
                instance.deleteEndpoint(ep);
            });

            // });


        }
    };
    return def;
});


myDirective.directive('jsPlumbConnection', ['$timeout', function ($timeout) {

    var def = {
        restrict: 'E',
        require: '^jsPlumbEndpoint',
        scope: {
            ngClick: '&ngClick',
            ngModel: '=ngModel',
            cards: '=cards', // expose this to jsPlumbConnection for updating chart
            data: '=data'
        },
        link: function (scope, element, attrs, jsPlumbEndpoint) {
            var instance = jsPlumbEndpoint.scope.jsPlumbInstance;
            var sourceUUID = jsPlumbEndpoint.scope.uuid;
            var targetUUID = scope.ngModel.uuid;

            //we delay the connections by just a small bit for loading
            $timeout(function () {
                //console.log(scope.ngModel.conn);
                if (typeof jsPlumbEndpoint.connectionObjects[targetUUID] === 'undefined') {
                    jsPlumbEndpoint.connectionObjects[targetUUID] = instance.connect({
                        uuids: [
                            sourceUUID,
                            targetUUID
                        ],
                        overlays: [
                            ["Arrow", {
                                location: 0.89,
                                id: "arrow",
                                length: 14,
                                foldback: 0.8
                            }],
                            ["Label", {
                                label: "",
                                id: "label"
                            }]
                        ],
                        Connector: ["Bezier", {
                            curviness: 30
                        }],
                        editable: true
                    });

                    //console.log('[created---------][directive][jsPlumbConnection] ');
                }

                var connection = jsPlumbEndpoint.connectionObjects[targetUUID];

                connection.bind("mouseenter", mouseEnterConn);

                connection.bind("mouseleave", mouseLeaveConn);

                // connection.bind("click", function (conn, originalEvent) {
                //     scope.ngClick();
                //     scope.$apply();
                // });

                function mouseEnterConn(conn, originalEvent) {
                    setCardHighlightState(conn, scope, true);
                }

                function mouseLeaveConn(conn, originalEvent) {
                    setCardHighlightState(conn, scope, false);
                }

                /*
                * When hover on the link (connection) between a parent and a child card,
                * highlight the data points on the parent card.
                * When mouse leave the link, remove the highlight.
                * */
                function setCardHighlightState(conn, scope, isHover) {
                    if (conn && scope) {
                        var connSourceUuid = $(conn.source).scope().endpoint.uuid;
                        var connTargetUuid = $(conn.target).scope().endpoint.uuid;
                        var parent, child;

                        if (scope.cards && scope.data && connSourceUuid && connTargetUuid) {
                            scope.cards.forEach(function(card) {
                                var cardSourceUuid = card.sources[0].uuid;

                                if (connSourceUuid === cardSourceUuid) {
                                    parent = card;
                                    return;
                                }
                            });

                            scope.cards.forEach(function(card) {
                                var cardTargetUuid = card.targets[0].uuid;

                                if (connTargetUuid === cardTargetUuid) {
                                    child = card;
                                    return;
                                }
                            });

                            if (parent && child) {
                                var parentSelection = child.parentSelection;
                                var parentChartType = parent.chartType;
                                var chartData = scope.data[parent.id];

                                switch (parentChartType) {
                                    case GATHERPLOT:
                                        // push all indices of the chart data since all data was selected
                                        if (parentSelection.length === 0) {
                                            chartData.forEach(function(d,i) { parentSelection.push(i); });
                                        }

                                        parent.dimsum.selectionSpace = isHover ? parentSelection : [];

                                        break;
                                    case BAR_CHART:
                                        var targetData = chartData[0];

                                        if (parentSelection.length === 0) {
                                            targetData.values.forEach(function(d) { d.selected = isHover; });
                                        } else {
                                            parentSelection.forEach(function(ind) {
                                                targetData.values[ind].selected = isHover;
                                            });
                                        }

                                        break;
                                    case PIE_CHART:
                                        var targetData = chartData;

                                        if (parentSelection.length === 0) {
                                            targetData.forEach(function(d) { d.selected = isHover; });
                                        } else {
                                            parentSelection.forEach(function(ind) {
                                                targetData[ind].selected = isHover;
                                            });
                                        }

                                        break;
                                    default:
                                        break;
                                }

                                scope.$apply();
                            }
                        }
                    }
                }

                // not really using this... but we should fix it :)

                var overlay = connection.getOverlay("label");
                if (overlay) {
                    //console.log('[getOverlay][label]', connection.getOverlay("label"));
                    $(element).appendTo(overlay.canvas);
                }

            }, 300);


            // });


            scope.$on('$destroy', function () {
                //console.log('jsPlumbConnection for $destroy');
                try {
                    // instance.detach(jsPlumbEndpoint.connectionObjects[targetUUID]);
                } catch (err) {
                    //console.log('error', err, jsPlumbEndpoint.connectionObjects[targetUUID]);

                }
                // if the connection is destroyed, I am assuming the parent endPoint is also destroyed, and we need to remove
                // the reference that a link exists, so it will be rendered again
                jsPlumbEndpoint.connectionObjects[targetUUID] = undefined;
            });

        }
    };
    return def;
}]);
