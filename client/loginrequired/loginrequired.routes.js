'use strict';

angular.module('graphtrailApp')
.config(function($stateProvider) {
  $stateProvider
  .state('loginrequired', {
    url: '/loginrequired',
    templateUrl: 'client/loginrequired/loginrequired.view.html',
    controller: 'LoginrequiredCtrl'
  });
});