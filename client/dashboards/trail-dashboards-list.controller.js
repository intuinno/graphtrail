'use strict'

angular.module('graphtrailApp')
.controller('TrailDashboardsListCtrl', ['$scope', '$stateParams', function($scope, $stateParams) {
  $scope.page = 1;
  $scope.perPage = 10;
  $scope.sort = {name: 1};
  $scope.orderProperty = '1';
  $scope.searchText = '';
  
  $scope.helpers({
    dashboards: () => {
      return Dashboards.find({
        trailId: $stateParams.trailId
      }, {
        sort: {name: $scope.getReactively('sort')}
      });
    },
    dashboardsCount: () => {
      return Counts.findOne('numberOfDashboards');
    }
  });

  $scope.subscribe('dashboards', () => {
    return [
      {
        // how many items per page
        limit: parseInt($scope.perPage),
        // the number of items to start with
        skip: parseInt(($scope.getReactively('page') - 1) * $scope.perPage),
        // the sorting of the collection
        sort: $scope.getReactively('sort')
      },
      $scope.getReactively('searchText'),
    ]
  });
    
  $scope.save = function() {
  };
      
  $scope.remove = function(dashboard) {
    Dashboards.remove({_id: dashboard._id});
  };
    
  $scope.pageChanged = function(newPage) {
    $scope.page = newPage;
  };
    
  $scope.$watch('orderProperty', function() {
    if($scope.orderProperty) {
      $scope.sort = {name: parseInt($scope.orderProperty)};
    }
  });
}]);
        