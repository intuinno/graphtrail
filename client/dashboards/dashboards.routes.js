'use strict';

angular.module('graphtrailApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('dashboards-list', {
                url: '/dashboards',
                templateUrl: 'client/dashboards/dashboards-list.view.html',
                controller: 'DashboardsListCtrl'
            })
            .state('dashboard-detail', {
                url: '/dashboards/:dashboardId',
                templateUrl: 'client/dashboards/dashboard-detail.view.html',
                controller: 'DashboardDetailCtrl'
            })
            .state('trails-dashboards-list', {
                url:'/trails/:trailId/dashboards',
                templateUrl: 'client/dashboards/dashboards-list.view.html',
                controller: 'TrailDashboardsListCtrl'
            });
    });