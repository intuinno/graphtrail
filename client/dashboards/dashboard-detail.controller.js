'use strict';

angular.module('graphtrailApp')
    .controller('DashboardDetailCtrl', ['$scope', '$stateParams', '$timeout', function ($scope, $stateParams, $timeout) {
        $scope.index = 0;
        $scope.entityDataObjectsList = [];
        $scope.stateObjectsDashboard = [];
        $scope.isAllLoaded = false;

        /* Gridster settings */
        let DASH_GRIDSTER_OPT = GRIDSTER_OPT;
        DASH_GRIDSTER_OPT.resizable = {
            enabled: true,
            handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
            start: function(event, $element, widget) {}, // optional callback fired when resize is started,
            resize: function(event, $element, widget) {}, // optional callback fired when item is resized,
            stop: function(event, $element, widget) {
                $timeout(() => {
                    if ($scope.stateObjectsDashboard) {
                        let resizedCard = $element.scope().state;
                        let index = $scope.stateObjectsDashboard.findIndex((d) => {
                            return d.id === resizedCard.id;
                        });

                        if (resizedCard.type === "md") {
                            // scale content size if not fit
                            scaleDashboardMdContent(resizedCard, index);
                        }
                    }
                }, 10);
            } // optional callback fired when item is finished resizing
        };

        $scope.gridsterOpts = DASH_GRIDSTER_OPT;
        $scope.customItemMap = GRIDSTER_CUSTOM_MAP;

        /* Nvd3 chart settings */
        $scope.nvd3ChartConfigsDash = {};

        var disBarOptDash = angular.copy(NVD3_OPT_DISCRETEBAR);
        var pieOptDash = angular.copy(NVD3_OPT_PIE);

        $scope.helpers({
            entities: () => {
                return Data.find({});
            }
        });

        var trailData = {};
        var trailDataChecker = {};

        var dashboardsSubHandle = $scope.subscribe('dashboards', () => [], {
            onReady: function() {
                $scope.dashboard = Dashboards.findOne({_id: $stateParams.dashboardId});

                if ($scope.dashboard) {
                    if ($scope.dashboard.dataSource === 'trails') {
                        var trailsSubHandle = $scope.subscribe('trails', ()=> [], {
                            onReady: function() {
                                $scope.trail = Trails.findOne({_id: $scope.dashboard.trailId});
                                $scope.originalExplorationLink = '/trails/' + $scope.trail._id;

                                var dataSubHandler = $scope.subscribe('data', () => [], {
                                    onReady: function () {
                                        loadEntityDataInUse();
                                    },
                                    onStop: function(error) {
                                        if (error) {
                                            console.log('OnStop error: ' + error);
                                        } else {
                                            console.log('The data subscription stopped');
                                        }
                                    }
                                });
                            },
                            onStop: function(error) {
                                if (error) {
                                    console.log('OnStop error: ' + error);
                                } else {
                                    console.log('The trails subscription stopped');
                                }
                            }
                        });
                    }
                }
            },
            onStop: function(error) {
                if (error) {
                    console.log('OnStop error: ' + error);
                } else {
                    console.log('The dashboards subscription stopped');
                }
            }
        });

        // handle grid resize event
        $scope.$watch(($scope) => {
            if ($scope.stateObjectsDashboard) {
                return $scope.stateObjectsDashboard.map((obj) => {
                    return obj.gridster;
                });
            }
        }, (newVal, oldVal) => {
            if (newVal[0] && oldVal[0]) {
                if (newVal.length === oldVal.length) {
                    for (var i=0; i<newVal.length; i++) {
                        if ((newVal[i].size.x !== oldVal[i].size.x) ||
                            (newVal[i].size.y !== oldVal[i].size.y)) {
                            // update nvd3 chart height
                            let gridItem = $scope.stateObjectsDashboard[i];

                            if ($scope.nvd3ChartConfigsDash[gridItem.id]) {
                                initNvd3ChartHeightDash(gridItem);
                            }

                            // if a dashboard contains a nvd3 chart, dispatch a resize event
                            windowResize(50);
                        }
                    }
                }
            }
        }, true);

        $scope.getEntityLabel = (id) => {
            // search locally for the entity object
            let entityObj = Meteor.utilitiesHelper.getEntityById($scope.entities, id);

            if (entityObj) {
                return entityObj.label;
            } else {
                return "Missing Entity";
            }
        };

        function loadEntityDataInUse() {
            if ($scope.trail) {
                if ($scope.trail.loadedEntities.length > 0) {
                    $scope.trail.loadedEntities.forEach((entityId) => {
                        if (Meteor.utilitiesHelper.getEntityById($scope.entities, entityId)) {
                            trailDataChecker[entityId] = false;
                            // console.log('Retrieving entity: ' + entityId);

                            Meteor.call('getDataFromEntityId', entityId, (error, result ) => {
                                if (error) {
                                    console.log( error );
                                } else {
                                    // copy data into global array
                                    trailData[entityId] = result;
                                    trailDataChecker[entityId] = true;
                                    checkAllLoading();
                                }
                            });
                        } else {
                            doThingsAfterLoading();
                        }
                    });
                } else {
                    doThingsAfterLoading();
                }
            }
        }

        function checkAllLoading() {
            if (Meteor.utilitiesHelper.checkAllProp(trailDataChecker)) {
                doThingsAfterLoading();
            }
        }

        function doThingsAfterLoading() {
            $scope.isAllLoaded = true;

            if ($scope.trail.stateObjects) {
                loadEntityDataObjects();
                loadDashboardObjects();
                windowResize(50);
            }
        }

        function loadEntityDataObjects() {
            if ($scope.dashboard) {
                if ($scope.dashboard.grids.length > 0) {
                    $scope.dashboard.grids.forEach((state, index) => {
                        if (state.entityId) {
                            // if entity is removed, reset properties of the dashboard card and show missing data message
                            if (Meteor.utilitiesHelper.getEntityById($scope.entities, state.entityId)) {
                                // $scope.entityDataObjectsList[grid.id] = Meteor.utilitiesHelper.fromIdListToObjectList(grid.entityData, trailData[grid.entityId]);

                                if (state.chartType === 'gp') {
                                    if (state.entityData) {
                                        let selectedDocuments = Meteor.utilitiesHelper.fromIdListToObjectList(state.entityData, trailData[state.entityId]);
                                        updateDataUsedByGatherplot(state, selectedDocuments);
                                        selectedDocuments = null; // free space
                                    }
                                } else { // pie or bar
                                    initNvd3ChartConfigDash(state.id);
                                    initNvd3ChartHeightDash(state);

                                    if (state.chartData) {
                                        let selectedDim = state.chartConfig.selection.dimName;

                                        if (selectedDim) {
                                            updateDataUsedByChart(state.id, state.chartData[selectedDim]);
                                        } else {
                                            updateDataUsedByChart(state.id, state.chartData[Object.keys(state.chartData)[0]]);
                                        }
                                    }

                                    windowResize(500);
                                }
                            } else {
                                let stateMissingData = Meteor.utilitiesHelper.createBrokenCard(state, 'dashboard');

                                Dashboards.update({_id: $stateParams.dashboardId}, {
                                    $set : {
                                        [`grids.${index}`]: stateMissingData
                                    }
                                });
                            }
                        }
                    });
                }
            }
        }

        function loadDashboardObjects() {
            let dashboard = $scope.dashboard;
            $scope.stateObjectsDashboard = dashboard.grids;
        }

        /*
         * Update the chart data array for other chart types (pie, bar, and etc.)
         * */
        function updateDataUsedByChart(id, data) {
            $scope.entityDataObjectsList[id] = data;
        }

        /*
         * Update the chart data array with gatherplot data
         * */
        function updateDataUsedByGatherplot(state, data) {
            data = angular.copy(data);
            data.map((d, i) => { d.id = i; });
            $scope.entityDataObjectsList[state.id] = data;
        }

        /* Dispatch a resize event in order to re-render chart */
        function windowResize(time) {
            $timeout(function() {
                window.dispatchEvent(new Event('resize'));
            }, time);
        }

        function scaleDashboardMdContent(dashMd, mdIndex) {
            if (mdIndex !== -1) {
                let scaleFactor = 0;
                let $mdObjs = $('.dash-md-content'); // get all markdown content DOMs

                $mdObjs.each((i) => {
                    if ($($mdObjs[i]).scope().state.id === dashMd.id) {
                        let $targetDOM = $($mdObjs[i]);
                        let contentHeight = $targetDOM.outerHeight();
                        let contentWidth = $targetDOM.outerWidth();
                        let parentHeight = $targetDOM.parent().outerHeight();
                        let parentWidth = $targetDOM.parent().outerWidth();

                        // scale content based on markdown's dimensions
                        let contentPadding = parseInt($targetDOM.parent().css('padding').replace(/\D/g,''));
                        let headerHeight = $('.box-header').outerHeight();

                        scaleFactor = Math.min(
                            (parentHeight - contentPadding*2 - headerHeight) / contentHeight,
                            parentWidth / contentWidth
                        );
                    }
                });

                if (scaleFactor !== 0) {
                    dashMd.scaleFactor = scaleFactor;
                }
            }
        }

        function initNvd3ChartConfigDash(stateId) {
            $scope.nvd3ChartConfigsDash[stateId] = {
                pie: angular.copy(pieOptDash),
                bar: angular.copy(disBarOptDash)
            };
        }

        function initNvd3ChartHeightDash(state) {
            $scope.nvd3ChartConfigsDash[state.id].bar.chart.height = calcHeightDash(BASE_HEIGHT_BAR, state.gridster.size.y);
            $scope.nvd3ChartConfigsDash[state.id].pie.chart.height = calcHeightDash(BASE_HEIGHT_PIE, state.gridster.size.y);
        }

        function calcHeightDash(baseHeight, sizeFactor) {
            let result = baseHeight*sizeFactor;
            let boxHeaderHeight = 52;

            // when y is 1, the header takes up 52px,
            // needs to add header height every time y is incremented
            if (sizeFactor > 1) {
                result += sizeFactor*boxHeaderHeight;
            }

            return result;
        }
    }]);
