'use strict';

angular.module('graphtrailApp')
.controller('TrailsProfileListCtrl', ['$scope', '$rootScope' , function($scope, $root) {
  $scope.page = 1;
  $scope.perPage = 10;
  $scope.sort = {name : 1};
  $scope.orderProperty = '1';
  $scope.searchText = '';

  $scope.helpers({
    trails: () => {
      return Trails.find({
        "ownerId": $root.currentUser._id
      }, { sort: {name: $scope.getReactively('sort')}}); // sort by trail list item name
    },
    trailsCount: () => {
      return Counts.findOne('numberOfTrails');
    }
  });

  // give reactivity to search field, sort dropdown menu, page number
  $scope.subscribe('trails', () => {
    return [
      {
        // how many items per page
        limit: parseInt($scope.perPage),
        // the number of items to start with
        skip: parseInt(($scope.getReactively('page') - 1) * $scope.perPage),
        // the sorting of the collection
        sort: $scope.getReactively('sort')
      },
      $scope.getReactively('searchText')
    ]
  });

  $scope.save = () => {
    if($scope.form.$valid) {
      $scope.newTrail.ownerId = $root.currentUser._id;
      Trails.insert($scope.newTrail);
      $scope.newTrail = {};
    }
  };

  $scope.remove = (trail) => {
    Trails.remove({_id: trail._id});
  };

  $scope.pageChanged = (newPage) => {
    $scope.page = newPage;
  };

  // when orderProperty changes, sort is updated
  $scope.$watch('orderProperty', function() {
    if($scope.orderProperty) {
      $scope.sort = {name: parseInt($scope.orderProperty)};
    }
  });
}]);
