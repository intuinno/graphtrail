'use strict';

angular.module('graphtrailApp')
    .config(($stateProvider) => {
        $stateProvider
            .state('profile', {
                url: '/profile',
                templateUrl: 'client/profile/profile.view.html',
                controller: 'ProfileCtrl',
                resolve: {
                    currentUser: ['$meteor', ($meteor) => {
                        return $meteor.waitForUser();
                    }]
                }
            });
        });
