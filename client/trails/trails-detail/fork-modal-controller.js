angular.module('graphtrailApp').controller('forkModalCtrl', ['$scope', '$modalInstance', 'existingExploration', function ($scope, $modalInstance, existingExploration) {

    $scope.ok = function () {

        $modalInstance.close("Hello");
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);
/**
 * Created by deokgunpark on 8/20/15.
 */
