angular.module('graphtrailApp')
    .controller('TrailDetailCtrl', [
        '$scope',
        '$http',
        '$stateParams',
        '$location',
        '$modal',
        '$timeout',
        'hotkeys',
        '$rootScope',
        '$state', ($scope, $http, $stateParams, $location, $modal, $timeout, hotkeys, $rootScope, $state) => {
            const host = $location.host();
            const port = $location.port();
            const trailData = {};
            const trailDataChecker = {}; // check if trail data is loaded

            const LOG_ON = false; // toggle logging
            const ZOOM_TIMEOUT = 1000;
            const EXCLUDE_KEYS = ['key', 'Key', 'KEY'];
            const EMBED_TYPES = ['IFRAME', 'OBJECT', 'EMBED']; // for markdown content
            const slideZoomFactor = 602; // for calculating zoom factor of a slide
            const DEFAULT_CHART = GATHERPLOT;
            const DEFAULT_WIDTH = 1000;
            const DEFAULT_HEIGHT = 960;
            const INIT_TOP = 50100;
            const INIT_LEFT = 51000;
            const PADDING = 200;

            /* Initial bindings */
            $scope.isAllLoaded = false;
            $scope.canvasMode = 'trail';
            $scope.isTrail = true;

            // if it is in comparison selection mode or not
            $scope.isCompareSelectMode = false;

            // the id of the root card where the comparison operation is carried out
            $scope.currCompareRootId = null;

            // determine if a comparison path has been selected or not
            $scope.isComparePath = false;

            // the id of the selected child of in a selected comparison path
            $scope.comparePathSubrootId = null;

            // store the card coordinates of the current subtree for placement
            $scope.currSubtreeCoord = [];

            $scope.entityDataObjectsList = [];
            $scope.stateObjectsDashboard = [];
            $scope.fullscreenObjects = [];

            /* For dashboard list */
            $scope.isDashboardList = false;
            $scope.dashPage = 1;
            $scope.dashPerPage = 10;
            $scope.dashSort = {
                name: 1,
            };
            $scope.dashOrderProperty = '1';
            $scope.dashSearchText = '';

            /* For slideshow list */
            $scope.isSlideshowList = false;
            $scope.slidePage = 1;
            $scope.slidePerPage = 10;
            $scope.slideSort = {
                name: 1,
            };
            $scope.slideOrderProperty = '1';
            $scope.slideSearchText = '';

            $scope.newDashboard = {
                name: '',
            };
            $scope.newSlideshow = {
                name: '',
            };

            /* For slideshow sidebar preview */
            $scope.previewScales = [];

            /* For slideshow / fullscreen */
            $scope.isFullScreen = false;
            $scope.slideIndex = 0; // slide index
            $scope.zoomlevel = 50;
            $scope.pos_x = 17;
            $scope.pos_y = 37;

            $scope.panelHeight = 300;
            $scope.trailPanelBodyHeight = 300;
            $scope.panelBodyHeight = 300;
            $scope.slideshowContentHeight = 300;
            $scope.slideHeight = 300;
            $scope.slideshowSideBarHeight = 300;
            $scope.slideCenterZoom = 1;

            $scope.host = ($location.host() === 'localhost') ? (`${host}:${port}/`) : (`${host}/`);

            $scope.helpers({
                trail: () => {
                    return Trails.findOne({ _id: $stateParams.trailId });
                },
                trailDashboards: () => {
                    return Dashboards.find({
                        trailId: $stateParams.trailId,
                    }, {
                        sort: {
                            name: $scope.getReactively('dashSort'),
                        },
                    }); // sort by dashboard list item name
                },
                dashboardsCount: () => {
                    return Counts.findOne('numberOfTrailDashboards');
                },
                trailSlideshows: () => {
                    return Slideshows.find({
                        trailId: $stateParams.trailId,
                    }, {
                        sort: {
                            name: $scope.getReactively('slideSort'),
                        },
                    }); // sort by slideshow list item name
                },
                slideshowsCount: () => {
                    return Counts.findOne('numberOfTrailSlideshows');
                },
                entities: () => {
                    return Data.find({});
                },
            });

            const trailsSubHandler = $scope.subscribe('trails', () => [], {
                onReady: () => {
                    const dataSubHandler = $scope.subscribe('data', () => [], {
                        onReady: () => {
                            loadEntityDataInUse();
                        },
                    });
                },
            });

            // Give reactivity to search field, sort dropdown menu, page number in dashboard tab
            $scope.subscribe('trailDashboards', () => {
                return [
                    {
                        // how many items per page
                        limit: parseInt($scope.dashPerPage, 10),
                        // the number of items to start with
                        skip: parseInt(($scope.getReactively('dashPage') - 1) * $scope.dashPerPage, 10),
                        // the sorting of the collection
                        sort: $scope.getReactively('dashSort'),
                    },
                    $scope.getReactively('dashSearchText'),
                    $stateParams.trailId,
                ];
            });

            // Give reactivity to search field, sort dropdown menu, page number in slideshow tab
            $scope.subscribe('trailSlideshows', () => {
                return [
                    {
                        // how many items per page
                        limit: parseInt($scope.slidePerPage, 10),
                        // the number of items to start with
                        skip: parseInt(($scope.getReactively('slidePage') - 1) * $scope.slidePerPage, 10),
                        // the sorting of the collection
                        sort: $scope.getReactively('slideSort'),
                    },
                    $scope.getReactively('slideSearchText'),
                    $stateParams.trailId,
                ];
            });

            $scope.chartTypes = [{
                id: 0,
                type: 'gp',
                label: 'Gatherplot',
            }, {
                id: 1,
                type: 'bar',
                label: 'Bar Chart',
            }, {
                id: 2,
                type: 'pie',
                label: 'Pie Chart',
            }];

            /**
             * TO DEVELOPERS: SEE CONSTANT DECLARATION IN client/lib/helpers/
             */

            /* Gridster settings in Dashboard View */
            const TRAIL_GRIDSTER_OPT = GRIDSTER_OPT;
            TRAIL_GRIDSTER_OPT.resizable = {
                enabled: true,
                handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],

                // optional callback fired when resize is started
                start(event) {},

                // optional callback fired when item is resized
                resize(event) {},

                // optional callback fired when item is finished resizing
                stop(event, $element) {
                    $timeout(() => {
                        if ($scope.stateObjectsDashboard) {
                            const resizedCard = $element.scope().state;
                            const index = $scope.stateObjectsDashboard.findIndex((d) => {
                                return d.id === resizedCard.id;
                            });

                            if (resizedCard.type === 'md') {
                                // scale content size if not fit
                                scaleDashboardMdContent(resizedCard, index);
                            }
                        }
                    }, 10);
                },
            };

            $scope.gridsterOpts = TRAIL_GRIDSTER_OPT;
            $scope.customItemMap = GRIDSTER_CUSTOM_MAP;

            /* Nvd3 chart settings in Trail */
            // nvd3 chart configuration hashmap
            $scope.nvd3ChartConfigs = {};

            /* Discrete bar chart, make deep copies when need to modify properties */
            const barTrailConfig = angular.copy(NVD3_OPT_DISCRETEBAR);
            barTrailConfig.chart.barSelect = true;
            barTrailConfig.chart.discretebar = {
                dispatch: {
                    elementClick: (e) => {
                        if (d3.event.shiftKey) {
                            updateBarShiftSelection(e);
                        } else if (d3.event.ctrlKey || d3.event.metaKey || d3.event.altKey) {
                            updateBarCtrlSelection(e);
                        } else {
                            updateBarSingleSelection(e);
                        }
                    },
                },
            };

            /* Pie chart, make deep copies when need to modify properties */
            const pieTrailConfig = angular.copy(NVD3_OPT_PIE);
            pieTrailConfig.chart.sliceSelect = true; // enable data selection
            pieTrailConfig.chart.pie = {
                dispatch: {
                    elementClick: (e) => {
                        if (d3.event.shiftKey) {
                            updatePieShiftSelection(e);
                        } else if (d3.event.ctrlKey || d3.event.metaKey || d3.event.altKey) {
                            updatePieCtrlSelection(e);
                        } else {
                            updatePieSingleSelection(e);
                        }
                    },
                },
            };

            /* Nvd3 chart settings in Dashboard */
            // dashboard configs
            $scope.nvd3ChartConfigsDash = {};

            const disBarOptDash = angular.copy(NVD3_OPT_DISCRETEBAR);
            // disBarOptDash.chart.height = 420;

            const pieOptDash = angular.copy(NVD3_OPT_PIE);
            // pieOptDash.chart.height = 600;

            /* Nvd3 chart settings in Slideshow */
            /* Bar chart */
            $scope.disBarOptSlideFs = angular.copy(NVD3_OPT_DISCRETEBAR);
            $scope.disBarOptSlideFs.chart.height = 470;

            $scope.disBarOptSlidePreview = angular.copy(NVD3_OPT_DISCRETEBAR);
            $scope.disBarOptSlidePreview.chart.height = 800;
            $scope.disBarOptSlidePreview.chart.showValues = false;
            $scope.disBarOptSlidePreview.chart.showXAxis = false;
            $scope.disBarOptSlidePreview.chart.showYAxis = false;
            $scope.disBarOptSlidePreview.chart.tooltip = { enabled: false };

            /* Pie chart */
            $scope.pieOptSlideFs = angular.copy(NVD3_OPT_PIE);
            $scope.pieOptSlideFs.chart.height = 470;

            $scope.pieOptSlidePreview = angular.copy(NVD3_OPT_PIE);
            $scope.pieOptSlidePreview.chart.height = 800;
            $scope.pieOptSlidePreview.chart.showLabels = false;
            $scope.pieOptSlidePreview.chart.showLegend = false;
            $scope.pieOptSlidePreview.chart.growOnHover = false;
            $scope.pieOptSlidePreview.chart.tooltip = { enabled: false };

            /* Jsplumb settings */
            $scope.targetEndpointStyle = JSPLUMB_TARGET_ENDPOINT_STYLE;
            $scope.sourceEndpointStyle = JSPLUMB_SRC_ENDPOINT_STYLE;

            $scope.sortableOptions = {
                containment: '#sortable-container',
            };

            $scope.setTrailPublic = (isPublic) => {
                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        public: isPublic,
                    },
                });
            };

            $scope.logfunc = (type, message, data) => {
                // console.log("[logFunc] " + type + " " + message);
                logActivity(type, message, data);
            };

            $scope.removeIndex = (index, object) => {
                object.splice(index, 1);
            };

            /*
             * Remove a trail card from the current trail instance
             * if it is a trail card,
             * 1) update its parent's children count
             * 2) remove it from the global card array
             *
             * if it is a markdown card, remove it from the global card array
             *
             * @param {Number} index: the index of the card
             * @param {Object array} cards: the global card array
             */
            $scope.removeCard = (index, cards) => {
                // get the card object to be removed
                const itemToRemove = cards[index];

                // trail card can be removed only if it is a root or leaf
                if (itemToRemove.type === 'trail') {
                    // if it is a leaf
                    if (itemToRemove.parent) {
                        // child's connector uuid
                        const itemToRemoveTargetUuid = itemToRemove.targets[0].uuid;

                        const parentIndex = cards.findIndex((d) => {
                            return d.id === itemToRemove.parent;
                        });

                        if (parentIndex !== -1) {
                            const parent = cards[parentIndex];

                            // if the only child is deleted, the parent card becomes child card
                            if (parent.children.length === 1) {
                                Trails.update({ _id: $stateParams.trailId }, {
                                    $set: {
                                        [`cards.${parentIndex}.isParent`]: false,
                                    },
                                });
                            }

                            for (let i = 0; i < parent.children.length; i++) {
                                if (parent.children[i] === itemToRemove.id) {
                                    parent.children.splice(i, 1);
                                    break;
                                }
                            }

                            // remove child id
                            Trails.update({ _id: $stateParams.trailId }, {
                                $set: {
                                    [`cards.${parentIndex}.children`]: parent.children,
                                },
                            });

                            // remove parent's connection to the card to be removed
                            const parentSourceIndex = parent.sources[0].connections.indexOf(itemToRemoveTargetUuid);

                            if (parentSourceIndex !== -1) {
                                Trails.update({ _id: $stateParams.trailId }, {
                                    $unset: {
                                        [`cards.${parentIndex}.sources.0.connections.${parentSourceIndex}`]: 1,
                                    },
                                });

                                Trails.update({ _id: $stateParams.trailId }, {
                                    $pull: {
                                        [`cards.${parentIndex}.sources.0.connections`]: null,
                                    },
                                });
                            }
                        }
                    }
                }

                cards.splice(index, 1);

                /* mongodb open issue: may not be atomic */
                Trails.update({ _id: $stateParams.trailId }, {
                    $unset: {
                        [`cards.${index}`]: 1,
                    },
                });

                Trails.update({ _id: $stateParams.trailId }, {
                    $pull: {
                        cards: null,
                    },
                });
            };

            $scope.newCard = (entityId) => {
                if (entityId) {
                    if (trailData[entityId]) {
                        createCardFromEntity(entityId, DEFAULT_CHART);
                    } else {
                        loadDataAndCreateCard(entityId, null, null, null, null, null, 'start', DEFAULT_CHART);
                    }
                }
            };

            /* entity: the entity name that the parent entity pivots to */
            $scope.pivotCard = (link, card, index) => {
                if (card.chartType === (BAR_CHART || PIE_CHART)) {
                    if (!card.chartConfig.selection.dimName) {
                        Bert.alert('Please select some data before pivot.', 'danger', 'growl-top-right', 'fa-frown-o');
                        return;
                    }
                }

                if (link) {
                    const entityId = link.linksToEntityId;

                    if (trailData[entityId]) {
                        createCardByPivot(entityId, card.sources, card.dimsum.selectionSpace, card, index, link);
                    } else {
                        loadDataAndCreateCard(entityId, card.sources, card.dimsum.selectionSpace, card,
                            index, link, 'pivot', DEFAULT_CHART);
                    }
                }
            };

            /*
             * Filter a single card while preserving the parameterization
             *
             * @param {Object} card: the parent card to be filtered from
             * @param {Number} i: the index of the parent card
             */
            $scope.filterCard = (card, index) => {
                if (card.chartType === (BAR_CHART || PIE_CHART)) {
                    if (!card.chartConfig.selection.dimName) {
                        Bert.alert('Please select some data before filter.', 'danger', 'growl-top-right', 'fa-frown-o');
                        return;
                    }
                }

                createCardByFilter(card.entityId, card.sources, card.dimsum.selectionSpace, card, index);
            };

            $scope.comparePivot = (link, card, index) => {
                if (card.chartType === (BAR_CHART || PIE_CHART)) {
                    if (!card.chartConfig.selection.dimName) {
                        Bert.alert('Please select some data before pivot.', 'danger', 'growl-top-right', 'fa-frown-o');
                        return;
                    }
                }

                if (link) {
                    const entityId = link.linksToEntityId;
                    const parentLoc = {
                        left: card.x,
                        top: card.y,
                    };

                    // TODO
                }
            };

            $scope.compareSelect = (parent) => {
                if (!$scope.isCompareSelectMode && !$scope.currCompareRootId) {
                    // turn on compare select mode
                    $scope.isCompareSelectMode = !$scope.isCompareSelectMode;
                    $scope.currCompareRootId = parent.id;
                    doDfs(parent, $scope.isCompareSelectMode, setCompareModeOnNode);
                } else if (parent.id === $scope.currCompareRootId) {
                        // if the compare root is from the current parent card, turn off compare select mode
                        $scope.isCompareSelectMode = !$scope.isCompareSelectMode;
                        $scope.currCompareRootId = null;
                        $scope.comparePathSubrootId = null;
                        $scope.isComparePath = false;
                        doDfs(parent, $scope.isCompareSelectMode, setCompareModeOnNode);
                    }
            };

            $scope.compareSelectPath = (card) => {
                // the root card of the comparison path
                const compareRoot = getCardById($scope.currCompareRootId);
                let curr = card;
                let parent = getCardById(card.parent);

                // after the current card is clicked and checked
                // select a new chain or extend the existing chain
                if (curr.isCompare === true) {
                    while (curr.compareOn && (parent !== null)) {
                        if (parent && (parent.id === $scope.currCompareRootId)) {
                            $scope.isComparePath = true;
                            $scope.comparePathSubrootId = curr.id;
                        }

                        curr = parent;
                        parent = getCardById(parent.parent);
                        curr.isCompare = true;
                    }

                    // disable cards in other subtree
                    compareRoot.children.forEach((cid) => {
                        const child = getCardById(cid);

                        if (child.id !== $scope.comparePathSubrootId) {
                            doDfs(child, true, disableCompareCard);
                        }
                    });
                } else if (curr.isCompare === false) {
                    // if the subroot is being unchecked, the path is deselected and enable compare on other subtrees
                    if (curr.id === $scope.comparePathSubrootId) {
                        compareRoot.children.forEach((cid) => {
                            const child = getCardById(cid);

                            if (child.id !== curr.id) {
                                doDfs(child, false, disableCompareCard);
                            }
                        });
                    }

                    // uncheck the current card and all checked card in its subtree
                    doDfs(curr, false, uncheckCompareCard);

                    $scope.isComparePath = false;
                    $scope.comparePathSubrootId = null;

                    compareRoot.children.forEach((cid) => {
                        const child = getCardById(cid);

                        if (child && child.isCompare) {
                            $scope.isComparePath = true;
                            $scope.comparePathSubrootId = child.id;
                        }
                    });
                }
            };

            $scope.checkCompareSelectStatus = (card) => {
                if ($scope.isCompareSelectMode) {
                    if (card.id === $scope.currCompareRootId) {
                        return true;
                    }
                } else if (card.children.length > 0) {
                        return true;
                    }

                return false;
            };

            /*
             * Filter by duplicating the parameterization of a selected chain of cards,
             * and it is only available on parent card.
             *
             * The compare filter action only becomes available when a valid comparison chain gets selected.
             *
             * @param {Object} parent: the parent card
             * @param {Number} i: the index of the parent card
             *
             */
            $scope.compareFilter = (parent, i) => {
                /*
                * From the comparison root node, create a comparison chain like this:
                * 1. Create the comparison subroot card by copying the root card's parameterization,
                * with a different subset of data. (normal filtering)
                * 2. Change comparison subroot card's parameterization to subroot card's.
                *
                * For the rest of the comparison chain, for each comparison card
                * 3. Create new card by copying its parent's parameterization and its comparison counterpart's queries,
                * 4. Change new card's parameterization to its comparison counterpart's
                * */

                const children = parent.children; // get all children

                const subroot = getCardById($scope.comparePathSubrootId);

                $scope.currSubtreeCoord = []; // reset coordinate list

                getSubtreeCoordsDfs(subroot); // update coordinate list

                const newSubtreePos = calcNewSubtreePos(parent, $scope.currSubtreeCoord);

                if (children.length > 0) {
                    createCardDfs(subroot, parent, i, newSubtreePos);
                }

                // reset compare selection mode
                $scope.isCompareSelectMode = false;
                $scope.currCompareRootId = null;
                $scope.comparePathSubrootId = null;
                $scope.isComparePath = false;
                doDfs(parent, $scope.isCompareSelectMode, setCompareModeOnNode);
            };

            /* Update chart type */
            $scope.updateChartType = (card, index) => {
                if (card.chartType) {
                    let sIndex = -1; // index of the card in the slideshow list

                    if ($scope.fullscreenObjects.length > 0) {
                        sIndex = $scope.fullscreenObjects.findIndex((f) => {
                            return f.id === card.id;
                        });
                    }

                    switch (card.chartType) {
                        case 'gp': { // gatherplot
                            if (card.entityData && (card.entityData.length > 0)) {
                                // get selected documents
                                let selectedDocuments = Meteor.utilitiesHelper.fromIdListToObjectList(card.entityData,
                                    trailData[card.entityId]);

                                updateDataUsedByGatherplot(card.id, selectedDocuments);

                                selectedDocuments = null; // free space
                            } else {
                                // get all documents
                                initGatherplotData(card, trailData[card.entityId]);
                                updateDataUsedByGatherplot(card.id, trailData[card.entityId]);
                            }
                            break;
                        }
                        case 'bar': { // bar chart
                            if (card.entityData && (card.entityData.length > 0)) {
                                // initialize chart data of the selected type
                                let selectedDocuments = trailData[card.entityId].filter((d) => {
                                    return card.entityData.indexOf(d._id) !== -1;
                                });

                                initBarData(card, selectedDocuments);

                                updateDBChartData(index, card.chartData);

                                selectedDocuments = null;

                                const selectedDim = card.chartConfig.selection.dimName;

                                if (selectedDim) {
                                    updateDataUsedByChart(card.id, card.chartData[selectedDim]);
                                } else {
                                    updateDataUsedByChart(card.id, []);
                                }
                            } else {
                                initBarData(card, trailData[card.entityId]);
                                updateDBChartData(index, card.chartData);
                                updateDataUsedByChart(card.id, []);
                            }

                            // resize so pie labels get rendred correctly
                            windowResize(50);
                            break;
                        }
                        case 'pie': { // pie chart
                            if (card.entityData && (card.entityData.length > 0)) {
                                let selectedDocuments = trailData[card.entityId].filter((d) => {
                                    return card.entityData.indexOf(d._id) !== -1;
                                });

                                initPieData(card, selectedDocuments);

                                updateDBChartData(index, card.chartData);

                                selectedDocuments = null;

                                const selectedDim = card.chartConfig.selection.dimName;

                                if (selectedDim) {
                                    updateDataUsedByChart(card.id, card.chartData[selectedDim]);
                                } else {
                                    updateDataUsedByChart(card.id, []);
                                }
                            } else {
                                initPieData(card, trailData[card.entityId]);
                                updateDBChartData(index, card.chartData);
                                updateDataUsedByChart(card.id, []);
                            }

                            // resize so pie labels get rendered correctly
                            windowResize(50);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    updateDBChartType(index, card.chartType);
                    updateDBSlideshowChartType(sIndex, card.chartType);
                }
            };

            $scope.onConnection = (instance, connection, targetUUID, sourceUUID) => {
                angular.forEach($scope.trail.cards, (card) => {
                    angular.forEach(card.sources, (source) => {
                        if (source.uuid === sourceUUID) {
                            if (typeof source.connections === 'undefined') source.connections = [];

                            source.connections.push({
                                uuid: targetUUID,
                            });

                            // scope apply required because this event handler is outside of the angular domain
                            $scope.$apply();
                        }
                    });
                });
            };

            $scope.getStateFromId = (id) => {
                const result = $scope.trail.cards.find((d) => {
                    return d.id === id;
                });

                return result;
            };

            $scope.getEntityLabel = (id) => {
                // search locally for the entity object
                const entityObj = Meteor.utilitiesHelper.getEntityById($scope.entities, id);

                if (entityObj) {
                    return entityObj.label;
                }
                    return 'Missing Entity';
            };

            $scope.getEntityDimensions = (id) => {
                // search locally for the entity object
                const entityObj = Meteor.utilitiesHelper.getEntityById($scope.entities, id);

                if (entityObj) {
                    return entityObj.dimensions;
                }
                    return [];
            };

            $scope.getEntityLinks = (id) => {
                // search locally for the entity object
                const entityObj = Meteor.utilitiesHelper.getEntityById($scope.entities, id);
                const links = []; // to hold all the processed directed/undirected links
                const selfJoinLinks = []; // an array to hold all the processed self join links
                const pairLinksHash = {}; // a hash array to hold all the pair links

                if (entityObj) {
                    // create display texts for each link
                    entityObj.links.forEach((l) => {
                        const toEntity = Meteor.utilitiesHelper.getEntityById($scope.entities, l.linksToEntityId);
                        if (toEntity) {
                            if (l.directed) {
                                if (l.selfJoinKey) {
                                    if (pairLinksHash[l.selfJoinKey]) {
                                        pairLinksHash[l.selfJoinKey].push(l);
                                    } else {
                                        pairLinksHash[l.selfJoinKey] = [l];
                                    }
                                } else {
                                    l.displayText = `${toEntity.label} (->) `;
                                    links.push(l);
                                }
                            } else {
                                l.displayText = toEntity.label;
                                links.push(l);
                            }
                        }
                    });

                    if (pairLinksHash) {
                        Object.keys(pairLinksHash).forEach((pk) => {
                            const tempPairLinks = pairLinksHash[pk]; // get a pair of links

                            if (tempPairLinks.length === 2) {
                                const link1 = tempPairLinks[0];
                                const link2 = tempPairLinks[1];
                                const toEntity = Meteor.utilitiesHelper.getEntityById(
                                    $scope.entities,
                                    link1.linksToEntityId);

                                if (link1.fromKey && link1.selfJoinKey) {
                                    const joinKeys = link1.selfJoinKey.split('-'); // get the join keys
                                    if (joinKeys.length > 0) {
                                        const fromKey = joinKeys[0];

                                        // determine which link should have which direction
                                        if (link1.fromKey === fromKey) { // link 1 should have ->
                                            link1.displayText = `${toEntity.label} (->) `;
                                            link2.displayText = `${toEntity.label} (<-) `;
                                        } else { // link 2 should have ->
                                            link1.displayText = `${toEntity.label} (<-) `;
                                            link2.displayText = `${toEntity.label} (->) `;
                                        }
                                    }
                                }

                                selfJoinLinks.push(link1);
                                selfJoinLinks.push(link2);
                            }
                        });
                    }

                    return links.concat(selfJoinLinks);
                }
            };

            $scope.getFullScreenObjLabel = (fullScreenObj) => {
                if (fullScreenObj.type === 'md') {
                    if (fullScreenObj.subtitle.length > 15) {
                        return `${fullScreenObj.subtitle.substring(0, 13)}...`;
                    }
                    return fullScreenObj.subtitle;
                }  // trail card
                    return $scope.getEntityLabel(fullScreenObj.entityId);
            };

            /* Update the coordinate of a trail card, dimsum.source will be added */
            $scope.updateTrailPosition = (card, index) => {
                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        [`cards.${index}`]: angular.copy(card),
                    },
                });
            };

            /* Reset the selection of data points of a trail card*/
            $scope.resetTrailSelection = (card, index) => {
                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        [`cards.${index}.dimsum`]: card.dimsum,
                    },
                });
            };

            /* Update markdown content in trail view*/
            $scope.updateTrailMarkdownContent = (card, index) => {
                if (card.slide) {
                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.slide`]: card.slide,
                        },
                    });
                }
            };

            /* Update markdown content in dashboard view*/
            $scope.updateDashMarkdownContent = (card, index) => {
                if (card.slide) {
                    Dashboards.update({ _id: $scope.dashboard._id }, {
                        $set: {
                            [`grids.${index}.slide`]: card.slide,
                        },
                    });
                }

                windowResize(10);
            };

            /* Update slide content(subtitle)*/
            $scope.updateSlideshowSubtitle = (index) => {
                const fullScreenObject = $scope.fullscreenObjects[index];

                Slideshows.update({ _id: $scope.slideshow._id }, {
                    $set: {
                        [`slideshowContent.${index}.subtitle`]: fullScreenObject.subtitle,
                    },
                });
            };

            /* Update slide content(body)*/
            $scope.updateSlideshowMarkdown = (index) => {
                const fullScreenObject = $scope.fullscreenObjects[index];
                let processedMd = fullScreenObject.markdown;
                processedMd = filterMarkdownContent(processedMd);

                if (processedMd) {
                    Slideshows.update({ _id: $scope.slideshow._id }, {
                        $set: {
                            [`slideshowContent.${index}.markdown`]: processedMd,
                        },
                    });
                }

                // scale content if it does not fit into the container anymore
                windowResize(10);
            };


            /* Update nvd3 chart dimensions */
            $scope.updateChartDim = (card, index) => {
                // find the selected attribute
                const plotData = card.chartData;
                const selectedDim = card.chartConfig.selection.dimName;

                if (selectedDim) {
                    if (plotData && Object.prototype.hasOwnProperty.call(plotData, selectedDim)) {
                        // undate global plot data
                        updateDataUsedByChart(card.id, plotData[selectedDim]);
                        // update dimension in DB
                        updateDBChartDim(index, selectedDim);
                    }
                } else { // undefined, nvd3 will print "No Data Available"
                    // undate global plot data
                    updateDataUsedByChart(card.id, []);
                    // update dimension in DB
                    updateDBChartDim(index, '');
                }

                windowResize(50);
            };

            /* Update gatherplot dimension, x axis*/
            $scope.updateGatherConfigXDim = (card, index) => {
                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        [`cards.${index}.gatherConfig.params.xDim`]: card.gatherConfig.params.xDim,
                    },
                });
            };

            /* Update gatherplot dimension, y axis*/
            $scope.updateGatherConfigYDim = (card, index) => {
                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        [`cards.${index}.gatherConfig.params.yDim`]: card.gatherConfig.params.yDim,
                    },
                });
            };

            /* Update gatherplot dimension, color axis*/
            $scope.updateGatherConfigColorDim = (card, index) => {
                initGatherColorCoding(card, index);

                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        [`cards.${index}.gatherConfig.params.colorDim`]: card.gatherConfig.params.colorDim,
                    },
                });
            };

            /* Update gatherplot dimension, mode checkbox*/
            $scope.updateGatherConfigMode = (card, index) => {
                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        [`cards.${index}.gatherConfig.params.relativeMode`]: card.gatherConfig.params.relativeMode,
                    },
                });
            };

            /* Methods for updating the dimensions from fullscreen object */

            /* Update nvd3 chart dimensions */
            $scope.fullscreenUpdateChartDim = (card) => {
                const index = $scope.trail.cards.findIndex((s) => {
                    return s.id === card.id;
                });

                // find the selected attribute
                const plotData = card.chartData;
                const selectedDim = card.chartConfig.selection.dimName;

                if ((index !== -1) && plotData && selectedDim) {
                    if (Object.prototype.hasOwnProperty.call(plotData, selectedDim)) {
                        // update plot data to re-redner
                        updateDataUsedByChart(card.id, plotData[selectedDim]);

                        // update trail objects
                        $scope.trail.cards[index] = card;

                        // update dimension in DB
                        updateDBChartDim(index, selectedDim);
                    }
                }
            };

            $scope.fullscreenSetXDim = (card) => {
                const index = $scope.trail.cards.findIndex((s) => {
                    return s.id === card.id;
                });

                const selectedDim = card.gatherConfig.params.xDim;

                if ((index !== -1) && selectedDim) {
                    // update trail objects
                    $scope.trail.cards[index] = card;

                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.gatherConfig.params.xDim`]: selectedDim,
                        },
                    }, (error) => {
                        if (error) {
                            // logActivity("fullscreenSetXDim", 'failed to update dimension!', selectedDim);
                            console.log('fullscreenSetXDim: failed to update dimension!');
                        } else {
                            // logActivity("fullscreenSetXDim", 'updated successfully!', selectedDim);
                        }
                    });
                }
            };

            $scope.fullscreenSetYDim = (card) => {
                const index = $scope.trail.cards.findIndex((s) => {
                    return s.id === card.id;
                });

                const selectedDim = card.gatherConfig.params.yDim;

                if ((index !== -1) && selectedDim) {
                    // update trail objects
                    $scope.trail.cards[index] = card;

                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.gatherConfig.params.yDim`]: selectedDim,
                        },
                    }, (error) => {
                        if (error) {
                            // to replace with log function
                            console.log('fullscreenSetYDim: failed to update dimension!');
                        } else {
                            // to replace with log function
                        }
                    });
                }
            };

            $scope.fullscreenSetColorDim = (card) => {
                const index = $scope.trail.cards.findIndex((s) => {
                    return s.id === card.id;
                });

                const selectedDim = card.gatherConfig.params.colorDim;

                if ((index !== -1) && selectedDim) {
                    // update trail objects
                    $scope.trail.cards[index] = card;

                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.gatherConfig.params.colorDim`]: selectedDim,
                        },
                    }, (error) => {
                        if (error) {
                            // to replace with log function
                            console.log('fullscreenSetColorDim: failed to update dimension!');
                        } else {
                            // to replace with log function
                        }
                    });
                }
            };

            $scope.fullscreenSetMode = (card) => {
                const index = $scope.trail.cards.findIndex((s) => {
                    return s.id === card.id;
                });

                const selectedMode = card.gatherConfig.params.relativeMode;

                if ((index !== -1) && selectedMode) {
                    // update trail objects
                    $scope.trail.cards[index] = card;

                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.gatherConfig.params.relativeMode`]: selectedMode,
                        },
                    }, (error) => {
                        if (error) {
                            // to replace with log function
                            console.log('fullscreenSetMode: failed to update dimension!');
                        } else {
                            // to replace with log function
                        }
                    });
                }
            };

            $scope.zoomToFit = () => {
                const $panzoom = $('.panzoom');
                const $panelBody = $('.trail-panel-body');
                const cardMaxWidth = 1000;
                const cardMaxHeight = 1000;

                if (!$scope.trail) {
                    return;
                }

                if ($scope.isTrail && !$scope.trail.cards) {
                    return;
                }

                if ($panzoom.length === 0) return;

                // logActivity("zoomToFit", 'null', 'null');

                // a sorted list of coordinate x of all cards
                const extentX = d3.extent($scope.trail.cards, (d) => {
                    return d.x;
                });

                // a sorted list of coordinate y of all cards
                const extentY = d3.extent($scope.trail.cards, (d) => {
                    return d.y;
                });

                // calculate panel dimensions
                const width = $panzoom[0].offsetWidth;
                const height = $panelBody[0].offsetHeight;

                // calculate the bounding box of furthest cards
                const boxWidth = extentX[1] - extentX[0];
                const boxHeight = (extentY[1] + cardMaxHeight) - extentY[0];

                const horizScale = boxWidth ? (width / boxWidth) : (width / cardMaxWidth);
                const verScale = boxHeight ? (height / boxHeight) : (height / cardMaxHeight);

                const calculatedScale = Math.min(horizScale, verScale);

                const panX = -1 * (extentX[0] + (boxWidth / 2));
                const panY = -1 * ((extentY[0] + (boxHeight / 2)) - cardMaxHeight);

                const zoomFactor = calculatedScale * 0.6;

                $panzoom.panzoom('zoom', 1, {
                    animate: true,
                    focal: {
                        clientX: 0,
                        clientY: 0,
                    },
                });

                $panzoom.panzoom('pan', panX, panY, {
                    animate: true,
                    relative: false,
                });

                $panzoom.panzoom('zoom', zoomFactor, {
                    focal: {
                        clientX: width / 2,
                        clientY: height * (0.8 - verScale),
                    },
                    animate: true,
                });

                // resize so pie labels will be correctly rendered
                windowResize(100);
            };

            $('.panzoom').on('click', (e) => {
                if (e.which === 2) {
                    // console.log(e);
                    $scope.zoomToFit();
                }
            });

            $scope.handleMiddleMouse = (e) => {
                // console.log(e);
                if (e.which === 2) {
                    $scope.zoomToFit();
                }
            };

            $scope.handleKeyboard = (e) => {
                if (e.keyCode === 39) {
                    $scope.nextCard();
                } else if (e.keyCode === 37) {
                    $scope.previousCard();
                }
            };

            $scope.setTrailMode = () => {
                // console.log("Switch to Trail Mode!");

                $scope.canvasMode = 'trail';
                $scope.isTrail = true;
                $scope.zoomlevel = 100;

                windowResize(50);

                $timeout(() => {
                    $scope.zoomToFit();
                }, ZOOM_TIMEOUT);

                logActivity('setTrailMode', '', '');
            };

            $scope.setDashboardMode = () => {
                // console.log("Switch to Dashboard Mode!");

                $scope.isDashboardList = true;
                $scope.isTrail = false;

                logActivity('setDashboardMode', '', '');
            };

            $scope.setSlideshowMode = () => {
                // console.log("Switch to Slideshow Mode!");

                $scope.isSlideshowList = true;
                $scope.isTrail = false;

                logActivity('setSlideshowMode', '', '');
            };

            $scope.backToDashboardList = () => {
                $scope.isDashboardList = true;

                logActivity('Back to Dashboard List', '', '');
            };

            $scope.backToSlideshowList = () => {
                $scope.isSlideshowList = true;
                logActivity('Back to Slideshow List', '', '');
            };

            /* Initialize a dashboard */
            $scope.openDashboard = (dashboard) => {
                $scope.canvasMode = 'dashboard';
                $scope.isDashboardList = false; // disable list view
                $scope.dashboardName = dashboard.name;

                if (dashboard.grids) { // if any item is imported into the dashboard
                    $scope.stateObjectsDashboard = dashboard.grids;
                    $scope.dashboard = dashboard;

                    // if entity is removed, reset properties of the dashboard card and show missing data message
                    if ($scope.dashboard.grids.length > 0) {
                        $scope.dashboard.grids.forEach((grid, index) => {
                            initNvd3ChartConfigDash(grid.id);
                            initNvd3ChartHeightDash(grid);

                            if (grid.entityId) {
                                if (($scope.entities.length > 0) &&
                                    (!Meteor.utilitiesHelper.getEntityById($scope.entities, grid.entityId))) {
                                    const cardNoData = createTrailCardNoData(grid);

                                    Dashboards.update({ _id: $scope.dashboard._id }, {
                                        $set: {
                                            [`grids.${index}`]: cardNoData,
                                        },
                                    });
                                }
                            }
                        });
                    }

                    windowResize(500);

                    logActivity('openDashboard', 'Open an existing dashboard', dashboard.name);
                } else { // if this is a new dashboard
                    logActivity('openDashboard', 'Open a new dashboard', dashboard.name);
                }
            };

            /* Initialize a slideshow */
            $scope.openSlideshow = (slideshow) => {
                $scope.isSlideshowList = false; // disable list view
                $scope.slideshow = slideshow;
                $scope.canvasMode = 'slideshow';
                $scope.zoomlevel = 100;

                if (slideshow.slideshowContent) {
                    if (slideshow.slideshowContent.length > 0) {
                        $scope.slideshow.slideshowContent.forEach((slide, index) => {
                            if (slide.type === 'trail') {
                                if (slide.entityId) {
                                    // if entity no more exists, reset slide
                                    if (($scope.entities.length > 0) &&
                                        (!Meteor.utilitiesHelper.getEntityById($scope.entities, slide.entityId))) {
                                        const stateMissingData = {};

                                        stateMissingData.type = slide.type;
                                        stateMissingData.id = slide.id;
                                        stateMissingData.subtitle = '';

                                        Slideshows.update({ _id: $scope.slideshow._id }, {
                                            $set: {
                                                [`slideshowContent.${index}`]: stateMissingData,
                                            },
                                        });

                                        slide = stateMissingData;
                                    }
                                }
                            }
                        });

                        let card;

                        // if the first slide is a trail
                        if (slideshow.slideshowContent[0].type === 'trail') {
                            // get the trail card
                            card = getCardById(slideshow.slideshowContent[0].id);
                        // if it is markdown
                        } else if (slideshow.slideshowContent[0].type === 'md') {
                            // get the first slide
                            card = slideshow.slideshowContent[0];
                        }

                        $scope.fullscreenState = card;
                        $scope.fullscreenObjects = slideshow.slideshowContent;

                        logActivity('openSlideshow', 'Open an existing slideshow', slideshow.name);
                    }
                } else { // create a new title slide
                    $scope.fullscreenState = createTitleSlide();
                    $scope.fullscreenObjects = [$scope.fullscreenState];
                    slideshow.slideshowContent = $scope.fullscreenObjects;

                    logActivity('openSlideshow', 'Open a new slideshow', slideshow.name);
                }

                initializeSlideshowPreviewScale();

                // right now this event is also helpful in rendering nvd3 charts
                windowResize(10);
            };

            $scope.removeFromDashboard = (card, index) => {
                if ($scope.dashboard) {
                    $scope.stateObjectsDashboard.splice(index, 1);

                    Dashboards.update({ _id: $scope.dashboard._id }, {
                        $unset: {
                            [`grids.${index}`]: 1,
                        },
                    });

                    Dashboards.update({ _id: $scope.dashboard._id }, {
                        $pull: {
                            grids: null,
                        },
                    });

                    logActivity('removeFromDashboard', `Remove dashboard at ${index}`, card.id);
                } else {
                    console.log('removeFromDashboard: Dashboard not found!');
                }
            };

            $scope.removeFromSlideshow = (card, index) => {
                if ($scope.slideshow) {
                    if ($scope.fullscreenObjects.length > 0) {
                        $scope.fullscreenObjects.splice(index, 1);

                        Slideshows.update({ _id: $scope.slideshow._id }, {
                            $unset: {
                                [`slideshowContent.${index}`]: 1,
                            },
                        });

                        Slideshows.update({ _id: $scope.slideshow._id }, {
                            $pull: {
                                slideshowContent: null,
                            },
                        });

                        logActivity('removeFromSlideshow', index, card.id);

                        const newLen = $scope.fullscreenObjects.length;

                        /* after removing a slide,
                         * if there is no subsequent slides, set to previous slide
                         * else the next slide becomes the current slide
                         */
                        if ($scope.slideIndex >= newLen) {
                            $scope.previousCard();
                        } else if ($scope.fullscreenObjects[$scope.slideIndex].type === 'trail') {
                                $scope.fullscreenState = $scope.getStateFromId(
                                    $scope.fullscreenObjects[$scope.slideIndex].id);
                            } else {
                                $scope.fullscreenState = $scope.fullscreenObjects[$scope.slideIndex];
                            }
                    }

                    windowResize(10);
                } else {
                    console.log('removeFromSlideshow: Slideshow not found!');
                }
            };

            $scope.setFullScreen = (fullscreenState) => {
                logActivity('setFullScreen', '', fullscreenState.id);
                $scope.isFullScreen = fullscreenState;
            };

            $scope.previousCard = () => {
                if ($scope.fullscreenObjects.length > 0) {
                    if ($scope.slideIndex > 0) {
                        $scope.slideIndex -= 1;

                        if ($scope.fullscreenObjects[$scope.slideIndex].type === 'trail') {
                            $scope.fullscreenState = $scope.getStateFromId(
                                $scope.fullscreenObjects[$scope.slideIndex].id);
                        } else {
                            $scope.fullscreenState = $scope.fullscreenObjects[$scope.slideIndex];
                        }
                    }
                } else {
                    $scope.fullscreenState = undefined;
                }

                windowResize(50);

                logActivity('previousCard', '', {
                    id: $scope.fullscreenState ? $scope.fullscreenState.id : undefined,
                });
            };

            $scope.nextCard = () => {
                if ($scope.fullscreenObjects.length > 0) {
                    if ($scope.slideIndex < $scope.fullscreenObjects.length - 1) {
                        $scope.slideIndex += 1;

                        if ($scope.fullscreenObjects[$scope.slideIndex].type === 'trail') {
                            $scope.fullscreenState = $scope.getStateFromId(
                                $scope.fullscreenObjects[$scope.slideIndex].id);
                        } else {
                            $scope.fullscreenState = $scope.fullscreenObjects[$scope.slideIndex];
                        }
                    }
                } else {
                    $scope.fullscreenState = undefined;
                }

                windowResize(50);

                logActivity('nextCard', '', {
                    id: $scope.fullscreenState ? $scope.fullscreenState.id : undefined,
                });
            };

            $scope.switchCard = (card, seq) => {
                $scope.slideIndex = seq;

                if (card.type === 'trail') {
                    $scope.fullscreenState = $scope.getStateFromId(card.id);
                } else {
                    $scope.fullscreenState = card;
                }

                windowResize(50);

                logActivity('switchCard', '', {
                    id: $scope.fullscreenState ? $scope.fullscreenState.id : undefined,
                });
            };

            /*
                Upon clicking the settings icon of a dashboard card,
                resume to Trail View and set "selectedCard" of every trail cards
                such that only the selected card will be highlighted
            */
            $scope.findCardInTrail = (card) => {
                logActivity('findCardInTrail', '', {
                    id: card.id,
                });

                // $scope.isTrail = true;

                $scope.setTrailMode();

                $scope.trail.cards.forEach((d, index) => {
                    if (d.id === card.id) {
                        d.selectedCard = true;
                    } else {
                        d.selectedCard = false;
                    }

                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.selectedCard`]: d.selectedCard,
                        },
                    });
                });

                $timeout(() => {
                    // console.log('Zoom to fit');
                    $scope.zoomToFit();
                }, ZOOM_TIMEOUT);
            };

            $scope.isCurrentSlide = (card, seq) => {
                // console.log(card);

                if (seq === $scope.slideIndex) {
                    return true;
                }
                return false;
            };

            $scope.dashPageChanged = (newPage) => {
                $scope.dashPage = newPage;
            };

            $scope.slidePageChanged = (newPage) => {
                $scope.slidePage = newPage;
            };

            $scope.addDashboard = () => {
                if ($scope.newDashboard.name) {
                    $scope.newDashboard.trailId = $stateParams.trailId;
                    $scope.newDashboard.dataSource = 'trails';
                    $scope.newDashboard.ownerId = $scope.currentUser._id;
                    $scope.newDashboard.grids = [];
                    Dashboards.insert($scope.newDashboard);

                    $scope.newDashboard = {
                        name: '',
                    };

                    logActivity('addDashboard', '', {
                        newDashboardname: $scope.newDashboard.name,
                    });
                } else {
                    Bert.alert('Name empty! Please enter something.', 'danger', 'growl-top-right', 'fa-frown-o');
                }
            };

            $scope.removeDashboard = (dashboard) => {
                Dashboards.remove({ _id: dashboard._id });

                logActivity('removeDashboard', '', {
                    dashboardname: dashboard.name,
                });
            };

            $scope.addSlideshow = () => {
                if ($scope.newSlideshow.name) {
                    $scope.newSlideshow.trailId = $stateParams.trailId;
                    $scope.newSlideshow.dataSource = 'trails';
                    $scope.newSlideshow.ownerId = $scope.currentUser._id;
                    Slideshows.insert($scope.newSlideshow);

                    $scope.newSlideshow = {
                        name: '',
                    };

                    logActivity('addSlideshow', '', {
                        newSlideshowname: $scope.newSlideshow.name,
                    });
                } else {
                    Bert.alert('Name empty! Please enter something.', 'danger', 'growl-top-right', 'fa-frown-o');
                }
            };

            $scope.removeSlideshow = (slideshow) => {
                Slideshows.remove({ _id: slideshow._id });

                logActivity('removeDashboard', '', {
                    Dashboardname: slideshow.name,
                });
            };

            $scope.addSlideshowModal = () => {
                const modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'client/trails/trails-detail/import-trail-modal.html',
                    controller: 'ImportTrailModalCtrl',
                    size: 'lg',
                    resolve: {
                        entities: () => {
                            return $scope.entities;
                        },
                        entityDataObjectsList: () => {
                            return $scope.entityDataObjectsList;
                        },
                        cards: () => {
                            return trailDataCopy();
                        },
                    },
                });

                modalInstance.result.then((result) => {
                    const type = result.type;
                    const selectedState = result.slide;

                    let newFullScreenObject = {};

                    if (type === 'trail') {
                        // do not add duplicate because of one source
                        if (!isSlideAdded(selectedState.id)) {
                            if (!selectedState.subtitle) {
                                selectedState.subtitle = `${selectedState.gatherConfig.params.xDim}` +
                                    `vs ${selectedState.gatherConfig.params.yDim}`;
                            }

                            newFullScreenObject = {
                                type: 'trail',
                                chartType: selectedState.chartType,
                                id: selectedState.id,
                                entityId: selectedState.entityId, // for retrieving the updated entity name
                                subtitle: selectedState.subtitle,
                            };
                        } else {
                            Bert.alert('Unable to add duplicate trail, please try add a different one',
                                'danger', 'growl-top-right', 'fa-frown-o');
                            return;
                        }
                    } else if (type === 'md') {
                        const processedMd = filterMarkdownContent(selectedState);

                        newFullScreenObject = {
                            type: 'md',
                            markdown: processedMd || selectedState,
                            label: 'Markdown',
                            subtitle: 'Markdown',
                            scaleFactor: 1,
                        };
                    }

                    $scope.fullscreenObjects.push(newFullScreenObject);

                    newFullScreenObject = angular.copy(newFullScreenObject);

                    Slideshows.update({ _id: $scope.slideshow._id }, {
                        $push: {
                            slideshowContent: newFullScreenObject,
                        },
                    }, (error) => {
                        if (error) {
                            console.log('addSlideshowModal: update slideshow content failed!');
                        }
                    });

                    // after adding a new slide, set focus to it
                    $scope.fullscreenState = newFullScreenObject;
                    $scope.slideIndex = $scope.fullscreenObjects.length - 1;

                    windowResize(10);

                    logActivity('addSlideshowModal', '', {
                        type,
                        newCard: newFullScreenObject,
                    });
                }, () => {
                    // console.log('Modal dismissed at: ' + new Date());
                });
            };

            /* Reset dashboard data by copying from trail view */
            $scope.resetDashboardCards = (dashboard) => {
                // TODO
            };

            $scope.addDashboardModal = () => {
                const modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'client/trails/trails-detail/import-trail-modal.html',
                    controller: 'ImportTrailModalCtrl',
                    size: 'lg',
                    resolve: {
                        entities: () => {
                            return $scope.entities;
                        },
                        entityDataObjectsList() {
                            return $scope.entityDataObjectsList;
                        },
                        cards() {
                            return trailDataCopy();
                        },
                    },
                });

                modalInstance.result.then((result) => {
                    createDashboardCard(result);

                    $timeout(() => {
                        if ($scope.stateObjectsDashboard) {
                            const index = $scope.stateObjectsDashboard.length - 1;
                            const newCard = $scope.stateObjectsDashboard[index];

                            // scale content size if not fit
                            scaleDashboardMdContent(newCard, index);
                        }
                    }, 10);
                }, () => {
                    // console.log('Modal dismissed at: ' + new Date());
                });
            };

            function createDashboardCard(result) {
                const type = result.type;

                if (type === 'trail') {
                    const selectedState = angular.copy(result.slide); // get rid of $$hashkey

                    if (!selectedState.gridster) {
                        selectedState.gridster = {
                            size: {
                                x: 2,
                                y: 2,
                            },
                            position: {
                                row: 0,
                                col: 0,
                            },
                            minSize: {
                                x: 1,
                                y: 1,
                            },
                        };
                    } else {
                        if (!selectedState.gridster.position) {
                            selectedState.gridster.position = {
                                row: 0,
                                col: 0,
                            };
                        }

                        if (!selectedState.gridster.size) {
                            selectedState.gridster.size = {
                                x: 2,
                                y: 2,
                            };
                        }

                        if (!selectedState.gridster.minSize) {
                            selectedState.gridster.minSize = {
                                x: 1,
                                y: 1,
                            };
                        }
                    }

                    $scope.stateObjectsDashboard.push(selectedState);

                    Dashboards.update({ _id: $scope.dashboard._id }, {
                        $push: {
                            grids: selectedState,
                        },
                    });

                    initNvd3ChartConfigDash(selectedState.id);
                    initNvd3ChartHeightDash(selectedState);

                    // console.log("Added " + selectedState);
                } else if (type === 'md') {
                    if (result.slide) {
                        const processedMd = filterMarkdownContent(result.slide);
                        if (processedMd) result.slide = processedMd;
                    }

                    result.gridster = {
                        size: {
                            x: 2,
                            y: 2,
                        },
                        position: {
                            row: 0,
                            col: 0,
                        },
                        minSize: {
                            x: 1,
                            y: 1,
                        },
                    };

                    result.id = getNextUUID();
                    result.scaleFactor = 1;

                    $scope.stateObjectsDashboard.push(result);

                    Dashboards.update({ _id: $scope.dashboard._id }, {
                        $push: {
                            grids: result,
                        },
                    });
                }
            }

            function scaleDashboardMdContent(dashMd, mdIndex) {
                if (mdIndex !== -1) {
                    let scaleFactor = 0;
                    const $mdObjs = $('.dash-md-content'); // get all markdown content DOMs

                    $mdObjs.each((i) => {
                        if ($($mdObjs[i]).scope().card.id === dashMd.id) {
                            const $targetDOM = $($mdObjs[i]);
                            const contentHeight = $targetDOM.outerHeight();
                            const contentWidth = $targetDOM.outerWidth();
                            const parentHeight = $targetDOM.parent().outerHeight();
                            const parentWidth = $targetDOM.parent().outerWidth();

                            // scale content based on markdown's dimensions
                            const contentPadding = parseInt($targetDOM.parent().css('padding').replace(/\D/g, ''), 10);
                            const headerHeight = $('.box-header').outerHeight();

                            scaleFactor = Math.min(
                                ((parentHeight - (contentPadding * 2)) - headerHeight) / contentHeight,
                                parentWidth / contentWidth,
                            );
                        }
                    });

                    if (scaleFactor !== 0) {
                        dashMd.scaleFactor = scaleFactor;

                        Dashboards.update({ _id: $scope.dashboard._id }, {
                            $set: {
                                [`grids.${mdIndex}`]: angular.copy(dashMd),
                            },
                        });
                    }
                }
            }

            function scaleSlideMdContent(slideshowMd) {
                if ($scope.slideIndex !== -1) {
                    let scaleFactor = 0;

                    const $slideContainer = $('#slideCenter');
                    const $mdObj = $('#slideshow-md-content');

                    const contentHeight = $mdObj.outerHeight();
                    const contentWidth = $mdObj.outerWidth();

                    const containerHeight = $slideContainer.outerHeight();
                    const containerWidth = $slideContainer.outerWidth();

                    scaleFactor = Math.min(
                        containerHeight / contentHeight,
                        containerWidth / contentWidth,
                    );

                    if (scaleFactor !== 0) {
                        slideshowMd.scaleFactor = scaleFactor;

                        Slideshows.update({ _id: $scope.slideshow._id }, {
                            $set: {
                                [`slideshowContent.${$scope.slideIndex}`]: angular.copy(slideshowMd),
                            },
                        });
                    }
                }
            }

            /* Triggered when markdown is selected from dropdown menu in Trail view */
            $scope.addMarkdownModal = () => {
                const modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'client/trails/trails-detail/trail-add-markdown/trail-add-markdown-modal.html',
                    controller: 'AddMarkdownModalCtrl',
                    size: 'lg',
                });

                modalInstance.result.then((result) => {
                    createMarkdownCard(result, {
                        left: 51000,
                        top: 50200,
                    });

                    $timeout(() => {
                        // scale content size if not fit
                        scaleMarkdownContent();
                    }, 10);
                }, () => {
                    // console.log('Modal dismissed at: ' + new Date());
                });
            };

            function scaleMarkdownContent() {
                const jsPlumbObjScope = $('js-plumb-object').scope();
                const cards = jsPlumbObjScope ? jsPlumbObjScope.trail.cards : [];
                let scaleFactor = 0;

                if (cards) {
                    const lastIndex = cards.length - 1;
                    const newMd = cards[lastIndex]; // get the new markdown object
                    const $mdObjs = $('.md-content'); // get all markdown content DOMs

                    $mdObjs.each((index) => {
                        if ($($mdObjs[index]).scope().card.id === newMd.id) {
                            const $targetDOM = $($mdObjs[index]);
                            const contentHeight = $targetDOM[0].offsetHeight;
                            const parentHeight = $targetDOM[0].parentElement.offsetHeight;

                            // scale content based on markdown's dimensions
                            if (parentHeight <= contentHeight) {
                                const contentPadding = parseInt($targetDOM.css('padding').replace(/\D/g, ''), 10);
                                scaleFactor = (parentHeight - contentPadding) / contentHeight;
                            }
                        }
                    });

                    if (scaleFactor !== 0) {
                        newMd.scaleFactor = scaleFactor;

                        Trails.update({ _id: $stateParams.trailId }, {
                            $set: {
                                [`cards.${lastIndex}`]: angular.copy(newMd),
                            },
                        });
                    }
                }
            }

            $scope.forkModal = () => {
                const modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'client/trails/trails-detail/fork-modal.html',
                    controller: 'forkModalCtrl',
                    size: 'lg',
                    resolve: {
                        existingExploration() {
                            return $scope.trail;
                        },
                    },
                });

                modalInstance.result.then((result) => {
                    logActivity('forkModalDialog', '', {
                        trailName: $scope.trail.name,
                    });

                    if (!$scope.currentUser) {
                        // $scope.loginModal();
                        $state.go('login');

                        logActivity('forkRequireLogin', '', {
                            trailName: $scope.trail.name,
                        });
                    } else {
                        fileNameForForkModal();
                    }
                }, () => {
                    // console.log('Modal dismissed at: ' + new Date());
                });
            };

            $scope.loginModal = () => {
                const modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'client/loginModal.html',
                    controller: 'loginModalCtrl',
                    size: 'sm',
                });

                modalInstance.result.then((result) => {
                    $scope.forkFileNameModal();
                }, () => {
                    // console.log('Modal dismissed');
                });
            };

            // watch for window resizing
            $(window).resize(() => {
                $scope.$apply(() => {
                    if ($scope.canvasMode === 'trail') {
                        setPanelHeight();
                        setTrailPanelBodyHeight();
                    } else if ($scope.canvasMode === 'dashboard') {
                        // TODO: unclear how to make dashboard responsive when gridster is in use
                    } else if ($scope.canvasMode === 'slideshow') {
                        const slideshowMd = $scope.fullscreenObjects[$scope.slideIndex];

                        if (slideshowMd && (slideshowMd.type === 'trail')) {
                            trailScale();
                        } else {
                            markdownScale();
                        }
                    }
                });
            });

            $scope.$watch('fullscreenObjects', (newVal, oldVal) => {
                checkAndResetForDeletingCurrentCard();
            }, true);

            $scope.$watch('fullscreenState', (newVal, oldVal) => {
                //
            }, true);

            $scope.$watch('dashOrderProperty', () => {
                if ($scope.dashOrderProperty) {
                    $scope.dashSort = {
                        name: parseInt($scope.dashOrderProperty, 10),
                    };
                }
            });

            $scope.$watch('slideOrderProperty', () => {
                if ($scope.slideOrderProperty) {
                    $scope.slideSort = {
                        name: parseInt($scope.slideOrderProperty, 10),
                    };
                }
            });

            /*
            1. When dashboard is initialized, do not update; oldVal is undefined
            2. When dashboard is reset, properties of gridster objects are re-created first before update;
            newVal is undefined
            3. When resize / drag event is detected, need to update properties of gridster objects correctly
            4. When new dashboard card is added, update positions of other cards
            */
            $scope.$watch(($scope) => {
                return $scope.stateObjectsDashboard.map((obj) => {
                        return obj.gridster;
                    });
            }, (newVal, oldVal) => {
                if (newVal[0] && oldVal[0]) {
                    if (newVal.length === oldVal.length) {
                        for (let i = 0; i < newVal.length; i++) {
                            if ((newVal[i].position.col !== oldVal[i].position.col) ||
                                (newVal[i].position.row !== oldVal[i].position.row)) {
                                updateDashboardPos(angular.copy(newVal[i]), i);
                            }

                            if ((newVal[i].size.x !== oldVal[i].size.x) ||
                                (newVal[i].size.y !== oldVal[i].size.y)) {
                                // update nvd3 chart height
                                const gridItem = $scope.stateObjectsDashboard[i];

                                if ($scope.nvd3ChartConfigsDash[gridItem.id]) {
                                    initNvd3ChartHeightDash(gridItem);
                                }

                                updateDashboardSize(angular.copy(newVal[i]), i);

                                // if a dashboard contains a nvd3 chart, dispatch a resize event
                                windowResize(100);
                            }
                        }
                    } else { // new dashboard card is added
                        for (let i = 0; i < newVal.length; i++) {
                            updateDashboardPos(angular.copy(newVal[i]), i);
                        }
                    }
                }
            }, true);

            hotkeys.bindTo($scope)
                .add({
                    combo: 'left',
                    description: 'Previous Slide',
                    callback() {
                        $scope.previousCard();
                    },


                }).add({
                    combo: 'right',
                    description: 'Next Slide',
                    callback() {
                        $scope.nextCard();
                    },
                });

            function loadEntityDataInUse() {
                if ($scope.trail) {
                    if ($scope.trail.loadedEntities.length > 0) {
                        $scope.trail.loadedEntities.forEach((entityId, index) => {
                            if (Meteor.utilitiesHelper.getEntityById($scope.entities, entityId)) {
                                trailDataChecker[entityId] = false;
                                // console.log('Retrieving entity: ' + entityId);

                                Meteor.call('getDataFromEntityId', entityId, (error, result) => {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        // copy data into global array
                                        trailData[entityId] = result;
                                        trailDataChecker[entityId] = true;
                                        checkAllLoading();
                                    }
                                });
                            } else { // entity has been removed
                                // do not load the missing entity
                                Trails.update({ _id: $stateParams.trailId }, {
                                    $unset: {
                                        [`loadedEntities.${index}`]: 1,
                                    },
                                });

                                Trails.update({ _id: $stateParams.trailId }, {
                                    $pull: {
                                        loadedEntities: null,
                                    },
                                });

                                doThingsAfterLoading();
                            }
                        });
                    } else {
                        doThingsAfterLoading();
                    }
                }
            }

            function loadDataAndCreateCard(entityId, parentSources, selectionArray, parent,
                                           index, link, mode, defaultType) {
                // only load the data if it is not already there
                if ($scope.trail.loadedEntities.indexOf(entityId) === -1) {
                    Trails.update({ _id: $stateParams.trailId }, {
                        $push: {
                            loadedEntities: entityId,
                        },
                    });

                    // keep track of the trails that are depended on this entity
                    const entity = Meteor.utilitiesHelper.getEntityById($scope.entities, entityId);

                    if (entity) {
                        Data.update({ _id: entity._id }, {
                            $push: {
                                usedByTrails: $stateParams.trailId,
                            },
                        });
                    } else {
                        console.log('[loadDataAndCreateCard] Entity not found!');
                    }

                    Meteor.call('getDataFromEntityId', entityId, (error, result) => {
                        if (error) {
                            console.log(error);
                        } else {
                            // copy data into global array
                            trailData[entityId] = result;

                            createCardHandler(entityId, parentSources, selectionArray, parent,
                                index, link, mode, defaultType);
                        }
                    });
                } else {
                    createCardHandler(entityId, parentSources, selectionArray, parent,
                        index, link, mode, defaultType);
                }
            }

            function logActivity(type, message, data) {
                if (LOG_ON) {
                    const newLog = {};
                    newLog.createdAt = new Date();
                    newLog.createdAtStr = newLog.createdAt.toString();
                    newLog.createdAtUTC = Date.parse(newLog.createdAtStr);
                    newLog.user = $scope.currentUser ? $scope.currentUser._id : 'Not logged in';
                    newLog.type = type;
                    newLog.message = message;
                    newLog.username = $scope.currentUser ? $scope.currentUser.username : 'Not logged in';
                    newLog.name = type;
                    newLog.data = data;

                    Logs.insert(newLog, (err, result) => {
                        // console.log(err, result)
                    });
                }
            }

            function checkOwnership() {
                if (!isOwner()) {
                    $scope.forkModal();
                }
            }

            function isOwner() {
                if (!$scope.currentUser) {
                    return false;
                }

                if ($scope.trail.ownerId === $scope.currentUser._id) {
                    return true;
                }

                return false;
            }

            function checkAllLoading() {
                if (Meteor.utilitiesHelper.checkAllProp(trailDataChecker)) {
                    doThingsAfterLoading();
                }
            }

            function loadEntityDataObjects() {
                $scope.trail.cards.forEach((card, index) => {
                    if (card && card.entityId) {
                        // if entity is removed, reset properties of the trail card and show missing data message
                        if (Meteor.utilitiesHelper.getEntityById($scope.entities, card.entityId)) {
                            if (card.chartType === 'gp') {
                                if (card.entityData) {
                                    let selectedDocuments = Meteor.utilitiesHelper.fromIdListToObjectList(
                                        card.entityData, trailData[card.entityId]);
                                    updateDataUsedByGatherplot(card.id, selectedDocuments);
                                    selectedDocuments = null; // free space
                                }
                            } else { // pie or bar
                                if (card.chartData) {
                                    const selectedDim = card.chartConfig.selection.dimName;

                                    if (selectedDim) {
                                        updateDataUsedByChart(card.id, card.chartData[selectedDim]);
                                    } else {
                                        updateDataUsedByChart(card.id, []);
                                    }
                                }

                                initNvd3ChartEventConfig(card.id);
                                initNvd3ChartHeight(card.id, card.nvd3ChartHeight);
                            }
                        } else {
                            const trailCardNoData = createTrailCardNoData(card);

                            Trails.update({ _id: $stateParams.trailId }, {
                                $set: {
                                    [`cards.${index}`]: trailCardNoData,
                                },
                            });
                        }
                    }
                });
            }

            function doThingsAfterLoading() {
                $scope.isAllLoaded = true;

                // initialize UUID counter variable for id generation of trail instance and card objects
                if (!$scope.trail.globalUUIDCounter) {
                    $scope.trail.globalUUIDCounter = 2000;
                }

                if ($scope.trail.cards) {
                    loadEntityDataObjects();
                } else {
                    // $scope.newState();
                }

                if ($scope.trail.cards) {
                    $scope.fullscreenState = $scope.trail.cards[0];
                }

                $timeout(() => {
                    // Here your view content is fully loaded !!
                    $scope.zoomToFit();
                    checkOwnership();
                }, ZOOM_TIMEOUT);
            }

            function getNextUUID() {
                if (!$scope.trail.globalUUIDCounter) {
                    $scope.trail.globalUUIDCounter = 2000;
                }

                $scope.trail.globalUUIDCounter++;
                return $scope.trail.globalUUIDCounter;
            }

            /*
             * Initialize or update data for a trail card
             * @param {Object} card: the card object
             * @param {Array} data: selected data from the parent card
             *
             */
            function initGatherplotData(card, data) {
                card.entityData = data.map((d) => { return d._id; });
            }

            /*
             * Update the chart data array with gatherplot data
             * @param {Number} id: the id of the card object
             * @param {Array} data: gatherplot data
             * */
            function updateDataUsedByGatherplot(id, data) {
                data = angular.copy(data);
                data.map((d, i) => { d.id = i; });
                $scope.entityDataObjectsList[id] = data;
            }

            /*
             * Update the chart data array for other chart types (pie, bar, and etc.)
             * */
            function updateDataUsedByChart(id, data) {
                $scope.entityDataObjectsList[id] = data || [];
            }

            /**
             * Initialize nvd3 chart data in the card
             * @param card
             * @param data
             */
            function initNvd3ChartData(card, data) {
                switch (card.chartType) {
                    case BAR_CHART: {
                        initBarData(card, data);
                        break;
                    }
                    case PIE_CHART: {
                        initPieData(card, data);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }

            /* Return a child card object containing bar chart data
             */
            function initBarData(card, data) {
                if (data) {
                    const barPlotData = {};
                    const bdata = angular.copy(data); // bar data

                    // x-axis represents the categorical values
                    // y-axis represents the count of a categorical value
                    bdata.forEach((doc) => {
                        Object.keys(doc).forEach((dim) => {
                            if ((dim !== '_id') && (dim !== 'id')) {
                                if (!Object.prototype.hasOwnProperty.call(barPlotData, dim)) {
                                    const bars = {};
                                    bars.key = dim;
                                    bars.values = [];

                                    const bar = {};
                                    bar.x = doc[dim];
                                    bar.y = 1;
                                    bar.entityIds = [doc._id];
                                    bar.selected = false;

                                    // To remove, a temporary way to have access to card object when selecting bar
                                    bar.stateId = card.id;
                                    bar.dim = dim;

                                    bars.values.push(bar);

                                    barPlotData[dim] = [bars];
                                } else {
                                    // set the attribute value as the x-aixs key
                                    const barKey = doc[dim];

                                    // search for existing keys with the same value
                                    const barIndex = barPlotData[dim][0].values.findIndex((d) => {
                                        return d.x === barKey;
                                    });

                                    if (barIndex !== -1) {
                                        // existing bar, increment count
                                        barPlotData[dim][0].values[barIndex].y += 1;
                                        barPlotData[dim][0].values[barIndex].entityIds.push(doc._id);
                                    } else {
                                        // new bar, initialize bar data
                                        const newBar = {};
                                        newBar.x = barKey;
                                        newBar.y = 1;
                                        newBar.entityIds = [doc._id];
                                        newBar.selected = false;

                                        /**
                                         * TODO: To remove,
                                         * a temporary way to have access to card object when selecting sector
                                         */
                                        newBar.stateId = card.id;
                                        newBar.dim = dim;

                                        // add a new bar
                                        barPlotData[dim][0].values.push(newBar);
                                    }
                                }
                            }
                        });
                    });

                    // sort the x-axis labels
                    Object.keys(barPlotData).forEach((dim) => {
                        barPlotData[dim][0].values.sort((a, b) => {
                            naturalSort.insensitive = true;
                            return naturalSort(a.x, b.x);
                        });
                    });

                    /* for each dimension, assign fixed color values to bars if
                     the data of a dimension is nominal; use heatmap scale for ordinal data*/
                    Object.keys(barPlotData).forEach((dim) => {
                        let bars = barPlotData[dim][0].values;

                        bars = computeColorForNvd3(bars, 'x');

                        barPlotData[dim][0].values = bars;
                    });

                    card.chartData = barPlotData;
                }
            }

            function initPieData(card, data) {
                if (data) {
                    const piePlotData = {};
                    const pdata = angular.copy(data); // pie data

                    pdata.forEach((doc) => {
                        Object.keys(doc).forEach((dim) => {
                            if ((dim !== '_id') && (dim !== 'id')) {
                                if (!Object.prototype.hasOwnProperty.call(piePlotData, dim)) {
                                    const pieSectorObj = {};
                                    pieSectorObj.x = doc[dim]; //
                                    pieSectorObj.y = 1; // the count of the value
                                    pieSectorObj.entityIds = [doc._id];
                                    pieSectorObj.selected = false;

                                    // To remove, a temporary way to have access to card object when selecting sector
                                    pieSectorObj.stateId = card.id;
                                    pieSectorObj.dim = dim;

                                    piePlotData[dim] = [pieSectorObj];
                                } else {
                                    // set the data value as the sector key
                                    const sectorKey = doc[dim];

                                    const sectorIndex = piePlotData[dim].findIndex((sector) => {
                                        return sector.x === sectorKey;
                                    });

                                    if (sectorIndex !== -1) {
                                        // existing sector, increment count
                                        piePlotData[dim][sectorIndex].y += 1;
                                        piePlotData[dim][sectorIndex].entityIds.push(doc._id);
                                    } else {
                                        // new sector, initialize sector data
                                        const newSectorObj = {};
                                        newSectorObj.x = sectorKey;
                                        newSectorObj.y = 1;
                                        newSectorObj.entityIds = [doc._id];
                                        newSectorObj.selected = false;

                                        /**
                                         * TODO: to remove, a temporary way to have access to card object
                                         * when selecting sector
                                         */
                                        newSectorObj.stateId = card.id;
                                        newSectorObj.dim = dim;

                                        // add a new sector
                                        piePlotData[dim].push(newSectorObj);
                                    }
                                }
                            }
                        });
                    });

                    // sort the x-axis labels
                    Object.keys(piePlotData).forEach((dim) => {
                        piePlotData[dim].sort((a, b) => {
                            naturalSort.insensitive = true;
                            return naturalSort(a.x, b.x);
                        });
                    });

                    /* for each dimension, assign fixed color values to bars if
                     the data of a dimension is nominal; use heatmap scale for ordinal data*/
                    Object.keys(piePlotData).forEach((dim) => {
                        let slices = piePlotData[dim];
                        slices = computeColorForNvd3(slices, 'x');
                        piePlotData[dim] = slices;
                    });

                    card.chartData = piePlotData;
                }
            }

            /* Return the data with computed color values
             */
            function computeColorForNvd3(data, keyName) {
                let colorFunc;

                if (isNominal(data, keyName)) {
                    // get a list of categories or labels
                    const keys = data.map((d) => { return d[keyName]; });

                    if (keys.length > 0) {
                        if (keys.length <= 10) {
                            colorFunc = d3.scale.category10()
                                .domain(keys);
                        } else {
                            colorFunc = d3.scale.category20()
                                .domain(keys);
                        }

                        data.forEach((d) => {
                            d.color = colorFunc(d[keyName]);
                        });
                    }
                } else { // ordinal
                    // get min and max values from the data
                    const colorDomain = d3.extent(data, (d) => {
                        return +d[keyName];
                    });

                    colorFunc = d3.scale.linear()
                        .range(['#98c8fd', '08306b'])
                        .domain(colorDomain)
                        .interpolate(d3.interpolateHsl);

                    data.forEach((d) => {
                        d.color = colorFunc(d[keyName]);
                    });
                }

                return data;
            }

            function filterNvd3ChartColor(newCard, parentCard, selectedData) {
                switch (newCard.chartType) {
                    case BAR_CHART: {
                        filterBarChartColor(newCard, parentCard, selectedData);
                        break;
                    }
                    case PIE_CHART: {
                        filterPieChartColor(newCard, parentCard, selectedData);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }

            /* Return a child card object with bar colors copied from its parent card
             */
            function filterBarChartColor(newCard, parent, selectedData) {
                if (parent.chartData) {
                    // create a color map for child card to reference from
                    const parentColorMap = {};

                    Object.keys(parent.chartData).forEach((dim) => {
                        const barsColorMap = {};
                        const bars = parent.chartData[dim][0].values || [];

                        if (bars.length > 0) {
                            bars.forEach((bar) => {
                                if (bar.color) {
                                    barsColorMap[bar.x] = bar.color;
                                }
                            });
                        }

                        parentColorMap[dim] = barsColorMap;
                    });

                    // initialize new chart data for the child card
                    initBarData(newCard, selectedData);

                    // copy all the color values from parent card
                    Object.keys(newCard.chartData).forEach((dim) => {
                        const bars = newCard.chartData[dim][0].values || [];

                        if (bars.length > 0) {
                            newCard.chartData[dim][0].values.forEach((bar) => {
                                bar.color = parentColorMap[dim][bar.x];
                            });
                        }
                    });
                }
            }

            /* Return a child card object with bar colors copied from its parent card
             */
            function filterPieChartColor(newCard, parent, selectedData) {
                if (parent.chartData) {
                    // create a color map for child card to reference from
                    const parentColorMap = {};

                    Object.keys(parent.chartData).forEach((dim) => {
                        const slicesColorMap = {};
                        const slices = parent.chartData[dim] || [];

                        if (slices.length > 0) {
                            slices.forEach((s) => {
                                if (s.color) {
                                    slicesColorMap[s.x] = s.color;
                                }
                            });
                        }

                        parentColorMap[dim] = slicesColorMap;
                    });

                    // initialize new chart data for the child card
                    initPieData(newCard, selectedData);

                    // copy all the color values from parent card
                    Object.keys(newCard.chartData).forEach((dim) => {
                        const slices = newCard.chartData[dim] || [];

                        if (slices.length > 0) {
                            newCard.chartData[dim].forEach((slice) => {
                                slice.color = parentColorMap[dim][slice.x];
                            });
                        }
                    });
                }
            }

            function initDimension(newCard) {
                if (newCard && newCard.chartData) {
                    const firstDim = getDimNonkey(newCard.chartData);
                    newCard.chartConfig.selection.dimName = firstDim;
                }
            }

            function initGatherColorCoding(newCard, index) {
                const colorDim = newCard.gatherConfig.params.colorDim;

                if (!colorDim) {
                    return;
                }

                if (newCard.gatherConfig.colorCoding[colorDim]) {
                    return;
                }

                const categories = {};
                let colorDomain = [];
                let colorRange = [];
                let colorNominalScale;
                let colorCoding;
                const data = $scope.entityDataObjectsList[newCard.id];

                if (data && (data.length > 0)) {
                    if (isNominal(data, colorDim)) {
                        data.forEach((d) => {
                            if (!categories[d[colorDim]]) {
                                categories[d[colorDim]] = 1;
                            } else {
                                categories[d[colorDim]] += 1;
                            }
                        });

                        colorDomain = Object.keys(categories);

                        if (colorDomain.length > 0) {
                            // sort categories
                            naturalSort.insensitive = true;
                            colorDomain.sort(naturalSort);

                            if (colorDomain.length <= 10) {
                                colorNominalScale = d3.scale.category10()
                                    .domain(colorDomain);
                            } else {
                                colorNominalScale = d3.scale.category20()
                                    .domain(colorDomain);
                            }

                            colorDomain.forEach((d) => {
                                colorRange.push(colorNominalScale(d));
                            });
                        }
                    } else { // oridnal or semi-ordinal
                        colorDomain = d3.extent(data, (d) => {
                            return +d[colorDim];
                        });

                        colorRange = ['#98c8fd', '08306b']; // heatmap
                    }

                    colorCoding = {
                        categories: colorDomain,
                        colors: colorRange,
                    };

                    updateDBGatherColorCoding(index, colorDim, colorCoding);
                }
            }

            /*
             Return the new card object with computed color codings
             */
            function filterGatherColorCoding(newCard, parentColorCoding, colorDim, selectedData) {
                if (parentColorCoding && parentColorCoding.categories && parentColorCoding.colors) {
                    const parentCategories = parentColorCoding.categories; // parent card's categories
                    const parentColors = parentColorCoding.colors; // parent card's color values or range

                    if (selectedData.length === 0) { // no data is selected
                        newCard.gatherConfig.colorCoding[colorDim] = {
                            categories: parentCategories,
                            colors: parentColors,
                        };
                    } else {
                        const categoryMap = {}; // find the categories from the selected data points
                        let childCategories = [];
                        let childColors = []; // child card's color values or range

                        if (isNominal(selectedData, colorDim)) {
                            selectedData.forEach((d) => {
                                if (!categoryMap[d[colorDim]]) {
                                    categoryMap[d[colorDim]] = 1;
                                } else {
                                    categoryMap[d[colorDim]] += 1;
                                }
                            });

                            childCategories = Object.keys(categoryMap);

                            // sort categories
                            naturalSort.insensitive = true;
                            childCategories.sort(naturalSort);

                            // for nominal categories, find the corresponding color from parent card's colors
                            childCategories.forEach((d) => {
                                const i = parentCategories.findIndex((c) => {
                                    return c === d;
                                });

                                if (i === -1) {
                                    // something went wrong, there should be a mapping
                                    console.log(`Cannot find color for category ${d}`);
                                }

                                childColors.push(parentColors[i]);
                            });
                        } else { // semi-ordinal or ordinal
                            // currently do not preserve color values as it is using heatmap
                            childCategories = d3.extent(selectedData, (d) => {
                                return +d[colorDim];
                            });

                            childColors = parentColors; // use the same color range as in parent card
                        }

                        newCard.gatherConfig.colorCoding[colorDim] = {
                            categories: childCategories,
                            colors: childColors,
                        };
                    }
                }

                return newCard;
            }

            /* Return true if data is nominal, false if it is ordinal
             */
            function isNominal(data, attribute) {
                let isNom = false;

                if (!data || !attribute) {
                    return isNom;
                }

                data.forEach((d) => {
                    if (!isNumeric(+d[attribute])) {
                        isNom = true;
                    }
                });

                return isNom;
            }

            function updateDBGatherColorCoding(index, colorDim, colorCoding) {
                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        [`cards.${index}.gatherConfig.colorCoding.${colorDim}`]: colorCoding,
                    },
                });
            }

            /* Single click behaviour for bar chart:
             * select a bar and deselect others
             */
            function updateBarSingleSelection(event) {
                if (event.data) {
                    const card = $scope.getStateFromId(event.data.stateId);
                    const index = $scope.trail.cards.indexOf(card);

                    if (index !== -1) {
                        const dim = event.data.dim;
                        const barIndex = event.index;
                        const dimData = card.chartData[dim][0].values;
                        const numBars = dimData.length;

                        if (numBars > 0) {
                            for (let i = 0; i < numBars; i++) {
                                if (i === barIndex) {
                                    Trails.update({ _id: $stateParams.trailId }, {
                                        $set: {
                                            [`cards.${index}.chartData.${dim}.0.values.${barIndex}.selected`]: event.data.selected,
                                        },
                                    });
                                } else {
                                    Trails.update({ _id: $stateParams.trailId }, {
                                        $set: {
                                            [`cards.${index}.chartData.${dim}.0.values.${i}.selected`]: false,
                                        },
                                    });

                                    $scope.entityDataObjectsList[event.data.stateId][0].values[i].selected = false;
                                }
                            }
                        }
                    }
                }
            }

            /* Control + click behaviour for bar chart:
             * add to existing selections of bars
             */
            function updateBarCtrlSelection(event) {
                if (event.data) {
                    const card = $scope.getStateFromId(event.data.stateId);
                    const index = $scope.trail.cards.indexOf(card);

                    if (index !== -1) {
                        const dim = event.data.dim;
                        const barIndex = event.index;

                        Trails.update({ _id: $stateParams.trailId }, {
                            $set: {
                                [`cards.${index}.chartData.${dim}.0.values.${barIndex}.selected`]: event.data.selected,
                            },
                        });
                    }
                }
            }

            /* Shift + click behaviour for bar chart:
             * search nearest selected element,
             * add intermediate bars to selection;
             * if none selected, select all bars to the left
             */
            function updateBarShiftSelection(event) {
                if (event.data) {
                    const card = $scope.getStateFromId(event.data.stateId);
                    const index = $scope.trail.cards.indexOf(card);

                    if (index !== -1) {
                        const dim = event.data.dim;
                        const barIndex = event.index;

                        if (dim && (barIndex >= 0)) {
                            const dimData = card.chartData[dim][0].values;
                            const numBars = dimData.length;

                            if (numBars > 0) {
                                const selectedCnt = getChartEleSelectedCount(dimData);

                                if (!selectedCnt) {
                                    // select all bars to the left
                                    for (let i = 0; i <= barIndex; i++) {
                                        Trails.update({ _id: $stateParams.trailId }, {
                                            $set: {
                                                [`cards.${index}.chartData.${dim}.0.values.${i}.selected`]: true,
                                            },
                                        });

                                        $scope.entityDataObjectsList[event.data.stateId][0].values[i].selected = true;
                                    }
                                // if all is selected, unselect all bars
                                } else if (selectedCnt === numBars) {
                                    for (let i = 0; i < numSectors; i++) {
                                        Trails.update({ _id: $stateParams.trailId }, {
                                            $set: {
                                                [`cards.${index}.chartData.${dim}.0.values.${i}.selected`]: false,
                                            },
                                        });

                                        $scope.entityDataObjectsList[event.data.stateId][0].values[i].selected = false;
                                    }
                                } else { // selectedCnt > 0 && (selectedCnt < numBars)
                                    const nearestIndex = searchSelectedIndex(dimData, barIndex);
                                    if (nearestIndex === -1) {
                                        // should not reach here
                                    } else if (nearestIndex === barIndex) {
                                        // shift click on same element, select/unselect current element
                                        Trails.update({ _id: $stateParams.trailId }, {
                                            $set: {
                                                [`cards.${index}.chartData.${dim}.0.values.${barIndex}.selected`]: event.data.selected,
                                            },
                                        });
                                    } else if (nearestIndex < barIndex) {
                                        // select all sectors from sectorIndex to nearestIndex counter clockwise
                                        for (let i = barIndex; i >= nearestIndex; i--) {
                                            Trails.update({ _id: $stateParams.trailId }, {
                                                $set: {
                                                    [`cards.${index}.chartData.${dim}.0.values.${i}.selected`]: true,
                                                },
                                            });

                                            $scope.entityDataObjectsList[event.data.stateId][0].values[i].selected = true;
                                        }
                                    } else { // nearestIndex > sectorIndex
                                        // select all sectors from the shift click index to the nearest selected index clockwise
                                        for (let i = barIndex; i < nearestIndex; i++) {
                                            Trails.update({ _id: $stateParams.trailId }, {
                                                $set: {
                                                    [`cards.${index}.chartData.${dim}.0.values.${i}.selected`]: true,
                                                },
                                            });

                                            $scope.entityDataObjectsList[event.data.stateId][0].values[i].selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /* Single click behaviour: select only a slice and deselect others */
            function updatePieSingleSelection(event) {
                if (event.data) {
                    const card = $scope.getStateFromId(event.data.stateId);
                    const index = $scope.trail.cards.indexOf(card);

                    if (index !== -1) {
                        const dim = event.data.dim;
                        const sectorIndex = event.index;
                        const dimData = card.chartData[dim];
                        const numSectors = dimData.length;

                        if (numSectors > 0) {
                            for (let i = 0; i < numSectors; i++) {
                                if (!$scope.entityDataObjectsList[event.data.stateId][i]) {
                                    console.log(`Data error: could not select element at ${i}`);
                                    return;
                                }

                                if (i === sectorIndex) {
                                    Trails.update({ _id: $stateParams.trailId }, {
                                        $set: {
                                            [`cards.${index}.chartData.${dim}.${sectorIndex}.selected`]: event.data.selected,
                                        },
                                    });
                                } else {
                                    Trails.update({ _id: $stateParams.trailId }, {
                                        $set: {
                                            [`cards.${index}.chartData.${dim}.${i}.selected`]: false,
                                        },
                                    });

                                    $scope.entityDataObjectsList[event.data.stateId][i].selected = false;
                                }
                            }
                        }
                    }
                }
            }

            function updatePieCtrlSelection(event) {
                if (event.data) {
                    const card = $scope.getStateFromId(event.data.stateId);
                    const index = $scope.trail.cards.indexOf(card);

                    if (index !== -1) {
                        const dim = event.data.dim;
                        const sectorIndex = event.index;

                        Trails.update({ _id: $stateParams.trailId }, {
                            $set: {
                                [`cards.${index}.chartData.${dim}.${sectorIndex}.selected`]: event.data.selected,
                            },
                        });
                    }
                }
            }

            function updatePieShiftSelection(event) {
                if (event.data) {
                    const card = $scope.getStateFromId(event.data.stateId);
                    const index = $scope.trail.cards.indexOf(card);

                    if (index !== -1) {
                        const dim = event.data.dim;
                        const sectorIndex = event.index;

                        if (dim && (sectorIndex >= 0)) {
                            const dimData = card.chartData[dim];
                            const numSectors = dimData.length;

                            if (numSectors > 0) {
                                // Shift click conditions
                                // if none is selected, select all sectors
                                const selectedCnt = getChartEleSelectedCount(dimData);

                                if (!selectedCnt) {
                                    for (let i = 0; i < numSectors; i++) {
                                        updateDBPieChartSectorSelection(index, dim, i, true);
                                        $scope.entityDataObjectsList[event.data.stateId][i].selected = true;
                                    }
                                // if all is selected, unselect all sectors
                                } else if (selectedCnt === numSectors) {
                                    for (let i = 0; i < numSectors; i++) {
                                        updateDBPieChartSectorSelection(index, dim, i, false);
                                        $scope.entityDataObjectsList[event.data.stateId][i].selected = false;
                                    }
                                } else { // selectedCnt > 0 && selectedCnt < numSectors
                                    /*
                                        If one or more selected sectors.
                                        Suppose the pie is divided into 6 slices,
                                        like the following symbol, (*) , such that
                                        the index of the top right is 0, all the way
                                        to the top left 5.

                                        Going counter clockwise from the sector
                                        under the cursor, add every sector until
                                        we reach a selected sector.

                                        Case 1:
                                        sector 2 (previously selected),
                                        sector 5 (currenly selected sector)
                                        then sector 2,1,0,5 will be selected

                                        Case 2:
                                        sector 5 (previously selected),
                                        sector 2 (currently selected),
                                        then sector 5,4,3,2 will be selected
                                    */
                                    const nearestIndex = searchSelectedIndex(dimData, sectorIndex);
                                    if (nearestIndex === -1) {
                                        // should not reach here
                                    } else if (nearestIndex === sectorIndex) {
                                        // shift key on same sector, select/unselect current sector ?
                                        updateDBPieChartSectorSelection(index, dim, sectorIndex, event.data.selected);
                                    } else if (nearestIndex < sectorIndex) {
                                        for (let i = sectorIndex; i >= nearestIndex; i--) {
                                            updateDBPieChartSectorSelection(index, dim, i, true);
                                            $scope.entityDataObjectsList[event.data.stateId][i].selected = true;
                                        }
                                    } else { // nearestIndex > sectorIndex
                                        for (let i = nearestIndex; i < numSectors; i++) {
                                            updateDBPieChartSectorSelection(index, dim, i, true);
                                            $scope.entityDataObjectsList[event.data.stateId][i].selected = true;
                                        }

                                        for (let j = 0; j <= sectorIndex; j++) {
                                            updateDBPieChartSectorSelection(index, dim, j, true);
                                            $scope.entityDataObjectsList[event.data.stateId][j].selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /*
                search selected element clockwise from startIndex in list,
                returns the index of the firstly encountered selected element
            */
            function searchSelectedIndex(list, startIndex) {
                let resultIndex = -1;
                const len = list.length;

                // search from current index to last
                for (let i = startIndex; i < len; i++) {
                    if (list[i].selected) {
                        resultIndex = i;
                        break;
                    }
                }

                // selected not found, search from first to startIndex
                if (resultIndex === -1) {
                    for (let i = 0; i < startIndex; i++) {
                        if (list[i].selected) {
                            resultIndex = i;
                            break;
                        }
                    }
                }

                return resultIndex;
            }

            /* Get the count of selected chart elements */
            function getChartEleSelectedCount(dimData) {
                let selectedCnt = 0;

                dimData.forEach((d) => {
                    if (d.selected) selectedCnt++;
                });

                return selectedCnt;
            }

            function updateDBChartDim(index, dim) {
                if (index !== -1) {
                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.chartConfig.selection.dimName`]: dim,
                        },
                    }, (error) => {
                        if (error) {
                            // logActivity("updateDBChartDim", 'failed to update dimension!', selectedDim);
                            console.log('Failed to update dimension!');
                        } else {
                            // logActivity("updateDBChartDim", 'updated successfully!', selectedDim);
                        }
                    });
                }
            }

            function updateDBChartType(index, type) {
                if ((index !== -1) && type) {
                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.chartType`]: type,
                        },
                    });
                }
            }

            function updateDBSlideshowChartType(index, type) {
                if ((index !== -1) && type) {
                    Slideshows.update({ _id: $scope.slideshow._id }, {
                        $set: {
                            [`slideshowContent.${index}.chartType`]: type,
                        },
                    });
                }
            }

            function updateDBChartData(index, chartData) {
                if ((index !== -1) && chartData) {
                    Trails.update({ _id: $stateParams.trailId }, {
                        $unset: {
                            [`cards.${index}.chartData`]: 1,
                        },
                    });

                    Trails.update({ _id: $stateParams.trailId }, {
                        $pull: {
                            [`cards.${index}.chartData`]: null,
                        },
                    });

                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.chartData`]: chartData,
                        },
                    });
                }
            }

            /* Update the selection status of a sector in pie chart */
            function updateDBPieChartSectorSelection(cardIndex, dim, sectorIndex, selection) {
                if ((cardIndex !== -1) && dim && (sectorIndex !== -1)) {
                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${cardIndex}.chartData.${dim}.${sectorIndex}.selected`]: selection,
                        },
                    });
                }
            }

            /*
                fork trail instance and the associated dashboards and
                slideshows for the current user
            */
            function forkExploration(name, isCopyAll) {
                const newObj = $scope.trail.getRawObject();

                const oldId = newObj._id;

                // newObj._id = Random.id();

                delete newObj._id;

                newObj.ownerId = $scope.currentUser._id;

                newObj.name = name;

                Trails.insert(newObj, (err, newid) => {
                    if (!err) {
                        if (isCopyAll) {
                            const myDashboards = Dashboards.find({
                                trailId: oldId,
                            }, {
                                reactive: false,
                            }).fetch();

                            myDashboards.forEach((d) => {
                                console.log(d);
                                delete d._id;
                                d.trailId = newid;
                                d.ownerId = $scope.currentUser._id;
                                Dashboards.insert(d, (err, newid) => {
                                    console.log(err, newid);
                                });
                            });

                            const mySlideshows = Slideshows.find({
                                trailId: oldId,
                            }, {
                                reactive: false,
                            }).fetch();

                            mySlideshows.forEach((d) => {
                                console.log(d);
                                delete d._id;
                                d.trailId = newid;
                                d.ownerId = $scope.currentUser._id;
                                Slideshows.insert(d, (err, newid) => {
                                    console.log(err, newid);
                                });
                            });
                        }

                        // console.log(result);
                        $state.go('trail-detail', {
                            trailId: newid,
                        });
                    } else {
                        console.log(err);
                    }
                });
                logActivity('forkExploration', '', {
                    trailName: name,
                });
            }

            function fileNameForForkModal() {
                const modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'client/trails/trails-detail/file-name-for-fork-modal.html',
                    controller: 'fileNameForForkModalCtrl',
                    size: 'lg',
                    resolve: {
                        existingExploration() {
                            return $scope.trail;
                        },
                    },
                });

                logActivity('filename for fork', '', {
                    trailName: $scope.trail.name,
                });

                modalInstance.result.then((result) => {
                    // console.log(result);

                    forkExploration(result.filename, result.copyAll);

                    logActivity('filename for fork', '', {
                        trailName: $scope.trail.name,
                        newFileName: result.filename,
                    });
                }, () => {
                    // console.log('Modal dismissed at: ' + new Date());
                });
            }

            function createCardHandler(entityId, parentSources, selectionArray, parent,
                                       index, link, mode, defaultType) {
                if (mode === 'start') {
                    createCardFromEntity(entityId, defaultType);
                } else if (mode === 'filter') {
                    createCardByFilter(entityId, parentSources, selectionArray, parent, index);
                } else { // pivot
                    createCardByPivot(entityId, parentSources, selectionArray, parent, index, link);
                }
            }

            /*
             * Create a new trail card with the selected entity from the header dropdown menu
             * @param {Number} entityId, the entity id of the new card
             * @param {String} defaultType, the default type of chart
             */
            function createCardFromEntity(entityId, defaultType) {
                // get the entity object
                const entityObject = Meteor.utilitiesHelper.getEntityById($scope.entities, entityId);

                if (!entityObject) {
                    console.log('Entity does not exist!');
                    return;
                }

                /* Initialize trail card object with chart configuration */
                let newCard = null;
                let newCardIndex = -1;

                // create a card from scratch
                newCard = createTrailCardObj(entityId);

                // initialize chart specific configuration properties
                initGatherplotConfig(newCard);
                initNvd3ChartConfig(newCard);
                newCard.chartType = defaultType;

                newCard.nvd3ChartHeight = CHART_HEIGHT_MIN;

                $scope.trail.cards = $scope.trail.cards || [];

                newCardIndex = $scope.trail.cards.push(newCard);

                newCard = $scope.trail.cards[newCardIndex - 1];

                initNvd3ChartEventConfig(newCard.id);

                // initialize gatherplot and other chart data for the trail card
                initGatherplotData(newCard, trailData[entityId]);

                switch (defaultType) {
                    case PIE_CHART:
                        initPieData(newCard, trailData[entityId]);
                        initDimension(newCard);

                        updateDataUsedByChart(newCard.id,
                            newCard.chartData[newCard.chartConfig.selection.dimName]);
                        break;
                    case BAR_CHART:
                        initBarData(newCard, trailData[entityId]);
                        initDimension(newCard);

                        updateDataUsedByChart(newCard.id,
                            newCard.chartData[newCard.chartConfig.selection.dimName]);
                        break;
                    default: // gatherplot
                        updateDataUsedByGatherplot(newCard.id, trailData[entityId]);
                        break;
                }

                updateDBAddNewCard([], newCardIndex, null, newCard);
            }

            /*
             * Create a new trail card by filtering
             * @param {Number} entityId, the entity id of the new card
             * @param {obj Array} parentSources, the outgoing connections of the parent card
             * @param {Object} parentState, the parent card
             * @param {Number} parentIndex, the index of the parent card
             * @param {int Array} selectionArray, the array of ids of selected entities, for gatherplot only
             */
            function createCardByFilter(entityId, parentSources, selectionArray, parent, parentIndex) {
                if (!entityId) {
                    console.log('Parent entity id does not exist!');
                    return;
                }

                /* Initialize trail card object with chart configuration */
                let newCard = null;
                let posFactor = -1;
                let indNewCard = -1;

                // create a card based on parent's configuration
                newCard = createTrailCardObj(entityId);

                newCard.parent = parent.id;

                // update parent card's children
                parent.children.push(newCard.id);

                posFactor = parent.children.length - 1;
                newCard.x = parent.x + parent.width + PADDING;
                newCard.y = parent.y + (posFactor * (parent.height + PADDING)); // not to let the new card overlay with each other

                // initialize chart specific configuration properties
                initGatherplotConfig(newCard);

                newCard.chartType = parent.chartType;
                newCard.chartConfig = parent.chartConfig;

                newCard.width = parent.width;
                newCard.height = parent.height;
                newCard.nvd3ChartHeight = CHART_HEIGHT_MIN;

                newCard.queries = [];

                $scope.trail.cards = $scope.trail.cards || [];

                indNewCard = $scope.trail.cards.push(newCard);

                newCard = $scope.trail.cards[indNewCard - 1];

                initNvd3ChartEventConfig(newCard.id);

                // initialize gatherplot and other chart data for the trail card
                initGatherplotData(newCard, trailData[entityId]);

                if (newCard.chartType === 'gp') {
                    // preserve selected dimensions
                    newCard.gatherConfig.params = parent.gatherConfig.params;
                    const colorDim = newCard.gatherConfig.params.colorDim;
                    const colorCoding = colorDim ? parent.gatherConfig.colorCoding[colorDim] : null;

                    // if no data point is selected, use parent card's data for the new trail card
                    if (selectionArray.length === 0 || (selectionArray.length === 1 && selectionArray[0] === -1)) {
                        initGatherplotData(newCard, $scope.entityDataObjectsList[parent.id]);

                        updateDataUsedByGatherplot(newCard.id, $scope.entityDataObjectsList[parent.id]);

                        newCard = filterGatherColorCoding(newCard, colorCoding, colorDim, []);
                    } else {
                        let selectedParentData = $scope.entityDataObjectsList[parent.id].filter((d) => {
                            return selectionArray.indexOf(d.id) !== -1;
                        });

                        const xDim = newCard.gatherConfig.params.xDim;
                        const yDim = newCard.gatherConfig.params.yDim;

                        extractQueriesGp(selectedParentData, xDim, yDim, newCard.queries);

                        initGatherplotData(newCard, selectedParentData);

                        updateDataUsedByGatherplot(newCard.id, selectedParentData);

                        newCard = filterGatherColorCoding(newCard, colorCoding, colorDim, selectedParentData);

                        selectedParentData = null;
                    }

                    // update parent's selection space
                    if (parent.dimsum.selectionSpace) {
                        Trails.update({ _id: $stateParams.trailId }, {
                            $set: {
                                [`cards.${parentIndex}.dimsum.selectionSpace`]: parent.dimsum.selectionSpace,
                            },
                        });
                    }
                } else if (newCard.chartType === 'pie') {
                    // get the data of the selected dimension
                    // if no dimension is selected, start with undefined
                    const selectedDim = parent.chartConfig.selection.dimName || '';

                    // get pie data
                    let pieData = parent.chartData[selectedDim];
                    let selectedDocumentIds = [];

                    if (selectedDim && pieData) {
                        // get the IDs of documents that are mapped to the sectors being selected
                        pieData.forEach((obj) => {
                            if (obj.selected) {
                                selectedDocumentIds = selectedDocumentIds.concat(obj.entityIds);
                            }
                        });

                        pieData = null;
                    }

                    // parameter preservation from parent card with nvd3 chart
                    newCard.chartConfig.selection.dimName = selectedDim;

                    if (selectedDocumentIds.length > 0) {
                        newCard.entityData = selectedDocumentIds;

                        let selectedParentData = Meteor.utilitiesHelper.fromIdListToObjectList(selectedDocumentIds,
                            trailData[parent.entityId]);

                        extractQueriesNvd3(selectedParentData, selectedDim, newCard.queries);

                        initPieData(newCard, selectedParentData);

                        filterPieChartColor(newCard, parent, selectedParentData);

                        const selectedPieData = newCard.chartData[selectedDim];

                        updateDataUsedByChart(newCard.id, selectedPieData);

                        selectedParentData = null;
                    } else {
                        // copy data from parent card
                        newCard.entityData = parent.entityData;
                        newCard.chartData = parent.chartData;

                        const newPieData = newCard.chartData[selectedDim];

                        updateDataUsedByChart(newCard.id, newPieData);
                    }
                } else if (newCard.chartType === 'bar') {
                    // get the data of the selected dimension
                    // if no dimension is selected, start with first element
                    const selectedDim = parent.chartConfig.selection.dimName || '';

                    // get bar data
                    let barData = parent.chartData[selectedDim];

                    let selectedDocumentIds = [];

                    if (selectedDim && (barData.length > 0)) {
                        let barVals = barData[0].values;

                        if (barVals.length > 0) {
                            // get the IDs of documents that are mapped to the bars being selected
                            barVals.forEach((obj) => {
                                if (obj.selected) {
                                    selectedDocumentIds = selectedDocumentIds.concat(obj.entityIds);
                                }
                            });
                        }

                        barData = null;
                        barVals = null;
                    }

                    // parameter preservation from parent card with nvd3 chart
                    newCard.chartConfig.selection.dimName = selectedDim;

                    if (selectedDocumentIds.length > 0) {
                        newCard.entityData = selectedDocumentIds;

                        let selectedParentData = Meteor.utilitiesHelper.fromIdListToObjectList(selectedDocumentIds,
                            trailData[parent.entityId]);

                        extractQueriesNvd3(selectedParentData, selectedDim, newCard.queries);

                        initBarData(newCard, selectedParentData);

                        filterBarChartColor(newCard, parent, selectedParentData);

                        const selectedBarData = newCard.chartData[selectedDim];

                        updateDataUsedByChart(newCard.id, selectedBarData);

                        selectedParentData = null;
                    } else {
                        // copy data from parent card
                        newCard.entityData = parent.entityData;
                        newCard.chartData = parent.chartData;

                        const newBarData = newCard.chartData[selectedDim];

                        updateDataUsedByChart(newCard.id, newBarData);
                    }
                }

                updateDBAddNewCard(parentSources, parentIndex, parent, newCard);
            }

            /*
            Create trail cards using depth-first search, given the root of a chain to be compared
             */
            function createCardDfs(compCard, parent, parentIndex, newSubtreePos) {
                if (compCard.isCompare) {
                    console.log(`Create card: ${compCard.id}`);
                    if (compCard.id === $scope.comparePathSubrootId) {
                        // for the subroot of the comparison chain, use user-selected subset of the data
                        createCardByCompareFilter(parent.entityId, parent.sources, true, parent,
                            parentIndex, compCard, newSubtreePos);
                    } else {
                        createCardByCompareFilter(parent.entityId, parent.sources, false, parent,
                            parentIndex, compCard, null);
                    }

                    const newCardIndex = $scope.trail.cards.length - 1;
                    const newCard = $scope.trail.cards[newCardIndex];
                    parent = newCard;
                    parentIndex = newCardIndex;
                }

                compCard.children.forEach((cid) => {
                    createCardDfs(getCardById(cid), parent, parentIndex, null);
                });
            }

            /*
            * Recursively visit all children given a root of a subtree and store their coordinates
            */
            function getSubtreeCoordsDfs(compCard) {
                let d = {
                    x: compCard.x,
                    y: compCard.y,
                    width: compCard.width,
                    height: compCard.height
                };

                $scope.currSubtreeCoord.push(d);

                compCard.children.forEach((cid) => {
                    getSubtreeCoordsDfs(getCardById(cid));
                });
            }

            /*
            * Return the starting position of the root of the comparison subtree
            */
            function calcNewSubtreePos(root, coords) {
                if (coords && coords.length > 0) {
                    // get center of root card
                    const x = root.x + root.width/2;
                    const y = root.y + root.height/2;

                    let x1, y1;

                    const maxXObj = coords.reduce(function(a, b) {
                        return a.x > b.x ? a : b;
                    });

                    const maxYObj = coords.reduce(function(a, b) {
                        return a.y > b.y ? a : b;
                    });

                    const minXObj = coords.reduce(function(a, b) {
                        return a.x < b.x ? a : b;
                    });

                    const minYObj = coords.reduce(function(a, b) {
                        return a.y < b.y ? a : b;
                    });

                    // get the center coordinate of the bounding box of the subtree
                    if (coords.length === 1) {
                        x1 = maxXObj.x + maxXObj.width/2;
                        y1 = maxYObj.y + maxYObj.height/2;
                    } else {
                        if (minXObj.x === maxXObj.x) {
                            // cards are vertically aligned
                            x1 = maxXObj.x + maxXObj.width/2;
                        } else {
                            x1 = minXObj.x + (maxXObj.x + maxXObj.width - minXObj.x)/2;
                        }

                        if (minYObj.y === maxYObj.y) {
                            // cards are horizontally aligned
                            y1 = maxYObj.y + maxYObj.height/2;
                        } else {
                            y1 = minYObj.y + (maxYObj.y + maxYObj.height - minYObj.y)/2;
                        }
                    }

                    const dx = x - x1;
                    const dy = y - y1;

                    let pos;

                    if (dx < 0) { // subtree is on the right
                        if (dy < 0) { // bottom right
                            if (minXObj.x <= (root.x + root.width)) {
                                pos = "right";
                            } else {
                                pos = "top";
                            }
                        } else { // top right
                            if (minXObj.x <= (root.x + root.width)) {
                                pos = "right";
                            } else {
                                pos = "bottom";
                            }
                        }
                    } else { // subtree is on the left
                        if (dy < 0) { // bottom left
                            if (maxXObj.x + maxXObj.width >= root.x) {
                                pos = "right";
                            } else {
                                pos = "top";
                            }
                        } else { // top left
                            if (maxXObj.x + maxXObj.width >= root.x) {
                                pos = "right";
                            } else {
                                pos = "bottom";
                            }
                        }
                    }

                    // place the new subtree to the the right/top/bottom of the compared subtree
                    switch (pos) {
                        case "right":
                            x1 = maxXObj.x + maxXObj.width + PADDING;
                            y1 = minYObj.y;
                            break;
                        case "top":
                            x1 = minXObj.x;
                            y1 = minYObj.y - PADDING - (maxYObj.y + maxYObj.height - minYObj.y);
                            break;
                        case "bottom":
                            x1 = minXObj.x;
                            y1 = maxYObj.y + maxYObj.height + PADDING;
                            break;
                        default:
                            break;
                    }

                    return {x: x1, y: y1};
                }

                return null;
            }

            /*
             (Gatherplot) Convert the visual queries into a query list for creating comparison chain,
             in the form of,
             [
                  {
                    xDim: x1,
                    yDim: y1
                  },
                  ...
                  {
                    xDim: xn,
                    yDim: yn
                  }
             ]
             */
            function extractQueriesGp(data, xDim, yDim, queries) {
                if (data) {
                    const queriesHashMap = {}; // temporarily store the queries, O(1) lookup

                    if (!yDim && xDim) {
                        data.forEach((obj) => {
                            if (!queriesHashMap[obj[xDim]]) {
                                queriesHashMap[obj[xDim]] = 1;
                            } else {
                                queriesHashMap[obj[xDim]] += 1;
                            }
                        });

                        const keys = Object.keys(queriesHashMap);

                        if (keys && (keys.length > 0)) {
                            keys.forEach((x) => {
                                const query = {};
                                query[xDim] = x;
                                queries.push(query);
                            });
                        }
                    } else if (!xDim && yDim) {
                        data.forEach((obj) => {
                            if (!queriesHashMap[obj[yDim]]) {
                                queriesHashMap[obj[yDim]] = 1;
                            } else {
                                queriesHashMap[obj[yDim]] += 1;
                            }
                        });

                        const keys = Object.keys(queriesHashMap);

                        if (keys && (keys.length > 0)) {
                            keys.forEach((y) => {
                                const query = {};
                                query[yDim] = y;
                                queries.push(query);
                            });
                        }
                    } else if (xDim && yDim) {
                        data.forEach((obj) => {
                            if (!queriesHashMap[obj[xDim]]) {
                                queriesHashMap[obj[xDim]] = {};
                                queriesHashMap[obj[xDim]][obj[yDim]] = 1;
                            } else if (!queriesHashMap[obj[xDim]][obj[yDim]]) {
                                    queriesHashMap[obj[xDim]][obj[yDim]] = 1;
                                } else {
                                    queriesHashMap[obj[xDim]][obj[yDim]] += 1;
                                }
                        });

                        const keys = Object.keys(queriesHashMap);

                        if (keys && (keys.length > 0)) {
                            keys.forEach((k) => {
                                const yVals = Object.keys(queriesHashMap[k]); // multiple selected y-axis attributes
                                yVals.forEach((y) => {
                                    const query = {};
                                    query[xDim] = k;
                                    query[yDim] = y;
                                    queries.push(query);
                                });
                            });
                        }
                    }
                }
            }

            /*
             (Nvd3) Convert the visual queries into a query list for creating comparison chain,
             */
            function extractQueriesNvd3(data, dim, queries) {
                if (data && dim) {
                    const queriesHashMap = {};

                    data.forEach((obj) => {
                        if (!queriesHashMap[obj[dim]]) {
                            queriesHashMap[obj[dim]] = 1;
                        } else {
                            queriesHashMap[obj[dim]] += 1;
                        }
                    });

                    const keys = Object.keys(queriesHashMap);

                    if (keys && (keys.length > 0)) {
                        keys.forEach((k) => {
                            const query = {};
                            query[dim] = k;
                            queries.push(query);
                        });
                    }
                }
            }

            /*
             * Create a new trail card by comparison filtering
             * @param {Number} entityId, the entity id of the new card
             * @param {obj Array} parentSources, the outgoing connections of the parent card
             * @param {Boolean} isSubroot, true if it is the subroot of the subtree
             * @param {Object} parent, the parent card
             * @param {Number} parentIndex, the index of the parent card
             * @param {Object} compCard, the card being compared
             * @param {Object} pos, the position of the root of the new subtree
             */
            function createCardByCompareFilter(entityId, parentSources, isSubroot, parent, parentIndex, compCard, pos) {
                if (!entityId) {
                    console.log('Parent entity id does not exist!');
                    return;
                }

                if (!compCard) {
                    return;
                }

                // Initialize trail card object with chart configuration
                let newCard = null;
                let posFactor = -1;
                let newCardIndex = -1;
                let chartData = [];

                // create a card based on parent's configuration
                newCard = createTrailCardObj(entityId);

                newCard.parent = parent.id;

                // update parent card's children
                parent.children.push(newCard.id);

                if (isSubroot) {
                    // set initial position for the root of the new subtree
                    newCard.x = pos.x;
                    newCard.y = pos.y;
                } else {
                    posFactor = parent.children.length - 1;
                    newCard.x = parent.x + parent.width + PADDING;
                    // spawn the child card always at the right of its parent
                    newCard.y = parent.y + (posFactor * (parent.height + PADDING));
                }

                // initialize chart specific configuration properties
                initGatherplotConfig(newCard);

                // copy configuration from parent's sibling
                newCard.chartType = compCard.chartType;
                newCard.chartConfig = compCard.chartConfig;
                newCard.queries = [];
                newCard.chartData = {};

                newCard.width = parent.width;
                newCard.height = parent.height;
                newCard.nvd3ChartHeight = CHART_HEIGHT_MIN;

                $scope.trail.cards = $scope.trail.cards || [];

                newCardIndex = $scope.trail.cards.push(newCard);

                newCard = $scope.trail.cards[newCardIndex - 1];

                initNvd3ChartEventConfig(newCard.id);

                // initialize gatherplot and other chart data for the trail card
                initGatherplotData(newCard, trailData[entityId]);

                let selectedDocumentIds = [];
                let selectedParentData = [];

                switch (newCard.chartType) {
                    case GATHERPLOT: {
                        // preserve selected dimensions from root
                        newCard.gatherConfig.params = compCard.gatherConfig.params;

                        // preserve color coding from parent
                        const colorDim = newCard.gatherConfig.params.colorDim;
                        const colorCoding = colorDim ? parent.gatherConfig.colorCoding[colorDim] : null;

                        // Chart type not changed
                        if (parent.chartType === GATHERPLOT) {
                            const xDim = parent.gatherConfig.params.xDim;
                            const yDim = parent.gatherConfig.params.yDim;

                            // for subroot, perform a normal filtering
                            if (isSubroot) {
                                const selection = parent.dimsum.selectionSpace || [];

                                // if no data point is selected, use parent card's data for the new trail card
                                if (selection.length === 0 || (selection.length === 1 && selection[0] === -1)) {
                                    initGatherplotData(newCard, $scope.entityDataObjectsList[parent.id]);

                                    updateDataUsedByGatherplot(newCard.id, $scope.entityDataObjectsList[parent.id]);

                                    newCard = filterGatherColorCoding(newCard, colorCoding, colorDim, []);
                                } else {
                                    selectedParentData = $scope.entityDataObjectsList[parent.id].filter((d) => {
                                        return (selection.indexOf(d.id) !== -1);
                                    });

                                    // create new queries for the current card
                                    extractQueriesGp(selectedParentData, xDim, yDim, newCard.queries);

                                    initGatherplotData(newCard, selectedParentData);

                                    updateDataUsedByGatherplot(newCard.id, selectedParentData);

                                    newCard = filterGatherColorCoding(newCard, colorCoding, colorDim,
                                        selectedParentData);
                                }
                            // perform a filtering using the compared card's queries on the parent's dataset
                            } else {
                                // copy comparison counterpart's queries
                                newCard.queries = compCard.queries;

                                const parentData = Meteor.utilitiesHelper.fromIdListToObjectList(parent.entityData,
                                    trailData[parent.entityId]);

                                // use queries to filter the selected data
                                selectedParentData = GetDataByQueriesGp(parentData, newCard.queries, xDim, yDim);

                                if (selectedParentData.length > 0) {
                                    initGatherplotData(newCard, selectedParentData);

                                    updateDataUsedByGatherplot(newCard.id, selectedParentData);

                                    newCard = filterGatherColorCoding(newCard, colorCoding,
                                        colorDim, selectedParentData);
                                } else {
                                    newCard.entityData = [];

                                    // For cards with empty data, set a minimum size
                                    newCard.width = DEFAULT_WIDTH;
                                    newCard.height = DEFAULT_HEIGHT;
                                }
                            }

                            // update parent's selection space
                            if (parent.dimsum.selectionSpace) {
                                Trails.update({ _id: $stateParams.trailId }, {
                                    $set: {
                                        [`cards.${parentIndex}.dimsum.selectionSpace`]: parent.dimsum.selectionSpace,
                                    },
                                });
                            }
                        // Chart type changed from nvd3
                        } else {
                            const parentDim = parent.chartConfig.selection.dimName;

                            chartData = parentDim ? parent.chartData[parentDim] : [];

                            if (isSubroot) {
                                selectedDocumentIds = getSelectedIdsFromNvd3ChartData(parent.chartType, chartData);

                                selectedParentData = trailData[entityId].filter((d) => {
                                    return (selectedDocumentIds.indexOf(d._id) !== -1);
                                });

                                if (selectedParentData.length > 0) {
                                    extractQueriesNvd3(selectedParentData, parentDim, newCard.queries);
                                } else {
                                    selectedParentData = trailData[entityId];
                                }

                                initGatherplotData(newCard, selectedParentData);
                            } else {
                                newCard.queries = compCard.queries;

                                selectedDocumentIds = getIdsByQueriesNvd3(parent.chartType,
                                    chartData, newCard.queries, parentDim);

                                selectedParentData = trailData[entityId].filter((d) => {
                                    return (selectedDocumentIds.indexOf(d._id) !== -1);
                                });

                                if (selectedParentData.length > 0) {
                                    initGatherplotData(newCard, selectedParentData);
                                } else {
                                    newCard.entityData = [];

                                    // For cards with empty data, set a minimum size
                                    newCard.width = DEFAULT_WIDTH;
                                    newCard.height = DEFAULT_HEIGHT;
                                }
                            }

                            updateDataUsedByGatherplot(newCard.id, selectedParentData);
                        }
                        break;
                    }
                    default: { // pie or bar chart
                        // parameter preservation from parent card with nvd3 chart
                        newCard.chartConfig = compCard.chartConfig;

                        // get the data of the selected dimension
                        // if no dimension is selected, start with undefined
                        const parentDim = parent.chartConfig.selection.dimName;

                        // the parameterization of parent's sibling
                        const currDim = newCard.chartConfig.selection.dimName;

                        // Chart type changed from gatherplot
                        if (parent.chartType === GATHERPLOT) {
                            const xDim = parent.gatherConfig.params.xDim;
                            const yDim = parent.gatherConfig.params.yDim;

                            if (isSubroot) {
                                const selection = parent.dimsum.selectionSpace || [];

                                if (selection.length === 0 || (selection.length === 1 && selection[0] === -1)) {
                                    // When no data is selected, load all parent data
                                    selectedParentData = $scope.entityDataObjectsList[parent.id];
                                } else {
                                    // Only load selected data into the new card
                                    selectedParentData = $scope.entityDataObjectsList[parent.id].filter((d) => {
                                        return selection.indexOf(d.id) !== -1;
                                    });

                                    extractQueriesGp(selectedParentData, xDim, yDim, newCard.queries);
                                }

                                initNvd3ChartData(newCard, selectedParentData);
                            } else {
                                newCard.queries = compCard.queries;

                                const parentData = Meteor.utilitiesHelper.fromIdListToObjectList(parent.entityData,
                                    trailData[parent.entityId]);

                                selectedParentData = GetDataByQueriesGp(parentData, newCard.queries, xDim, yDim);

                                initNvd3ChartData(newCard, selectedParentData);

                                if (selectedParentData.length === 0) {
                                    newCard.entityData = [];

                                    // For cards with empty data, set a minimum size
                                    newCard.width = DEFAULT_WIDTH;
                                    newCard.height = DEFAULT_HEIGHT;
                                }
                            }
                        // Chart type may have been changed between pie and bar chart
                        } else {
                            chartData = parentDim ? parent.chartData[parentDim] : [];

                            if (isSubroot) {
                                selectedDocumentIds = getSelectedIdsFromNvd3ChartData(parent.chartType, chartData);

                                if (selectedDocumentIds.length > 0) {
                                    newCard.entityData = selectedDocumentIds;

                                    selectedParentData = Meteor.utilitiesHelper.fromIdListToObjectList(
                                        newCard.entityData, trailData[parent.entityId]);

                                    extractQueriesNvd3(selectedParentData, parentDim, newCard.queries);

                                    initNvd3ChartData(newCard, selectedParentData);

                                    if (parent.chartType === newCard.chartType) {
                                        filterNvd3ChartColor(newCard, parent, selectedParentData);
                                    }
                                } else {
                                    // copy data from parent card
                                    newCard.entityData = parent.entityData;

                                    selectedDocumentIds = newCard.entityData;

                                    selectedParentData = trailData[entityId].filter((d) => {
                                        return selectedDocumentIds.indexOf(d._id) !== -1;
                                    });

                                    initNvd3ChartData(newCard, selectedParentData);
                                }
                            } else {
                                newCard.queries = compCard.queries;

                                selectedDocumentIds = getIdsByQueriesNvd3(parent.chartType,
                                    chartData, newCard.queries, parentDim);

                                selectedParentData = trailData[entityId].filter((d) => {
                                    return selectedDocumentIds.indexOf(d._id) !== -1;
                                });

                                initNvd3ChartData(newCard, selectedParentData);

                                if (selectedParentData.length > 0) {
                                    if (parent.chartType === newCard.chartType) {
                                        filterNvd3ChartColor(newCard, parent, selectedParentData);
                                    }
                                } else {
                                    newCard.entityData = [];

                                    // For cards with empty data, set a minimum size
                                    newCard.width = DEFAULT_WIDTH;
                                    newCard.height = DEFAULT_HEIGHT;
                                }
                            }
                        }
                        // update nvd3 chart data
                        updateDataUsedByChart(newCard.id, newCard.chartData[currDim]);
                        break;
                    }
                }

                selectedParentData = null;
                selectedDocumentIds = null;

                updateDBAddNewCard(parentSources, parentIndex, parent, newCard);
            }

            /**
             * Return a list of document IDs of the selected NVD3 chart data points
             * @param {String} chartType, the type of the chart
             * @param data, the chart data
             * @returns {Array}
             */
            function getSelectedIdsFromNvd3ChartData(chartType, data) {
                let ids = [];
                let values = [];

                if (!data || (data.length === 0)) return ids;

                switch (chartType) {
                    case BAR_CHART: {
                        values = data[0].values;
                        break;
                    }
                    case PIE_CHART: {
                        values = data;
                        break;
                    }
                    default: {
                        break;
                    }
                }

                if (values && (values.length > 0)) {
                    values.forEach((obj) => {
                        if (obj.selected) {
                            ids = ids.concat(obj.entityIds);
                        }
                    });
                }

                return ids;
            }

            /*
            gatherplot
             */
            function GetDataByQueriesGp(data, queries, xDim, yDim) {
                const result = [];

                if (queries && data && (data.length > 0)) {
                    if (queries.length > 0) {
                        if (!xDim && !yDim) {
                            return data;
                        } else if ((!xDim && yDim) || (!yDim && xDim)) {
                            data.forEach((obj) => {
                                queries.forEach((q) => {
                                    const dim = Object.keys(q)[0];

                                    if (dim && (obj[dim] === q[dim])) {
                                        result.push(obj);
                                    }
                                });
                            });
                        } else {
                            data.forEach((obj) => {
                                queries.forEach((q) => {
                                    const dim1 = Object.keys(q)[0];
                                    const dim2 = Object.keys(q)[1];

                                    if (dim1 && dim2 && (obj[dim1] === q[dim1]) && (obj[dim2] === q[dim2])) {
                                        result.push(obj);
                                    }
                                });
                            });
                        }
                    } else {
                        return data;
                    }
                }

                return result;
            }

            /**
             * Get selected NVD3 chart data document IDs by applying queries,
             * if queries is empty, return all documents of parent's
             * @param type, the chart type
             * @param data
             * @param queries
             * @param dim
             * @returns {Array}
             */
            function getIdsByQueriesNvd3(type, data, queries, dim) {
                switch (type) {
                    case BAR_CHART: {
                        return getIdsByQueriesBar(data, queries, dim);
                        break;
                    }
                    case PIE_CHART: {
                        return getIdsByQueriesPie(data, queries, dim);
                        break;
                    }
                    default: {
                        break;
                    }
                }

                return [];
            }

            /**
             * Get selected bar chart document IDs by applying queries
             * @param data
             * @param queries
             * @param dim
             * @returns {Array}
             */
            function getIdsByQueriesBar(data, queries, dim) {
                let result = [];

                if (queries && dim && data && (data.length > 0)) {
                    const values = data[0].values;

                    if (queries.length === 0) {
                        values.forEach((v) => {
                            result = result.concat(v.entityIds);
                        });
                    } else {
                        values.forEach((v) => {
                            queries.forEach((q) => {
                                if (q[dim] === v.x) {
                                    result = result.concat(v.entityIds);
                                }
                            });
                        });
                    }
                }

                return result;
            }

            /**
             * Get selected pie chart document IDs by applying queries
             * @param data
             * @param queries
             * @param dim
             * @returns {Array}
             */
            function getIdsByQueriesPie(data, queries, dim) {
                let result = [];

                if (queries && dim && data && (data.length > 0)) {
                    if (queries.length === 0) {
                        data.forEach((v) => {
                            result = result.concat(v.entityIds);
                        });
                    } else {
                        data.forEach((v) => {
                            queries.forEach((q) => {
                                if (q[dim] === v.x) {
                                    result = result.concat(v.entityIds);
                                }
                            });
                        });
                    }
                }

                return result;
            }

            /*
             * Create a new trail card with the selected entity from the pivot combobox
             * @param {Number} entityId, the entity id of the new card
             * @param {Object} parentLoc, the coordinate of the parent card
             * @param {obj Array} parentSources, the outgoing connections of the parent card
             * @param {Object} parentState, the parent card
             * @param {Number} parentIndex, the index of the parent card
             * @param {int Array} selectionArray, the array of ids of selected entities, for gatherplot only
             * @param {Object} link, the link object for querying the joining entity
             */
            function createCardByPivot(entityId, parentSources, selectionArray, parent, parentIndex, link) {
                // get the entity object
                const entityObject = Meteor.utilitiesHelper.getEntityById($scope.entities, entityId);

                if (!entityObject) {
                    console.log('Entity does not exist!');
                    return;
                }

                /* Initialize trail card object with chart configuration */
                let newCard = null;
                let posFactor = -1;
                let newCardIndex = -1;

                // create a card based on parent's configuration
                newCard = createTrailCardObj(entityId);

                newCard.parent = parent.id;

                // update parent card's children
                parent.children.push(newCard.id);

                posFactor = parent.children.length - 1;
                newCard.x = parent.x + parent.width + PADDING;
                // not to let the new card overlay with each other
                newCard.y = parent.y + (posFactor * (parent.height + PADDING));

                // initialize chart specific configuration properties
                initGatherplotConfig(newCard);

                newCard.chartConfig = parent.chartConfig;
                newCard.chartType = parent.chartType;

                newCard.width = parent.width;
                newCard.height = parent.height;
                newCard.nvd3ChartHeight = CHART_HEIGHT_MIN;

                $scope.trail.cards = $scope.trail.cards || [];

                newCardIndex = $scope.trail.cards.push(newCard);

                newCard = $scope.trail.cards[newCardIndex - 1];

                initNvd3ChartEventConfig(newCard.id);

                // initialize gatherplot and other chart data for the trail card
                initGatherplotData(newCard, trailData[entityId]);

                /* Pivot from an existing card.
                 * the data from parent entity is loaded,
                 * based on the selected data set, a set of doc ids are sent to server
                 * server returns a set of ids of the child entity to load:
                 * either pre-load the data and fetch selected ones, we don't need to make the server call,
                 * or we only load data */

                const args = {};
                args.fromEntityId = parent.entityId;
                args.toEntityId = entityId;
                args.linkField = link.linkField;
                args.isSelfJoin = false;

                // if it is self join
                if (args.fromEntityId === args.toEntityId) {
                    args.isSelfJoin = true;
                    args.fromKey = link.fromKey;
                    args.toKey = link.toKey;
                }

                if (parent.chartType === 'gp') {
                    // if no data is selected
                    if (selectionArray.length === 0 || (selectionArray.length === 1 && selectionArray[0] === -1)) {
                        args.selectedFromEntityIds = parent.entityData;
                    } else { // if some data are selected
                        let selectedParentsEntities = [];

                        selectedParentsEntities = $scope.entityDataObjectsList[parent.id].filter((d) => {
                            return selectionArray.indexOf(d.id) !== -1;
                        });

                        args.selectedFromEntityIds = selectedParentsEntities.map((d) => {
                            return d._id;
                        });
                    }

                    Meteor.call('getDataFromRelationship', args, (error, result) => {
                        if (error) {
                            console.log(error.reason);
                            updateDBAddNewCard(parentSources, parentIndex, parent, newCard);
                        } else if (result.length > 0) {
                                newCard.entityData = result;

                                const selectedToEntities = Meteor.utilitiesHelper.fromIdListToObjectList(
                                    result, trailData[entityId]);

                                updateDataUsedByGatherplot(newCard.id, selectedToEntities);
                                updateDBAddNewCard(parentSources, parentIndex, parent, newCard);
                            } else {
                                console.log('No data is found');
                            }
                    });
                } else if (parent.chartType === 'pie') {
                    // get the data of the selected dimension
                    let selectedDim = parent.chartConfig.selection.dimName;

                    // if no dimension is selected, get the first dimension as default
                    if (!selectedDim) {
                        selectedDim = getDimNonkey(parent.chartData);
                    }

                    const pieData = parent.chartData[selectedDim];
                    let selectedEntityIds = [];

                    if (selectedDim && pieData) {
                        // get the IDs of documents that are mapped to the sector being selected
                        pieData.forEach((obj) => {
                            if (obj.selected) {
                                selectedEntityIds = selectedEntityIds.concat(obj.entityIds);
                            }
                        });
                    }

                    if (selectedEntityIds.length > 0) {
                        args.selectedFromEntityIds = selectedEntityIds;
                    } else {
                        args.selectedFromEntityIds = parent.entityData;
                    }

                    Meteor.call('getDataFromRelationship', args, (error, result) => {
                        if (error) {
                            console.log(error.reason);
                            updateDBAddNewCard(parentSources, parentIndex, parent, newCard);
                        } else if (result.length > 0) {
                            // TODO: need to validate the logic as filtering

                            newCard.entityData = result;

                            const selectedToEntities = trailData[entityId].filter((d) => {
                                return result.indexOf(d._id) !== -1;
                            });

                            initPieData(newCard, selectedToEntities);

                            // render the data of the first attribute by default
                            const selectedPieData = newCard.chartData[getDimNonkey(parent.chartData)];

                            updateDataUsedByChart(newCard.id, selectedPieData);

                            updateDBAddNewCard(parentSources, parentIndex, parent, newCard);
                        } else {
                            console.log('No data is found');
                        }
                    });
                } else if (parent.chartType === 'bar') {
                    // get the data of the selected dimension
                    let selectedDim = parent.chartConfig.selection.dimName;

                    // if no dimension is selected, get the first dimension as default
                    if (!selectedDim) {
                        selectedDim = getDimNonkey(parent.chartData);
                    }

                    // get bar data
                    const barData = parent.chartData[selectedDim];

                    let selectedEntityIds = [];

                    if (selectedDim && (barData.length > 0)) {
                        const barVals = barData[0].values;

                        if (barVals.length > 0) {
                            // get the IDs of documents that are mapped to the bars being selected
                            barVals.forEach((obj) => {
                                if (obj.selected) {
                                    selectedEntityIds = selectedEntityIds.concat(obj.entityIds);
                                }
                            });
                        }
                    }

                    if (selectedEntityIds.length > 0) {
                        args.selectedFromEntityIds = selectedEntityIds;
                    } else {
                        args.selectedFromEntityIds = parent.entityData;
                    }

                    Meteor.call('getDataFromRelationship', args, (error, result) => {
                        if (error) {
                            console.log(error.reason);
                            updateDBAddNewCard(parentSources, parentIndex, parent, newCard);
                        } else if (result.length > 0) {
                            // TODO: need to validate the logic as filtering

                            newCard.entityData = result;

                            const selectedToEntities = trailData[entityId].filter((d) => {
                                return result.indexOf(d._id) !== -1;
                            });

                            initBarData(newCard, selectedToEntities);

                            // render the data of the first attribute by default
                            const selectedBarData = newCard.chartData[getDimNonkey(parent.chartData)];

                            updateDataUsedByChart(newCard.id, selectedBarData);

                            updateDBAddNewCard(parentSources, parentIndex, parent, newCard);
                        } else {
                            console.log('No data is found');
                        }
                    });
                }
            }

            function isRoot(card) {
                return card.parent === null;
            }

            function isSubroot(card) {
                return card.id === $scope.currCompareRootId;
            }

            function doDfs(card, status, func) {
                if (func) {
                    func(card, status);
                }

                card.children.forEach((cid) => {
                    doDfs(getCardById(cid), status, func);
                });
            }

            function setCompareModeOnNode(card, status) {
                if (isRoot(card) || isSubroot(card)) {
                    return;
                }

                if (card.isCompare && !status) {
                    card.isCompare = false;
                }

                card.compareOn = status;
                card.compareDisabled = !status;
            }

            function uncheckCompareCard(card, status) {
                if (isRoot(card) || isSubroot(card)) {
                    return;
                }

                card.isCompare = status;
            }

            function disableCompareCard(card, status) {
                if (isRoot(card) || isSubroot(card)) {
                    return;
                }

                card.compareDisabled = status;
            }

            function getCardById(id) {
                const result = $scope.trail.cards.find((d) => {
                    return d.id === id;
                });

                return result;
            }

            /* Returns a dimension that is not a unique identifier */
            function getDimNonkey(data) {
                if (data) {
                    const dims = Object.keys(data);
                    return dims.find((dim) => {
                        return !isKey(dim, EXCLUDE_KEYS);
                    });
                }

                return '';
            }

            function isKey(dim, criteria) {
                for (let i = 0; i < criteria.length; i++) {
                    if (dim.indexOf(criteria[i]) !== -1) {
                        return true;
                    }
                }
                return false;
            }

            /* Database update operations when adding a new trail card:
             * update parent card's connections,
             * update the global UUID,
             * push the new card object to the trail's cards array
             * update the number of children of the parent card */
            function updateDBAddNewCard(parentSources, index, parent, newCard) {
                newCard = angular.copy(newCard); // get rid of hashkey

                // update parent's connections for jsPlumb
                if (parentSources.length > 0) {
                    parentSources[0].connections.push({
                        uuid: newCard.targets[0].uuid,
                    });

                    newCard.parentSources = parentSources[0];

                    Trails.update({ _id: $stateParams.trailId }, {
                        $set: {
                            [`cards.${index}.sources`]: parentSources,
                        },
                    });
                }

                // update the UUID counter to produce unique ids
                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        globalUUIDCounter: $scope.trail.globalUUIDCounter,
                    },
                });

                if (parent) {
                    // update the status of the parent card
                    if (parent.children.length === 0) {
                        Trails.update({ _id: $stateParams.trailId }, {
                            $set: {
                                [`cards.${index}.isParent`]: false,
                            },
                        });
                    } else {
                        Trails.update({ _id: $stateParams.trailId }, {
                            $set: {
                                [`cards.${index}.children`]: parent.children,
                            },
                        });

                        Trails.update({ _id: $stateParams.trailId }, {
                            $set: {
                                [`cards.${index}.isParent`]: true,
                            },
                        });
                    }

                    // copy the selection of the parent card to the new card
                    newCard.parentSelection = [];

                    if (parent.chartType === 'gp') {
                        newCard.parentSelection = parent.dimsum.selectionSpace;

                        // reset trail selection in parent card
                        Trails.update({ _id: $stateParams.trailId }, {
                            $set: {
                                [`cards.${index}.dimsum.selectionSpace`]: [],
                            },
                        });
                    } else {
                        // store a list of indices of which the data points are selected
                        const selectedIndices = [];
                        const selectedDim = parent.chartConfig.selection.dimName;

                        if (parent.chartType === 'bar') {
                            parent.chartData[selectedDim][0].values.forEach((d, i) => {
                                if (d.selected) {
                                    selectedIndices.push(i);
                                    $scope.entityDataObjectsList[parent.id][0].values[i].selected = false;

                                    Trails.update({ _id: $stateParams.trailId }, {
                                        $set: {
                                            [`cards.${index}.chartData.${selectedDim}.0.values.${i}.selected`]: false,
                                        },
                                    });
                                }
                            });
                        } else { // pie
                            parent.chartData[selectedDim].forEach((d, i) => {
                                if (d.selected) {
                                    selectedIndices.push(i);
                                    $scope.entityDataObjectsList[parent.id][i].selected = false;

                                    Trails.update({ _id: $stateParams.trailId }, {
                                        $set: {
                                            [`cards.${index}.chartData.${selectedDim}.${i}.selected`]: false,
                                        },
                                    });
                                }
                            });
                        }

                        newCard.parentSelection = selectedIndices;
                    }
                }

                // add the new card to the current trail
                Trails.update({ _id: $stateParams.trailId }, {
                    $push: {
                        cards: newCard,
                    },
                }, (error) => {
                    if (error) {
                        console.log('Update cards error: ', error);
                    }
                });
            }

            // create a trail card base object, without chart configuration
            function createTrailCardObj(entityId) {
                const id = getNextUUID();
                return {
                    id,
                    name: `Trail Card ${id}`,
                    isParent: false,
                    parent: null,
                    children: [],
                    sources: [{
                        uuid: getNextUUID(),
                        connections: [],
                    }],
                    targets: [{
                        uuid: getNextUUID(),
                    }],
                    x: INIT_LEFT,
                    y: INIT_TOP,
                    entityId,
                    entityData: [],
                    type: 'trail',
                    width: DEFAULT_WIDTH,
                    height: DEFAULT_HEIGHT,
                };
            }

            /*
            Create a trail card with no data, due to
            1) dependent data entity is deleted
            2) querying from a parent card in a comparison filtering

            @param {Object} card: the parent card
             */
            function createTrailCardNoData(parent) {
                const trailCardNoData = {};

                trailCardNoData.id = parent.id;
                trailCardNoData.isParent = parent.isParent;
                trailCardNoData.parent = parent.parent;
                trailCardNoData.children = parent.children;
                trailCardNoData.sources = parent.sources;
                trailCardNoData.targets = parent.targets;
                trailCardNoData.type = parent.type;
                trailCardNoData.x = parent.x;
                trailCardNoData.y = parent.y;
                trailCardNoData.nvd3ChartHeight = parent.nvd3ChartHeight;

                if (parent.type === 'dashboard') {
                    trailCardNoData.gridster = parent.gridster;
                }

                return trailCardNoData;
            }

            // create a new trail card instance with gatherplot configuration
            function initGatherplotConfig(card) {
                if (card) {
                    const basicConfig = angular.copy(GATHERPLOT_OPT);
                    basicConfig.gatherplotId = card.id;
                    basicConfig.colorCoding = {};

                    const dims = $scope.getEntityDimensions(card.entityId);
                    basicConfig.dims = dims.map((d) => { return d.name; });

                    card.gatherConfig = basicConfig;
                    card.gatherConfig.params = {
                        xDim: '',
                        yDim: '',
                        colorDim: '',
                        relativeMode: 'absolute',
                    };

                    card.gatherBorder = true;
                    card.gatherRound = true;
                    card.gatherShapeRenderingMode = 'auto';
                    card.context = {
                        translate: [0, 0],
                        scale: 1,
                    };
                    card.dimsum = {
                        selectionSpace: [],
                    };
                }
            }

            // create a new trail card instance with pie chart configuration
            function initNvd3ChartConfig(card) {
                if (card) {
                    card.chartConfig = {
                        selection: {
                            dimName: '',
                        },
                    };
                }
            }

            function initNvd3ChartEventConfig(cardId) {
                // add event listeners for nvd3 charts
                $scope.nvd3ChartConfigs[cardId] = {
                    pie: angular.copy(pieTrailConfig),
                    bar: angular.copy(barTrailConfig),
                };
            }

            function initNvd3ChartHeight(cardId, height) {
                $scope.nvd3ChartConfigs[cardId].bar.chart.height = height || CHART_HEIGHT_MIN;
                $scope.nvd3ChartConfigs[cardId].pie.chart.height = height || CHART_HEIGHT_MIN;
            }

            function initNvd3ChartConfigDash(cardId) {
                $scope.nvd3ChartConfigsDash[cardId] = {
                    pie: angular.copy(pieOptDash),
                    bar: angular.copy(disBarOptDash),
                };
            }

            function initNvd3ChartHeightDash(card) {
                $scope.nvd3ChartConfigsDash[card.id].bar.chart.height = calcHeightDash(
                    BASE_HEIGHT_BAR, card.gridster.size.y);
                $scope.nvd3ChartConfigsDash[card.id].pie.chart.height = calcHeightDash(
                    BASE_HEIGHT_PIE, card.gridster.size.y);
            }

            function calcHeightDash(baseHeight, sizeFactor) {
                let result = baseHeight * sizeFactor;
                const boxHeaderHeight = 52;

                // when y is 1, the header takes up 52px,
                // needs to add header height every time y is incremented
                if (sizeFactor > 1) {
                    result += sizeFactor * boxHeaderHeight;
                }

                return result;
            }

            function checkAndResetForDeletingCurrentCard() {
                if (!isCurrentCardInShow()) {
                    resetToFirstSlide();
                }
            }

            function isCurrentCardInShow() {
                if (!$scope.fullscreenObjects) {
                    return true;
                }

                if (!$scope.fullscreenState) {
                    return true;
                }

                const cardlist = $scope.fullscreenObjects.map((d) => {
                    return d.id;
                });

                if (cardlist.length === 0) {
                    return true;
                }

                if (cardlist.indexOf($scope.fullscreenState.id) !== -1) {
                    return true;
                }

                return false;
            }

            function isSlideAdded(id) {
                const res = $scope.fullscreenObjects.find((f) => {
                    return f.id === id;
                });

                return res;
            }

            function resetToFirstSlide() {
                $scope.fullscreenState = $scope.trail.cards[$scope.fullscreenObjects[0].id];
                $scope.slideIndex = 0;
            }

            /* create the title slide when a slide is newly created */
            function createTitleSlide() {
                const titleMarkdown = `# ${$scope.slideshow.name}\n##  from ${$scope.trail.name}`;

                let newFullScreenObject = {
                    type: 'md',
                    markdown: titleMarkdown,
                    label: 'Title',
                    subtitle: '',
                    scaleFactor: 1,
                };

                newFullScreenObject = angular.copy(newFullScreenObject);

                Slideshows.update({ _id: $scope.slideshow._id }, {
                    $push: {
                        slideshowContent: newFullScreenObject,
                    },
                }, (error) => {
                    if (error) {
                        console.log('addSlideshowModal: update slideshow content failed!');
                    }
                });

                return newFullScreenObject;
            }

            /* Returns only an array of trail objects to be copied to the dashboard */
            function trailDataCopy() {
                let target = [];
                const source = $scope.trail.cards;

                if (source) {
                    target = source.filter((d) => {
                        return d.type === 'trail';
                    });
                }

                return angular.copy(target);
            }

            function createMarkdownCard(mdObj, parentLoc) {
                if (!mdObj) return;

                if (mdObj.slide) {
                    const processedMd = filterMarkdownContent(mdObj.slide);
                    if (processedMd) mdObj.slide = processedMd;
                }

                const md = {
                    id: getNextUUID(),
                    name: 'Markdown',
                    x: parentLoc.left + 1200,
                    y: parentLoc.top + 1200,
                    scaleFactor: 1,
                    slide: mdObj.slide,
                    type: mdObj.type,
                };

                $scope.trail.cards = $scope.trail.cards || [];

                // update the UUID counter to produce unique ids
                Trails.update({ _id: $stateParams.trailId }, {
                    $set: {
                        globalUUIDCounter: $scope.trail.globalUUIDCounter,
                    },
                });

                // update one property of a trail instance
                Trails.update({ _id: $stateParams.trailId }, {
                    $push: {
                        cards: md,
                    },
                });

                logActivity('createMarkdownCard', 'Created markdown card', md.id);
            }

            function filterMarkdownContent(slide) {
                // pre-render the markdown
                const preHTML = $.parseHTML(slide);
                const elements = [];
                let result = '';

                for (let i = 0; i < preHTML.length; i++) {
                    let element = preHTML[i].outerHTML ? preHTML[i].outerHTML : preHTML[i].textContent;
                    const mediaType = preHTML[i].nodeName;

                    // add a wrapper for video responsiveness
                    if (EMBED_TYPES.indexOf(mediaType) !== -1) {
                        element = `<div class="embed-container">${element}</div>`;
                    }

                    elements.push(element);
                }

                if (elements) {
                    elements.forEach((e) => { result += e; });
                }

                return result;
            }

            // function initDashboard(dashboard) {
            //     $scope.stateObjectsDashboard = trailDataCopy();
            //
            //     /* fill up the grid canvas horizontally to vertically */
            //     var colIndex = 0;
            //     var rowIndex = 0;
            //     $scope.stateObjectsDashboard.forEach(function(d) {
            //         d.gridster = {
            //             size: {
            //                 x: 1,
            //                 y: 1,
            //             },
            //             position: {
            //                 row: rowIndex,
            //                 col: colIndex
            //             },
            //             minSize: {
            //                 x: 1,
            //                 y: 1
            //             }
            //         };
            //         if (colIndex < $scope.gridsterOpts.columns-1) {
            //             colIndex++;
            //         } else {
            //             colIndex = 0;
            //             rowIndex++;
            //         }
            //     });
            //
            //     dashboard.grids = $scope.stateObjectsDashboard;
            //
            //     Dashboards.update({_id: dashboard._id}, {
            //         $set : {
            //             "grids": dashboard.grids
            //         }
            //     });
            //
            //     $scope.dashboard = dashboard;
            // }

            /* Update dashboard card position after drag event*/
            function updateDashboardPos(girdItem, index) {
                Dashboards.update({ _id: $scope.dashboard._id }, {
                    $set: {
                        [`grids.${index}.gridster.position`]: girdItem.position,
                    },
                });
            }

            /* Update dashboard card size after resize event*/
            function updateDashboardSize(gridItem, index) {
                Dashboards.update({ _id: $scope.dashboard._id }, {
                    $set: {
                        [`grids.${index}.gridster.size`]: gridItem.size,
                    },
                });
            }

            /* Dispatch a resize event for scaling markdown content */
            function windowResize(time) {
                $timeout(() => {
                    window.dispatchEvent(new Event('resize'));
                }, time);
            }

            /* Set panel available height */
            function setPanelHeight() {
                const $navMain = $('.navbar');
                const $navTab = $('.nav-tabs');
                const browserContentHeight = $(window).innerHeight();
                const navMainHeight = $navMain.outerHeight();
                const navTabHeight = $navTab.outerHeight(true); // include margin top
                const marginBot = 10;
                $scope.panelHeight = browserContentHeight - navMainHeight - navTabHeight - marginBot;
            }

            /* Get panel body available height for dashboard and slideshow */
            function setPanelBodyHeight() {
                const $heading = $('.slideshow-panel .panel-heading');
                const headingHeight = $heading.outerHeight(true);
                $scope.panelBodyHeight = $scope.panelHeight - headingHeight;
            }

            function setTrailPanelBodyHeight() {
                const $trailPanelHead = $('.trail-panel-heading');
                const trailPanelHeadHeight = $trailPanelHead.outerHeight(true);
                $scope.trailPanelBodyHeight = $scope.panelHeight - trailPanelHeadHeight;
            }

            /* Get trail slideshow panel content available height */
            function setSlideshowPanelPreviewHeight() {
                const $slideInput = $('#slide-input');
                const slideInputHeight = $slideInput.outerHeight(true);
                const botMargin = 5;
                $scope.slideshowContentHeight = $scope.panelBodyHeight - slideInputHeight - botMargin;
            }

            /* Initialize a list of scale factors as 1 */
            function initializeSlideshowPreviewScale() {
                // reset scale factors for each preivew slide item
                $scope.previewScales = [];

                // initialize n items, where n is the number of slides
                for (let i = 0; i < $scope.fullscreenObjects.length; i++) {
                    $scope.previewScales.push(1);
                }
            }

            /* Calculate scale factor for each item in the sidebar preview list */
            function scalePreviewContent() {
                const $sidePreviewBoxes = $('.sidebar-box .preview-box');

                $sidePreviewBoxes.each((i) => {
                    const $box = $($sidePreviewBoxes[i]);
                    const boxHeight = $box.outerHeight();
                    const boxWidth = $box.outerWidth();
                    const contentHeight = $box.find('.slide-md-content').outerHeight();
                    const contentWidth = $box.find('.slide-md-content').outerWidth();

                    $scope.previewScales[i] = Math.min(boxHeight / contentHeight,
                        boxWidth / contentWidth);
                });
            }

            function markdownScale() {
                $timeout(() => {
                    if ($scope.isFullScreen) {
                        setFullscreenContentHeight();
                    } else {
                        // scale elements in trail slideshow view
                        setPanelHeight();
                        setPanelBodyHeight();
                        setSlideshowPanelPreviewHeight();
                        $scope.slideshowSideBarHeight = $scope.panelBodyHeight;
                    }

                    $scope.slideHeight = $scope.slideshowContentHeight - $('#slideNav').outerHeight();
                    $scope.slideCenterZoom = $scope.slideHeight / slideZoomFactor;
                }, 10);

                $timeout(() => {
                    if (!$scope.isFullScreen) scalePreviewContent(); // scale content of all preview items

                    const slideshowMd = $scope.fullscreenObjects[$scope.slideIndex];

                    if (slideshowMd) scaleSlideMdContent(slideshowMd); // scale selected slide content
                }, 50);
            }

            function trailScale() {
                if ($scope.isFullScreen) {
                    setFullscreenContentHeight();
                } else {
                    // scale elements in trail slideshow view
                    setPanelHeight();
                    setPanelBodyHeight();
                    setSlideshowPanelPreviewHeight();
                    $scope.slideshowSideBarHeight = $scope.panelBodyHeight;
                    scalePreviewContent(); // scale content of all preview items
                }

                $scope.slideHeight = $scope.slideshowContentHeight - $('#slideNav').outerHeight();
                $scope.slideCenterZoom = $scope.slideHeight / slideZoomFactor;
            }

            function setFullscreenContentHeight() {
                const browserContentHeight = $(window).innerHeight();
                const marginBot = 5;
                $scope.slideshowContentHeight = browserContentHeight - marginBot;
            }
        },
    ],
);
