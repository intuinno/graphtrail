angular.module('graphtrailApp').controller('ImportTrailModalCtrl', ['$scope', '$modalInstance', '$timeout', 'entities', 'cards', 'entityDataObjectsList', function ($scope, $modalInstance, $timeout, entities, cards, entityDataObjectsList) {
    $scope.entities = entities;
    $scope.entityDataObjectsList = entityDataObjectsList;
    cards.forEach(function(d) {
        d.isAddStoryModalSelected = false;
    });

    $scope.stateObjectsDashboard = cards;

    $scope.customItemMap = GRIDSTER_CUSTOM_MAP;
    $scope.gridsterOpts = GRIDSTER_OPT_IMPORT_MODAL;

    $scope.pieOpt = angular.copy(NVD3_OPT_PIE);
    $scope.pieOpt.chart.height = 300;
    $scope.pieOpt.chart.showLegend = false;
    $scope.pieOpt.chart.growOnHover = false;

    $scope.disBarOpt = angular.copy(NVD3_OPT_DISCRETEBAR);
    $scope.disBarOpt.chart.height = 300;
    $scope.disBarOpt.chart.showLegend = false;

    $scope.radioSlideType = 'md';

    $scope.selected = {
        item: $scope.stateObjectsDashboard[0]
    };

    $scope.selectCard = (state) => {
        $scope.stateObjectsDashboard.forEach(function(d) {
            d.isAddStoryModalSelected = false;
        });
        state.isAddStoryModalSelected = true;
        windowResize(10);
    };

    $scope.getEntityLabel = (id) => {
        // search locally for the entity object
        var entityObj = Meteor.utilitiesHelper.getEntityById($scope.entities, id);

        if (entityObj) {
            return entityObj.label;
        } else {
            return "Missing Entity";
        }
    };

    $scope.getSubtitle = (state) => {
        if (!state.subtitle) {
            if (state.chartType === 'gp') {
                if (state.gatherConfig.xDim) {
                    state.subtitle = 'X: ' +  state.gatherConfig.xDim;
                }

                if (state.gatherConfig.yDim) {
                    state.subtitle = state.subtitle + " Y: " + state.gatherConfig.yDim;
                }
            } else {
                if (state.chartConfig.selection.dimName) {
                    state.subtitle = state.chartConfig.selection.dimName;
                }
            }
        }

        return Meteor.utilitiesHelper.truncSubtitle(state.subtitle, 12);
    };

    $scope.ok = () => {
        if ($scope.radioSlideType === 'trail') {
            var selectedStates = $scope.stateObjectsDashboard.filter(function (d) {
                return d.isAddStoryModalSelected;
            });

            if (selectedStates.length === 0) {
                $scope.showNoSelectError = true;
            } else {
                $modalInstance.close({
                    slide: selectedStates[0],
                    type: 'trail'
                });
            }
        } else if ($scope.radioSlideType === 'md') {
            $modalInstance.close({
                slide: $scope.my_markdown,
                type: 'md'
            });
        }
    };

    $scope.cancel = () => {
        $modalInstance.dismiss('cancel');
    };

    function windowResize(timeout) {
        $timeout(() => {
            window.dispatchEvent(new Event('resize'));
        }, timeout);
    }

    $scope.$watch('radioSlideType', function(newVal, oldVal) {
        windowResize(200); // wait longer until nvd3 charts are fully loaded
    });
}]);/**
 * Created by deokgunpark on 8/20/15.
 */
