/* Controller for adding markdown card in Trail View */
angular.module('graphtrailApp').controller('AddMarkdownModalCtrl', ['$scope', '$modalInstance', function ($scope, $modalInstance) {
    /* When OK button is fired, the markdown content is saved into the $scope object */
    $scope.ok = function () {
        $modalInstance.close({
            slide: $scope.my_markdown,
            type: 'md'
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);
/**
 * Created by minsheng zheng on 02/19/16.
 */
