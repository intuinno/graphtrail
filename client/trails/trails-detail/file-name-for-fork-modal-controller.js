angular.module('graphtrailApp').controller('fileNameForForkModalCtrl', ['$scope', '$modalInstance', 'existingExploration', function ($scope, $modalInstance, existingExploration) {

    console.log(existingExploration);

    $scope.newFileName = "Copy of " + existingExploration.name;

    $scope.copyAll = true;
    
    $scope.ok = function () {

        $modalInstance.close({filename: $scope.newFileName, 
        	copyAll: $scope.copyAll
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

/**
 * Created by deokgunpark on 8/20/15.
 */
