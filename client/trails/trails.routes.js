'use strict';

angular.module('graphtrailApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('trails-list', {
                url: '/trails',
                templateUrl: 'client/trails/trails-list/trails-list.html',
                controller: 'TrailsListCtrl',
                resolve: {
                    currentUser: ['$meteor', function($meteor) {
                        return $meteor.waitForUser();
                    }]
                }
            })
            .state('trail-detail', {
                url: '/trails/:trailId',
                templateUrl: 'client/trails/trails-detail/trail-detail.html',
                controller: 'TrailDetailCtrl',
                resolve: {
                    currentUser: ['$meteor', function($meteor) {
                        return $meteor.waitForUser();
                    }]
                }
            });
    })
    .config(['$angularMeteorSettings', function($angularMeteorSettings) {
        $angularMeteorSettings.suppressWarnings = true; // Disables write of warnings to console
    }]);
