'use strict';

angular.module('graphtrailApp')
    .controller('TrailsListCtrl', ['$scope', '$rootScope', ($scope, $root) => {
        $scope.page = 1;
        $scope.perPage = 10;
        $scope.sort = {name : 1};
        $scope.orderProperty = '1';
        $scope.searchText = '';
        $scope.newTrail = {
            name: ""
        };

        $scope.helpers({
            trails: () => {
                return Trails.find({}, {
                    // sort by trail list item name
                    sort: {
                        name: $scope.getReactively('sort')
                    }
                });
            },
            trailsCount: () => {
                return Counts.findOne('numberOfTrails');
            },
            entities: () => {
                return Data.find({});
            },
            dashboards: () => {
                return Dashboards.find({});
            },
            slideshows: () => {
                return Slideshows.find({});
            }
        });

        var trailsSubHandler = $scope.subscribe('trails', () => [
            // give reactivity to search field, sort dropdown menu, page number
            {
                // how many items per page
                limit: parseInt($scope.perPage),
                // the number of items to start with
                skip: parseInt(($scope.getReactively('page') - 1) * $scope.perPage),
                // the sorting of the collection
                sort: $scope.getReactively('sort')
            },
            $scope.getReactively('searchText')
        ], {
            onReady: () => {
                var dataSubHandler = $scope.subscribe('data', () => [], {
                    onReady: () => {
                        var dashboardsSubHandle = $scope.subscribe('dashboards', () => [], {
                            onReady: () => {
                                var slideshowsSubHandle = $scope.subscribe('slideshows', () => [], {
                                    onReady: () => {
                                        verifyTrailDependencies();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

        $scope.save = () => {
            if (!$scope.currentUser) {
                Bert.alert( 'Unable to add trail. Please log in first!', 'danger', 'growl-top-right', 'fa-frown-o' );
            } else {
                if ($scope.form.$valid) {
                    if ($scope.newTrail.name) {
                        $scope.newTrail.ownerId = $root.currentUser._id;
                        $scope.newTrail.loadedEntities = [];
                        $scope.newTrail.public = false; // default visibility is private
                        $scope.newTrail.removable = true;
                        Trails.insert($scope.newTrail);
                        $scope.newTrail = {
                            name: ""
                        };
                    } else {
                        Bert.alert( 'Name empty! Please enter something.', 'danger', 'growl-top-right', 'fa-frown-o' );
                    }
                }
            }
        };

        /*
        Remove the trail and its dependency in the entities.
        At the moment, if the trail has used but does not currently use the data from an entity, it would still count
        as being dependent on that entity.
         */
        $scope.remove = (trail) => {
            if (isOwner(trail)) {
                let dependentEntities = trail.loadedEntities;

                if (dependentEntities.length > 0) {
                    dependentEntities.forEach((eid) => {
                        // remove dependency
                        let entity = $scope.entities.find((e) => {
                            return e._id === eid;
                        });

                        if (!entity) {
                            console.log( "entity not fonud!" );
                            return;
                        }

                        let usedByTrails = entity.usedByTrails;

                        let trailIndex = usedByTrails.findIndex((tid) => {
                            return trail._id === tid;
                        });

                        let args = {};
                        args.entity = entity;
                        args.trailIndex = trailIndex;

                        if (trailIndex !== -1) {
                            Meteor.call( 'removeTrailDependency', args, ( error ) => {
                                if ( error ) {
                                    Bert.alert( 'Failed to delete trail dependency!', 'danger', 'growl-top-right', 'fa-frown-o' );
                                    console.log( error.reason );
                                } else {
                                    Trails.remove({_id: trail._id});
                                }
                            });
                        }
                    });
                }
            } else {
                Bert.alert( 'Unable to remove this trail. You are not the owner!', 'danger', 'growl-top-right', 'fa-frown-o' );
            }
        };

        $scope.pageChanged = (newPage) => {
            $scope.page = newPage;
        };

        // when orderProperty changes, sort is updated
        $scope.$watch('orderProperty', function() {
            if ($scope.orderProperty) {
                $scope.sort = {
                    name: parseInt($scope.orderProperty)
                };
            }
        });

        /*
            For each trail, check if there are dashboards or slideshows.
            If a trail instance does not have dependencies, it is labled as
            removable and remove button is shown.
        */
        function verifyTrailDependencies() {
            if ($scope.trails.length) {
                $scope.trails.forEach((trail,index) => {
                    let dashboards = $scope.dashboards.find((d) => {
                        return d.trailId === trail._id;
                    });

                    let slideshows = $scope.slideshows.find((d) => {
                        return d.trailId === trail._id;
                    });

                    let removable = false;

                    if ( !dashboards && !slideshows ) {
                        removable = true;
                    }

                    // the trail is only removable for its owner
                    if (isOwner(trail)) {
                        Trails.update({ _id: trail._id }, {
                            $set: {
                                removable: removable
                            }
                        });
                    } else {
                        $scope.trails[index].removable = false;
                    }
                });
            }
        }

        /*
         * Check if the trail belongs to the current user
         */
        function isOwner(trail) {
            if (!$scope.currentUser) {
                return false;
            }

            if (trail.ownerId === $scope.currentUser._id) {
                return true;
            } else {
                return false;
            }
        }
    }]);
