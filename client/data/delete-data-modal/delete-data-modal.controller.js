'use strict';

var app = angular.module('graphtrailApp');

app.controller('DeleteDataModalCtrl', ['$scope', '$modalInstance', 'entity', function ($scope, $modalInstance, entity) {
    $scope.trailsCount = 0;
    $scope.trailsInUse = [];
    $scope.relationships = [];

    $scope.helpers({
        entityToDelete: () => {
            return Data.findOne({_id: entity._id});
        }
    });

    $scope.subscribe('trails');

    var dataSubHandler = $scope.subscribe('data', () => [], {
        onReady: () => {
            loadRelationships();
            loadLinkedTrails();
        },
        onStop: (error) => {
            if (error) {
                console.log('An error happened - ', error);
            } else {
                console.log('The data subscription stopped');
            }
        }
    });

    function loadLinkedTrails() {
        if ($scope.entityToDelete && ($scope.entityToDelete.usedByTrails.length > 0)) {
            $scope.entityToDelete.usedByTrails.forEach(function(trailId){
                var trail = Trails.findOne({
                    _id: trailId
                }, {
                    sort: { name: 1 }
                    ,
                    fields: { name: 1 }
                });

                if (trail) {
                    $scope.trailsInUse.push(trail);
                }
            });
        }
    }

    // create a list of relationship descriptions
    function loadRelationships() {
        var selectedEntity = $scope.entityToDelete; 
        if (selectedEntity) {
            // get the links to other entities
            if (selectedEntity.links.length > 0) {
                selectedEntity.links.forEach(function(link){
                    // the target entity name may be changed, so get the most updated one
                    var toEntity = Data.findOne({_id: link.linksToEntityId}, {fields: {label: 1}});

                    if (toEntity) {
                        var relationText = selectedEntity.label + " " + link.linkName + " " + toEntity.label;

                        $scope.relationships.push(relationText);
                    }
                });
            }

            // get the links from other entities
            if (selectedEntity.linkedBy.length > 0) {
                selectedEntity.linkedBy.forEach(function(id){
                    var fromEntity = Data.findOne({_id: id}, {fields: {label: 1, links: 1}});

                    if (fromEntity) {
                        // get the link object of the entity that has a relationship with the current entity
                        var fromEntityLink = fromEntity.links.find((link) => {
                            return link.linksToEntityId === selectedEntity._id;
                        });

                        var relationText = fromEntity.label + " " + fromEntityLink.linkName + " " + selectedEntity.label;

                        $scope.relationships.push(relationText);
                    }
                });
            }
        }
    }

    $scope.ok = function () {
        $modalInstance.close({
            confirmDeleted: true
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);