'use strict';

var app = angular.module('graphtrailApp');

app.controller('DataCtrl', ['$scope', '$state', '$modal', function ($scope, $state, $modal) {
    $scope.fileInProcess = {};

    $scope.$watch('files', function () {
        $scope.upload($scope.files);
    });

    $scope.$watch('file', function () {
        if ($scope.file != null) {
            $scope.files = [$scope.file];
        }
    });

    $scope.helpers({
        entities: () => {
            return Data.find({});
        },
        entityCount: () => {
            return Counts.findOne('numberOfEntities');
        }
    });

    $scope.subscribe('data');

    $scope.upload = (files) => {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.$error) {
                    var config = buildConfig();
                    $scope.fileInProcess = file;
                    Papa.parse(file, config);
                }
            }
        }
    };

    $scope.deleteDataModal = (entityToDelete) => {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'client/data/delete-data-modal/delete-data-modal.html',
            controller: 'DeleteDataModalCtrl',
            size: 'lg',
            resolve: {
                entity: () => {
                    return entityToDelete;
                }
            }
        });

        modalInstance.result.then((result) => {
            if (result.confirmDeleted) {
                // remove all the links from the entity to be removed
                if (entityToDelete.links.length > 0) {
                    entityToDelete.links.forEach((l,i) => {
                        Meteor.relationshipsHelper.removeRelationship($scope.entities, entityToDelete, l, i);
                    });
                }

                // remove all the links to the entity to be removed
                if (entityToDelete.linkedBy.length > 0) {
                    entityToDelete.linkedBy.forEach((fromEntityId) => {
                        let fromEntity = Meteor.utilitiesHelper.getEntityById($scope.entities, fromEntityId);

                        if (fromEntity) {
                            if (fromEntity.links.length > 0) {
                                fromEntity.links.forEach((l,i) => {
                                    if (l.linksToEntityId === entityToDelete._id) {
                                        Meteor.relationshipsHelper.removeRelationship($scope.entities, fromEntity, l, i);
                                    }
                                });
                            }
                        }
                    });
                }

                Meteor.call( 'removeEntity', entityToDelete, ( error ) => {
                    if ( error ) {
                        Bert.alert( 'Failed to delete entity!', 'danger', 'growl-top-right', 'fa-frown-o' );
                        console.log( error.reason );
                    } else {
                        Bert.alert( 'Entity successfully deleted!', 'success', 'growl-top-right', 'fa-check' );
                    }
                });
            }
        });
    };

    $scope.goToRelationships = () => {
        $state.go('entity-relationships');
    };

    function buildConfig() {
        return {
            delimiter: "",  // auto-detect
            newline: "",  // auto-detect
            header: true,
            dynamicTyping: false,
            preview: 0,
            encoding: "",
            worker: false,
            comments: false,
            step: undefined,
            complete: completeFn,
            error: undefined,
            download: false,
            skipEmptyLines: false,
            chunk: undefined,
            fastMode: undefined,
            beforeFirstChunk: undefined,
            withCredentials: undefined
        };
    }

    function completeFn(results) {
        var arg = {};
        arg.fileName = $scope.fileInProcess.name.substr(0, $scope.fileInProcess.name.lastIndexOf('.'));
        arg.data = results.data;

        Meteor.call( 'modelData', arg, ( error ) => {
            if ( error ) {
                Bert.alert( 'Failed to upload!', 'danger', 'growl-top-right', 'fa-frown-o' );
                console.log( error.reason );
            } else {
                Bert.alert( 'File successfully uploaded!', 'success', 'growl-top-right', 'fa-check' );
            }
        });
    }
}]);