'use strict';

angular.module('graphtrailApp')
    .controller('EntityDetailCtrl', ['$scope', '$stateParams', function ($scope, $stateParams) {
        $scope.helpers({
            entity: () => {
                return Data.findOne({_id: $stateParams.entityId});
            },
            entities: () => {
                return Data.find({});
            }
        });

        $scope.subscribe('data');

        $scope.setAttributeDescription = (item) => {
            var index = $scope.entity.dimensions.indexOf(item);

            Data.update({_id: $stateParams.entityId}, {
                $set : {
                    [`dimensions.${index}.description`]: item.description
                }
            });
        };

        $scope.setEntityLabel = (entityLabel) => {
            Data.update({_id: $stateParams.entityId}, {
                $set : {
                    [`label`]: entityLabel
                }
            });
        };

        $scope.entityHasRelationship = () => {
            if (!$scope.entity) {
                return false;
            } else {
                if ($scope.entity.links.length > 0) {
                    return true;
                }
            }
            return false;
        };

        $scope.removeRelationship = (entity, link, index) => {
            Meteor.relationshipsHelper.removeRelationship($scope.entities, entity, link, index);
        };

        $scope.createLinkText = (fromEntityName, link) => {
            if ($scope.entities && link) {
                var toEntity = $scope.entities.find((e) => {
                    return e._id === link.linksToEntityId;
                });

                if (toEntity) {
                    return fromEntityName + " " + link.linkName + " " + toEntity.label;
                }
            }
        };
    }]);