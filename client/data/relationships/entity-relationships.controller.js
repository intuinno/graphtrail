'use strict';

var app = angular.module('graphtrailApp');

app.controller('RelationshipsCtrl', ['$scope', '$modal', function ($scope, $modal) {
    $scope.helpers({
        entities: () => {
            return Data.find({});
        }
    });

    $scope.subscribe('data');

    $scope.removeRelationship = (entity, link, index) => {
        if (link.selfJoinKey) {
            if (entity) {
                let selfLinks = {};

                entity.links.forEach((l, i) => {
                    if (l.selfJoinKey === link.selfJoinKey) {
                        selfLinks[i] = l;
                    }
                });

                if (selfLinks) {
                    Object.keys(selfLinks).forEach((key) => {
                        Meteor.relationshipsHelper.removeRelationship($scope.entities, entity, selfLinks[key], key);
                    });
                }
            }
        } else {
            Meteor.relationshipsHelper.removeRelationship($scope.entities, entity, link, index);
        }
    };

    $scope.addRelationshipModal = () => {
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'client/data/relationships/add-relationship-modal.html',
            controller: 'AddRelationshipModalCtrl',
            size: 'lg'
        });

        modalInstance.result.then(function(result) {
            if ((!result.key1 || !result.key2) || (!result.fromEntity || !result.toEntity)) {
                Bert.alert( 'Unable to define relationship! Keys or entities not identified', 'danger', 'growl-top-right', 'fa-frown-o' );
            } else {
                var args = {};
                args.fromEntity = result.fromEntity;
                args.toEntity = result.toEntity;
                args.fromKey = result.key1;
                args.toKey = result.key2;
                args.isSelfJoin = false;
                
                if ((result.fromEntity !== result.toEntity)) { // define relationship between different entities
                    args.directed = parseInt(result.directed);

                    if (args.directed) {
                        Meteor.call( 'addRelationship', args, ( error ) => {
                            if ( error ) {
                                Bert.alert( 'Failed to add a relationship!', 'danger', 'growl-top-right', 'fa-frown-o' );
                                console.log( error.reason );
                            } else {
                                Bert.alert( 'Relationship added!', 'success', 'growl-top-right', 'fa-check' );
                            }
                        });
                    } else { // undirected
                        Meteor.call( 'addRelationship', args, ( error ) => {
                            if ( error ) {
                                Bert.alert( 'Failed to add a relationship!', 'danger', 'growl-top-right', 'fa-frown-o' );
                                console.log( error.reason );
                            } else {
                                // swapping source and target entity for the other relationship
                                args.fromEntity = result.toEntity;
                                args.toEntity = result.fromEntity;

                                Meteor.call( 'addRelationship', args, ( error ) => {
                                    if ( error ) {
                                        Bert.alert( 'Failed to add a relationship!', 'danger', 'growl-top-right', 'fa-frown-o' );
                                        console.log( error.reason );
                                    } else {
                                        Bert.alert( 'Relationship added!', 'success', 'growl-top-right', 'fa-check' );
                                    }
                                });
                            }
                        });
                    }
                } else { // self join
                    args.isSelfJoin = true;
                    args.selfJoinKey = args.fromKey+"-"+args.toKey; // this has to be identical for both directed relationship

                    if (result.key1 === result.key2) {
                        Bert.alert( 'Unable to define relationship! Keys have to be different for self join', 'danger', 'growl-top-right', 'fa-frown-o' );
                    } else {
                        // add two directed relationships for self-join
                        Meteor.call( 'addRelationship', args, ( error ) => {
                            if ( error ) {
                                Bert.alert( 'Failed to add a relationship!', 'danger', 'growl-top-right', 'fa-frown-o' );
                                console.log( error.reason );
                            } else {
                                args.fromKey = result.key2;
                                args.toKey = result.key1;

                                Meteor.call( 'addRelationship', args, ( error ) => {
                                    if ( error ) {
                                        Bert.alert( 'Failed to add a relationship!', 'danger', 'growl-top-right', 'fa-frown-o' );
                                        console.log( error.reason );
                                    } else {
                                        Bert.alert( 'Relationship added!', 'success', 'growl-top-right', 'fa-check' );
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }, function() {
            // console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.createLinkText = (fromEntityName, link) => {
        if ($scope.entities && link) {
            var toEntity = $scope.entities.find((e) => {
                return e._id === link.linksToEntityId;
            });

            if (toEntity) {
                return fromEntityName + " " + link.linkName + " " + toEntity.label;
            }
        }
    };
}]);