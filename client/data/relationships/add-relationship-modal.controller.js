/* Controller for adding relationship */
angular.module('graphtrailApp')
    .controller('AddRelationshipModalCtrl', ['$scope', '$modalInstance', function ($scope, $modalInstance) {
        $scope.helpers({
            entities: () => {
                return Data.find({});
            }
        });

        $scope.subscribe('data');

        $scope.save = () => {
            $modalInstance.close({
                fromEntity: $scope.fromEntity,
                key1: $scope.attribute1.name,
                toEntity: $scope.toEntity,
                key2: $scope.attribute2.name,
                directed: $scope.directed
            });
        };

        $scope.cancel = () => {
            $modalInstance.dismiss('cancel');
        };
}]);