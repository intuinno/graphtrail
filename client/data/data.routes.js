'use strict';

angular.module('graphtrailApp')
	.config(function($stateProvider) {
		$stateProvider
		.state('data', {
			url: '/data',
			templateUrl: 'client/data/data.view.html',
			controller: 'DataCtrl'
		})
		.state('entity-detail', {
			url: '/data/:entityId',
			templateUrl: 'client/data/entity-detail/entity-detail.view.html',
			controller: 'EntityDetailCtrl'
		})
		.state('entity-relationships', {
			url: '/relationships',
			templateUrl: 'client/data/relationships/entity-relationships.view.html',
			controller: 'RelationshipsCtrl'
		});
	});