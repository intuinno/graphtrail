'use strict';

angular.module('graphtrailApp')
    .controller('SlideshowDetailCtrl', ['$scope', '$stateParams', '$timeout', 'hotkeys',
    ($scope, $stateParams, $timeout, hotkeys) => {
        const slideZoomFactor = 602;
        const panelBodyPad = 15;

        $scope.fullscreenObjects = [];
        $scope.entityDataObjectsList = [];
        $scope.isAllLoaded = false;
        $scope.fsflag = true;
        $scope.slideIndex = 0; // slide index
        $scope.isFullScreen = false;
        $scope.test = false;
        $scope.whatisgoingon = false;
        $scope.zoomlevel = 50;

        $scope.panelHeight = 300;
        $scope.panelBodyHeight = 300;
        $scope.slideshowContentHeight = 300;
        $scope.slideHeight = 300;
        $scope.slideCenterZoom = 1;

        $scope.pieOptSlideFs = angular.copy(NVD3_OPT_PIE);
        $scope.pieOptSlideFs.chart.height = 470;

        $scope.disBarOptSlideFs = angular.copy(NVD3_OPT_DISCRETEBAR);
        $scope.disBarOptSlideFs.chart.height = 470;

        $scope.helpers({
            entities: () => {
                return Data.find({});
            }
        });

        var trailData = {};
        var trailDataChecker = {};

        var slideshowsSubHandle = $scope.subscribe('slideshows', () => [], {
            onReady: () => {
                $scope.slideshow = Slideshows.findOne({_id: $stateParams.slideshowId});

                if ($scope.slideshow.dataSource === 'trails') {
                    var trailsSubHandle = $scope.subscribe('trails', ()=> [], {
                        onReady: () => {
                            $scope.trail = Trails.findOne({_id: $scope.slideshow.trailId});
                            $scope.originalExplorationLink = '/trails/' + $scope.slideshow.trailId;

                            var dataSubHandler = $scope.subscribe('data', () => [], {
                                onReady: () => {
                                    loadEntityDataInUse();
                                },
                                onStop: (error) => {
                                    if (error) {
                                        console.log('An error happened - ', error);
                                    }
                                }
                            });
                        },
                        onStop: (error) => {
                            if (error) {
                              console.log('OnStop error: ' + error);
                            }
                        }
                    });
                }
            },
            onStop: (error) => {
                if (error) {
                    console.log('OnStop error: ' + error);
                }
            }
        });

        /* Update nvd3 chart dimensions without updating the database */
        $scope.fullscreenUpdateChartDim = (state) => {
            // find the selected attribute
            let piePlotData = state.chartData;
            let selectedDim = state.chartConfig.selection.dimName;

            if (piePlotData && selectedDim) {
                if (piePlotData.hasOwnProperty(selectedDim)) {
                    // update plot data to re-redner
                    $scope.entityDataObjectsList[state.id] = piePlotData[selectedDim];
                }
            }
        };

        $scope.previousCard = () => {
            if ($scope.slideIndex !== 0) {
                $scope.slideIndex = $scope.slideIndex - 1;

                if ($scope.fullscreenObjects[$scope.slideIndex].type === 'trail') {
                    $scope.fullscreenState = $scope.getStateFromId($scope.fullscreenObjects[$scope.slideIndex].id);
                } else {
                    $scope.fullscreenState = $scope.fullscreenObjects[$scope.slideIndex];
                }
            }

            windowResize(50);
        };

        $scope.nextCard = () => {
            if ($scope.slideIndex !== $scope.fullscreenObjects.length - 1) {
                $scope.slideIndex = $scope.slideIndex + 1;

                if ($scope.fullscreenObjects[$scope.slideIndex].type === 'trail') {
                    $scope.fullscreenState = $scope.getStateFromId($scope.fullscreenObjects[$scope.slideIndex].id);
                } else {
                    $scope.fullscreenState = $scope.fullscreenObjects[$scope.slideIndex];
                }
            }

            windowResize(50);
        };

        $scope.switchCard = (card, seq) => {
            $scope.slideIndex = seq;

            if (card.type === 'trail') {
                $scope.fullscreenState = $scope.getStateFromId(card.id);
            } else {
                $scope.fullscreenState = card;
            }

            windowResize(50);
        };

        $scope.findCardInTrail = (state) => {
            $scope.trail.stateObjects.forEach((d, index) => {
                if (d.id === state.id) {
                    d.selectedCard = true;
                } else {
                    d.selectedCard = false;
                }

                Trails.update({_id: $scope.slideshow.trailId}, {
                    $set: {
                        [`stateObjects.${index}.selectedCard`]: d.selectedCard
                    }
                });
            });
        };

        $scope.isCurrentSlide = (card, seq) => {
            if (seq === $scope.slideIndex) {
                return true;
            }
            return false;
        };

        $scope.setFullScreen = (f) => {
            $scope.isFullScreen = f;
            // $scope.$apply();
        };

        // $scope.$watch('isFullScreen', (newVal, oldVal) => {
        //     // console.log(newVal);
        // });

        $scope.getStateFromId = (id) => {
            let result = $scope.trail.stateObjects.find((d) => {
                return d.id === id;
            });

            if (!result) {
                console.log("[getStateFromId] Cannot find state with id=", id);
            }

            return result;
        };

        $scope.getEntityLabel = (id) => {
            // search locally for the entity object
            let entityObj = Meteor.utilitiesHelper.getEntityById($scope.entities, id);

            if (entityObj) {
                return entityObj.label;
            } else {
                return "Missing Entity";
            }
        };

        $scope.getEntityDimensions = (id) => {
            // search locally for the entity object
            let entityObj = Meteor.utilitiesHelper.getEntityById($scope.entities, id);

            if (entityObj) {
                return entityObj.dimensions;
            }
        };

        hotkeys.bindTo($scope)
            .add({
                combo: 'left',
                description: 'Previous Slide',
                callback: function() {
                    $scope.previousCard();
                }


            }).add({
                combo: 'right',
                description: 'Next Slide',
                callback: function() {
                    $scope.nextCard();
                }
            });

        $scope.handleKeyboard = (e) => {
            if (e.keyCode === 39) {
                $scope.nextCard();
            } else if (e.keyCode === 37) {
                $scope.previousCard();
            }
        };

        // watch for window resizing
        $(window).resize(() => {
            $scope.$apply(() => {
                let slideshowMd = $scope.fullscreenObjects[$scope.slideIndex];

                if (slideshowMd && (slideshowMd.type === 'trail')) {
                    trailScale();
                } else {
                    markdownScale();
                }
            });
        });

        function loadEntityDataInUse() {
            if ($scope.trail) {
                if ($scope.trail.loadedEntities.length > 0) {
                    $scope.trail.loadedEntities.forEach((entityId) => {
                        if (Meteor.utilitiesHelper.getEntityById($scope.entities, entityId)) {
                            trailDataChecker[entityId] = false;
                            // console.log('Retrieving entity: ' + entityId);

                            Meteor.call('getDataFromEntityId', entityId, (error, result ) => {
                                if (error) {
                                    console.log( error );
                                } else {
                                    // copy data into global array
                                    trailData[entityId] = result;
                                    trailDataChecker[entityId] = true;
                                    checkAllLoading();
                                }
                            });
                        } else { // entity has been removed
                            doThingsAfterLoading();
                        }
                    });
                } else {
                    doThingsAfterLoading();
                }
            }
        }

        function checkAllLoading() {
            if (Meteor.utilitiesHelper.checkAllProp(trailDataChecker)) {
                doThingsAfterLoading();
            }
        }

        function loadFullscreenObjects() {
            let card;

            if ($scope.slideshow.slideshowContent) {
                if ($scope.slideshow.slideshowContent.length > 0) {
                    $scope.slideshow.slideshowContent.forEach((slide, index) => {
                        if (slide.type === 'trail') {
                            if (slide.entityId) {
                                // if entity no more exists, reset slide
                                if (!Meteor.utilitiesHelper.getEntityById($scope.entities, slide.entityId)) {
                                    let stateMissingData = {};

                                    stateMissingData.type = slide.type;
                                    stateMissingData.id = slide.id;
                                    stateMissingData.subtitle = "";

                                    Slideshows.update({_id: $scope.slideshow._id}, {
                                        $set : {
                                            [`slideshowContent.${index}`]: stateMissingData
                                        }
                                    });

                                    slide = stateMissingData;
                                }
                            }
                        }
                    });

                    let firstSlide = $scope.slideshow.slideshowContent[0];

                    if (firstSlide.type === 'trail') {
                        card = $scope.trail.stateObjects.find((d) => {
                            return d.id === firstSlide.id;
                        });
                    } else if (firstSlide.type === 'md') {
                        card = firstSlide;
                    }
                }
            }

            $scope.fullscreenState = angular.copy(card);
            $scope.fullscreenObjects = [];
            $scope.fullscreenObjects = $scope.slideshow.slideshowContent;
        }

        function doThingsAfterLoading() {
            $scope.isAllLoaded = true;

            if ($scope.trail.stateObjects) {
                loadEntityDataObjects();
                loadFullscreenObjects();
                windowResize(100); // so first slide is rendered properly
            }
        }

        function loadEntityDataObjects() {
            if ($scope.trail) {
                if ($scope.trail.stateObjects.length > 0) {
                    $scope.trail.stateObjects.forEach((state) => {
                        if (state.entityId) {
                            // if entity is removed, reset properties of the slide and show missing data message
                            if (Meteor.utilitiesHelper.getEntityById($scope.entities, state.entityId)) {
                                if (state.chartType === 'gp') {
                                    if (state.entityData) {
                                        let selectedDocuments = Meteor.utilitiesHelper.fromIdListToObjectList(state.entityData, trailData[state.entityId]);
                                        updateDataUsedByGatherplot(state, selectedDocuments);
                                        selectedDocuments = null; // free space
                                    }
                                } else { // pie or bar
                                    if (state.chartData) {
                                        let selectedDim = state.chartConfig.selection.dimName;

                                        if (selectedDim) {
                                            updateDataUsedByChart(state.id, state.chartData[selectedDim]);
                                        } else {
                                            updateDataUsedByChart(state.id, state.chartData[Object.keys(state.chartData)[0]]);
                                        }
                                    }
                                }
                            } else {
                                try {
                                    // find the index of the slide to be updated
                                    let index = $scope.slideshow.slideshowContent.map((slide) => {
                                        return slide.id;
                                    }).indexOf(state.id);

                                    Slideshows.update({_id: $stateParams.slideshowId}, {
                                        $unset : {
                                            [`slideshowContent.${index}.entityId`]: 1
                                        }
                                    });

                                    Slideshows.update({_id: $stateParams.slideshowId}, {
                                        $pull : {
                                            [`slideshowContent.${index}`]: null
                                        }
                                    });
                                } catch (e) {
                                    console.log("Slideshows update error, " + e);
                                }
                            }
                        }
                    });
                }
            }
        }

        /*
         * Update the chart data array with gatherplot data
         * */
        function updateDataUsedByGatherplot(state, data) {
            data = angular.copy(data);
            data.map((d, i) => { d.id = i; });
            $scope.entityDataObjectsList[state.id] = data;
        }

        /*
         * Update the chart data array for other chart types (pie, bar, and etc.)
         * */
        function updateDataUsedByChart(id, data) {
            $scope.entityDataObjectsList[id] = data;
        }

        /* Dispatch a resize event in order to re-render chart */
        function windowResize(time) {
            $timeout(() => {
                window.dispatchEvent(new Event('resize'));
            }, time);
        }

        /* Get panel available height */
        function setPanelAvailHeight() {
            let $navMain = $('.navbar');
            let browserContentHeight = $(window).innerHeight();
            let navMainHeight = $navMain.outerHeight(true);
            let marginBot = 5;
            $scope.panelHeight = browserContentHeight - navMainHeight - marginBot;
        }

        /* Get panel body available height */
        function setPanelBodyAvailHeight() {
            let $heading = $('.panel-heading');
            let headingHeight = $heading.outerHeight(true);
            $scope.panelBodyHeight = $scope.panelHeight - headingHeight;
        }

        function setSlideshowContentHeight() {
            $scope.slideshowContentHeight = $scope.panelBodyHeight - panelBodyPad*2;
        }

        function setSlideHeight() {
            $scope.slideHeight = $scope.slideshowContentHeight - $('#slideNav').outerHeight();
        }

        function setSlideZoomFactor() {
            $scope.slideCenterZoom = $scope.slideHeight / slideZoomFactor;
        }

        function scaleSlideMdContent(slideshowMd) {
            if ($scope.slideIndex !== -1) {
                let scaleFactor = 0;

                let $slideContainer = $('#slideCenter');
                let $mdObj = $('#slideshow-md-content');

                let contentHeight = $mdObj.outerHeight();
                let contentWidth = $mdObj.outerWidth();

                let containerHeight = $slideContainer.outerHeight();
                let containerWidth = $slideContainer.outerWidth();

                scaleFactor = Math.min(
                    containerHeight / contentHeight,
                    containerWidth / contentWidth
                );

                if (scaleFactor !== 0) {
                    slideshowMd.scaleFactor = scaleFactor;

                    Slideshows.update({_id: $scope.slideshow._id}, {
                        $set : {
                            [`slideshowContent.${$scope.slideIndex}`]: angular.copy(slideshowMd)
                        }
                    });
                }
            }
        }

        function setFullscreenContentHeight() {
            let browserContentHeight = $(window).innerHeight();
            let marginBot = 5;
            $scope.slideshowContentHeight = browserContentHeight - marginBot;
        }

        function trailScale() {
            if ($scope.isFullScreen) {
                setFullscreenContentHeight();
            } else {
                setPanelAvailHeight();
                setPanelBodyAvailHeight();
                setSlideshowContentHeight();
            }

            setSlideHeight();
            setSlideZoomFactor();
        }

        function markdownScale() {
            $timeout(() => {
                trailScale();
            }, 10);

            $timeout(() => {
                let slideshowMd = $scope.fullscreenObjects[$scope.slideIndex];
                if (slideshowMd) scaleSlideMdContent(slideshowMd); // scale selected slide content
            }, 50);
        }
    }]);
