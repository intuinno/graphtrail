'use strict';

angular.module('graphtrailApp')
    .controller('SlideshowsListCtrl', function($scope) {
        $scope.page = 1;
        $scope.perPage = 10;
        $scope.sort = {name: 1};
        $scope.orderProperty = '1';

        $scope.helpers({
            slideshows: () => {
                return Slideshows.find({}, { sort: {name: $scope.getReactively('sort')}});
            },
            slideshowsCount: () => {
                return Counts.findOne('numberOfSlideshows');
            }
        });

        $scope.subscribe('slideshows', () => {
            return [
                {
                    // how many items per page
                    limit: parseInt($scope.perPage),
                    // the number of items to start with
                    skip: parseInt(($scope.getReactively('page') - 1) * $scope.perPage),
                    // the sorting of the collection
                    sort: $scope.getReactively('sort')
                },
                $scope.getReactively('searchText')
            ];
        });

        $scope.remove = function(slideshow) {
            Slideshows.remove({_id: slideshow._id});
        };

        $scope.pageChanged = function(newPage) {
            $scope.page = newPage;
        };

        $scope.$watch('orderProperty', function() {
            if($scope.orderProperty) {
                $scope.sort = {name: parseInt($scope.orderProperty)};
            }
        });
    });
        