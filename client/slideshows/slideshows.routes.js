'use strict';
    angular.module('graphtrailApp')
        .config(function($stateProvider) {
            $stateProvider
                .state('slideshows-list', {
                    url: '/slideshows',
                    templateUrl: 'client/slideshows/slideshows-list.view.html',
                    controller: 'SlideshowsListCtrl'
                })
                .state('slideshow-detail', {
                    url: '/slideshows/:slideshowId',
                    templateUrl: 'client/slideshows/slideshow-detail.view.html',
                    controller: 'SlideshowDetailCtrl'
                });
        });