'use strict';

angular.module('graphtrailApp')
.config(function($stateProvider) {
  $stateProvider
  .state('logs-list', {
    url: '/logs',
    templateUrl: 'client/logs/logs-list.view.html',
    controller: 'LogsListCtrl'
  })
  .state('log-detail', {
    url: '/logs/:logId',
    templateUrl: 'client/logs/log-detail.view.html',
    controller: 'LogDetailCtrl'
  });
});