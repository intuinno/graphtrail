'use strict';

angular.module('graphtrailApp')
.controller('LogsListCtrl', function($scope, $meteor) {
  $scope.page = 1
  $scope.perPage = 15
  $scope.sort = {createdAtUTC : -1};
  $scope.orderProperty = '-1'
  
  $scope.logs = $meteor.collection(function() {
    return Logs.find({}, {sort:$scope.getReactively('sort')});
  });
  $meteor.autorun($scope, function() {
    $meteor.subscribe('logs', {
      limit: parseInt($scope.getReactively('perPage')),
      skip: parseInt(($scope.getReactively('page') - 1) * $scope.getReactively('perPage')),
      sort: $scope.getReactively('sort')
    }, $scope.getReactively('search')).then(function() {
      $scope.logsCount = $meteor.object(Counts, 'numberOfLogs', false);
    });
  });
    
  $scope.save = function() {
    if($scope.form.$valid) {
      $scope.logs.save($scope.newLog);
      $scope.newLog = undefined;
    }
  };
      
  $scope.remove = function(log) {
    $scope.logs.remove(log);
  };
    
  $scope.pageChanged = function(newPage) {
    $scope.page = newPage;
  };
    
  $scope.$watch('orderProperty', function() {
    if($scope.orderProperty) {
      $scope.sort = {createdAtUTC: parseInt($scope.orderProperty)};
    }
  });
});
        