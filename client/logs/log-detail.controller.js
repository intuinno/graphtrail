'use strict';

angular.module('graphtrailApp')
.controller('LogDetailCtrl', function($scope, $stateParams, $meteor) {
  $scope.log = $meteor.object(Logs, $stateParams.logId);
  $meteor.subscribe('logs');
  $scope.myJson = $scope.log.getRawObject();
  $scope.save = function() {
    if($scope.form.$valid) {
      $scope.log.save().then(
        function(numberOfDocs) {
          console.log('save successful, docs affected ', numberOfDocs);
        },
        function(error) {
          console.log('save error ', error);
        }
      )
    }
  };
        
  $scope.reset = function() {
    $scope.log.reset();
  };
});