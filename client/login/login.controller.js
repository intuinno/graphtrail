'use strict'

angular.module('graphtrailApp')
    .controller('LoginCtrl', ['$scope', '$meteor', '$state', function($scope, $meteor, $state) {
        var vm = this;

        vm.credentials = {
            email: '',
            password: ''
        };

        vm.error = '';

        vm.login = function() {
            $meteor.loginWithPassword(vm.credentials.email, vm.credentials.password).then(
                function() {
                    $state.go('profile');
                },
                function(err) {
                    vm.error = 'Login error - ' + err; 
                });
        }
    }]);
