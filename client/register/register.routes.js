'use strict'

angular.module('graphtrailApp')
.config(function($stateProvider) {
  $stateProvider
  .state('register', {
    url: '/register',
    templateUrl: 'client/register/register.view.html',
    controller: 'RegisterCtrl',
    controllerAs: 'rc'
  });
});