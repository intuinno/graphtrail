# StoryFacets

StoryFacets is a data provenance tool for collaborative visual analytics.  It allows you to
- Explore relational database
- Create dashboard from the exploration 
- Create a slide deck to present your exploration 

You can see [demo](https://graphtrail.mybluemix.net) 
or [the latest demo](https://storyfacets-test.herokuapp.com)

For more information, please refer the following paper. 

Deokgun Park, Mohamed Suhail, Minsheng Zheng, Cody Dunne, Eric Ragan, and Niklas Elmqvist. Storyfacets: A design study on storytelling with visualizations for collaborative data analysis. Information Visualization, 2021 (Accepted)

## Development Guide
1. Clone the source code
2. Install [meteor](https://www.meteor.com/install)
3. Go to the source directory and type
````
meteor
```` 
This will download all the requirements and start a local webserver which will monitor the change and live reload.  

## Deployment to Heroku Guide
1. Download and install the [Heroku Toolbelt](https://toolbelt.heroku.com/)
2. Log in to your heroku account through your console
````
$ heroku login
````
and enter your credentials,
3. Clone the repository,
Either use Git to clone storyfacets-test's source code to your local machine.
````
$ heroku git:clone -a storyfacets-test
$ cd storyfacets-test
````
Or if you have alreay have the source code on your local machine,
````
heroku git:remote -a storyfacets-test
````
4. Set a Meteor buildpack for your Heroku instance
````
heroku buildpacks:set https://github.com/jordansissel/heroku-buildpack-meteor.git
````
5. Set up the database (only for the first time)
````
heroku addons:create mongolab:sandbox
heroku config:add MONGO_URL=mongodb://<username>:<password>@ds061721.mlab.com:61721/storyfacets-test
heroku config:add ROOT_URL=https://storyfacets-test.herokuapp.com
````
6. Deploy your changes
````
git push heroku master
````

## Deployment to bluemix Guide
1. Install [Cloud foundry command line interface](https://console.ng.bluemix.net/?direct=classic/#/resources/orgGuid=2dbd8882-09f2-4b5f-82a1-ab837a29f4d9&spaceGuid=2f16e540-7520-4af8-afd4-2d0d7b468abe&appGuid=06c7fdf1-f877-4058-8b77-28e3a850d525&detailType=gettingstarted&paneId=4)
2. Log on to the your console at Bluemix and create a new web app using node.js template
3. Create a .cfignore file and add meteor local directory to it.  This reduces the number of files pushed during deployment
4. Create a server-side mongodb instance.  When using the mongolabs sandbox plan, using cf command line interface  
````
cf create-service mongolab sandbox myMongoDBName 
````
5. Bind the newly created mongodb instance to your app. 
````
cf bind-service myAppName myMongdoDBName
````
6. Push the app to bluemix. 
````                        
cf push myAppName -b https://github.com/intuinno/bluemix-buildpack-meteor.git 
```` 
You can also use 'cf push' using the **manifest.yml** file.  Please check the included file for an example. 

Please refer [following](http://www.ibm.com/developerworks/library/wa-bluemix-meteor-demo/index.html) on more information about the deployment. 

