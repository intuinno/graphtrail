Trails = new Mongo.Collection('trails');

Trails.allow({
  insert: function(userId, trail) {
    return userId && trail.ownerId == userId;
  },
  update: function(userId, trail, fields, modifier) {
    return userId && trail.ownerId == userId;
  },
  remove: function(userId, trail) {
    return userId && trail.ownerId == userId;
  }
});