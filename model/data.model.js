Data = new Mongo.Collection('data');

Data.allow({
  insert: function(userId, data) {
    return userId && data.ownerId == userId;
  },
  update: function(userId, data, fields, modifier) {
    return userId && data.ownerId == userId;
  },
  remove: function(userId, data) {
    return userId && data.ownerId == userId;
  }
});