Dashboards = new Mongo.Collection('dashboards');

Dashboards.allow({
  insert: function(userId, dashboard) {
    return userId && dashboard.ownerId == userId;
  },
  update: function(userId, dashboard, fields, modifier) {
    return userId && dashboard.ownerId == userId;
  },
  remove: function(userId, dashboard) {
    return userId && dashboard.ownerId == userId;
  }
});