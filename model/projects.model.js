Projects = new Mongo.Collection('projects');

Projects.allow({
  insert: function(userId, project) {
    return userId && project.ownerId == userId;
  },
  update: function(userId, project, fields, modifier) {
    return userId && project.ownerId == userId;
  },
  remove: function(userId, project) {
    return userId && project.ownerId == userId;
  }
});