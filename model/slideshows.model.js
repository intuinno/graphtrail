Slideshows = new Mongo.Collection('slideshows');

Slideshows.allow({
    insert: function(userId, slideshow) {
        return userId && slideshow.ownerId == userId;
    },
    update: function(userId, slideshow, fields, modifier) {
        return userId && slideshow.ownerId == userId;
    },
    remove: function(userId, slideshow) {
        return userId && slideshow.ownerId == userId;
    }
});