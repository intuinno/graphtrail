var global = this;

global.initDB = () => {
	if (Meteor.isClient) {
		Meteor.subscribe('data', {}, (err) => {
			var clientEntities = Data.find().fetch();

			if (clientEntities.length) {
				_.map(clientEntities, (entity) => {
					if (!global[entity.entityName]) {
						global[entity.entityName] = new Mongo.Collection(entity.entityName);
					}	
				});
			}
		});
	}

	if (Meteor.isServer) {
		// loop through all client and setup the DB
		var clientEntities = Data.find().fetch();

		if (clientEntities.length) {
			_.map(clientEntities, (entity) => {
				if (!global[entity.entityName]) {
					global[entity.entityName] = new Mongo.Collection(entity.entityName);
				}
			});
		}
	}
}

Meteor.startup(function() {
	global.initDB();
});