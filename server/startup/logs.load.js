Meteor.startup(function() {
  if(Logs.find().count() === 0) {
    var logs = [
      {
        'name': 'log 1'
      },
      {
        'name': 'log 2'
      }
    ];
    logs.forEach(function(log) {
      Logs.insert(log);
    });
  }
});