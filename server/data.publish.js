'use strict';

Meteor.publish('data', function(options, searchString) {
  if(!searchString) {
    searchString = '';
  }

  let selector = {
      'entityName': {
          '$regex': '.*' + searchString || '' + '.*',
          '$options': 'i'
      },
      $or: [{
          $and: [{
              'public': true
          }, {
              'public': {
                  $exists: true
              }
          }]
      }, {
          $and: [{
              ownerId: this.userId
          }, {
              ownerId: {
                  $exists: true
              }
          }]
      }]
  };

  Counts.publish(this, 'numberOfEntities', Data.find(selector), {noReady: true});
  return Data.find(selector, options);
});
