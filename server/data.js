var global = this;

Meteor.methods({
	modelData( arg ) {
		if (!arg || !arg.data || !arg.fileName) {
			throw new Meteor.Error(500, '[modelData] arguments not valid!');
		}

        var data = arg.data;
		var entityName = 'entity_' + arg.fileName + '_' + Math.random().toString(36).substr(2, 9);

		// create new collection
		global[entityName] = new Mongo.Collection(entityName);

		var newDataRecord = {};
		newDataRecord.entityName = entityName;
		newDataRecord.ownerId = Meteor.user()._id;
		newDataRecord.label = arg.fileName.charAt(0).toUpperCase() + arg.fileName.slice(1);
		newDataRecord.links = [];
		newDataRecord.linkedBy = [];
		newDataRecord.tooltipName = "";
		newDataRecord.usedByTrails = [];

		// retrieve attributes of the imported data
		var entityDimensions = [];
		var attributes = Object.keys(data[0]);

		attributes.forEach((d) => {
			entityDimensions.push({
				"name": d,
				"description": ""
			});
		});

		newDataRecord.dimensions = entityDimensions;

		// update Data collection
		Data.insert(newDataRecord);

		// insert data into collection
		data.forEach((d) => {
			try {
				global[entityName].insert(d);
			} catch (e) {
				throw new Meteor.Error(500, '[modelData] unable to insert data!', e);
			}
		});

		Meteor.publish( entityName, () => {
			return global[entityName].find();
		});
	},
    removeEntity ( entity ) {
        // remove dependencies
		try {
			Data.remove({_id: entity._id});
		} catch (e) {
			throw new Meteor.Error(500, '[removeEntity] unable to remove entity dependency in Data', e);
		}

        // remove the entity from the database
        try {
            Mongo.Collection.get(entity.entityName).remove({});
            Mongo.Collection.get(entity.entityName)._dropCollection();
        } catch (e) {
            throw new Meteor.Error(500, '[removeEntity] unable to remove target collection', e);
        }
	},
	// remove an entire field of embedded documents of an entity
	removeReferencesFromEntity( args ) {
		var entityName = args.entity.entityName;
		var linkField = args.link.linkField;

		if (!global[entityName]) {
			global[entityName] = new Mongo.Collection(entityName);
		}

        // remove the association field of all the documents
		global[entityName].update({[`${linkField}`]: {$exists: true}}, {
			$unset : {
				[`${linkField}`]: 1
			}
		}, {multi: true});

		global[entityName].update({[`${linkField}`]: {$exists: true}}, {
			$pull : {
				[`${linkField}`]: null
			}
		}, {multi: true});
	},
	/* Remove a relationship object from the links list of an entity */
	removeLinkFromEntity( args ) {
		var entity = args.entity;
		var linkIndex = args.linkIndex;

        if (!entity || (linkIndex === -1)) {
            throw new Meteor.Error(500, '[removeLinkFromEntity] invalid arguments!');
        }

		try {
			Data.update({_id: entity._id}, {
				$unset : {
					[`links.${linkIndex}`]: 1
				}
			});

			Data.update({_id: entity._id}, {
				$pull : {
					links: null
				}
			});
		} catch (e) {
			throw new Meteor.Error(500, '[removeLinkFromEntity] unable to remove link', e);
		}
	},
    /* Remove the id of an entity from the linkedBy list of another entity */
    removeIdFromLinkedBy( args ) {
        var entity = args.entity;
        var linkedByEntityIdIndex = args.linkedByEntityIdIndex;

        if (!entity || (linkedByEntityIdIndex === -1)) {
            throw new Meteor.Error(500, '[removeIdFromLinkedBy] invalid arguments!');
        }

        try {
            Data.update({_id: entity._id}, {
                $unset : {
                    [`linkedBy.${linkedByEntityIdIndex}`]: 1
                }
            });

            Data.update({_id: entity._id}, {
                $pull : {
                    linkedBy: null
                }
            });
        } catch (e) {
            throw new Meteor.Error(500, '[removeIdFromLinkedBy] unable to remove id', e);
        }
    },
	/*
	Remove the trail id from the dependency list of an entity
	 */
	removeTrailDependency( args ) {
		let entity = args.entity;
		let trailIndex = args.trailIndex;

		if (!entity || (trailIndex === -1)) {
			throw new Meteor.Error(500, '[removeTrailDependency] invalid arguments!');
		}

		try {
			Data.update({_id: entity._id}, {
				$unset : {
					[`usedByTrails.${trailIndex}`]: 1
				}
			});

			Data.update({_id: entity._id}, {
				$pull : {
					usedByTrails: null
				}
			});
		} catch (e) {
			throw new Meteor.Error(500, '[removeTrailDependency] unable to remove trail from dependency list', e);
		}
	},
	getDataFromEntityId( entityId ) {
        var entityObj = Data.findOne({_id: entityId});

        if (!entityObj) {
            throw new Meteor.Error(500, '[getDataFromEntityId] entity not found!');
        }

        var option = {};

        // exclude reference fields in returned data
        if (entityObj.links.length > 0) {
            let fields = {};

            let fieldsToExclude = entityObj.links.map((l) => {
                return l.linkField;
            });

            if (fieldsToExclude.length > 0) {
                fieldsToExclude.forEach((refField) => {
                    fields[refField] = 0;
                });
            }

            option.fields = fields;
        }

		var data = Mongo.Collection.get(entityObj.entityName).find({}, option).fetch();

		if (data) {
			return data;
		} else {
			throw new Meteor.Error(500, 'unable to locate collection: ' + entityName);
		}
	},
    // return a list of ids of the documents of the target entity that has a relationship with the current one
    getDataFromRelationship( args ) {
		var entityId = args.fromEntityId;
		var selectedFromEntityIds = args.selectedFromEntityIds;
		var linkField = args.linkField;

		if (!entityId || !selectedFromEntityIds) {
			throw new Meteor.Error(500, '[getDataFromRelationship] arguments not valid!');
		}

        var entityObj = Data.findOne({_id: entityId});

        if (!entityObj) {
            throw new Meteor.Error(500, '[getDataFromRelationship] entity not fonud!');
        }

		// only return the embedded document field
		let option = {
			sort: {
				[`${linkField}`]: 1
			},
			fields: {
				[`${linkField}`]:1
			}
		};

		// only return selected documents that have the embedded document field
		let selector = {
			$and : [
				{
					_id: {
						$in: selectedFromEntityIds
					}
				},
				{
					[`${linkField}`]: {$exists: true}
				}
			]
		};

		var selectedToEntityDocs = Mongo.Collection.get(entityObj.entityName).find(selector, option).fetch();
		var result = [];
		var unique = {};

		// only append the distinctive documents to the result array
		if (selectedToEntityDocs) {
			selectedToEntityDocs.forEach((doc) => {
				if ( typeof(unique[doc._id] === "undefined") ) {
					result = result.concat(doc[linkField]);
				}
				unique[doc._id] = 0;
			});
		}

		return result.map((doc) => { return doc._id; });
    },
	/* Perform all the actions needed for establishing a relationship between two entities,
	 * either identical or distinctive;
	 * 1. add a link object in Data
	 * 2. do a right join and embed result(s) into the source entity */
	addRelationship( args ) {
		if (!args || !args.fromEntity || !args.toEntity || !args.fromKey || !args.toKey) {
			throw new Meteor.Error(500, 'arguments invalid ');
		}

		var fromEntity = args.fromEntity;
		var toEntity = args.toEntity;
		var directed = args.directed;
        var fromKey = args.fromKey;
        var toKey = args.toKey;
		var linkField = fromEntity.entityName+"_"+fromKey+"_"+toKey; // the field name of embedding referenced objects
        var selfJoinKey = args.selfJoinKey;
		var groupKey = toKey; // for group documents in the entity for faster search later
		var searchKey = fromKey;

		// if an entity is joining with itself, we need two distinctive keys/fields
		if (args.isSelfJoin) {
		    if (selfJoinKey) {
                try {
                    Data.update({_id: fromEntity._id}, {
                        $push: {
                            "links": {
                                "linkName": "has", // dynamic value, to be changed by user
                                "linkField": linkField, // fixed value
                                "linksToEntityId": toEntity._id, // fixed value
                                "linksToEntityName": toEntity.entityName, // fixed value
                                "selfJoinKey": selfJoinKey,
                                "fromKey": fromKey, // fixed value
                                "toKey": toKey, // fixed value
                                "directed": 1 // fixed value, self join is always one-way
                            }
                        }
                    });
                } catch (e) {
                    throw new Meteor.Error(500, 'unable to add links to collections', e);
                }
            }
		} else { // an entity is joining with another entity on one key/field
            try {
				Data.update({_id: fromEntity._id}, {
					$push: {
						"links": {
							"linkName": "has", // dynamic value, to be changed by user
							"linkField": linkField, // fixed value
							"linksToEntityId": toEntity._id, // fixed value
							"linksToEntityName": toEntity.entityName, // fixed value
							"foreignJoinKey": fromKey+"-"+toKey, // fixed value
                            "fromKey": fromKey, // fixed value
                            "toKey": toKey, // fixed value
							"directed": directed // fixed value, false represents undirected
						}
					}
				});
			} catch (e) {
				throw new Meteor.Error(500, 'unable to add linkTo object', e);
			}

            try {
                Data.update({_id: toEntity._id}, {
                    $push: {
                        "linkedBy": fromEntity._id
                    }
                });
            } catch (e) {
                throw new Meteor.Error(500, 'unable to add linkedBy object', e);
            }
		}

		// find out and exclude all the embedded reference fields of the joining entity
		var option = {};

		var toEntityDataLinks = Data.findOne({_id: toEntity._id}).links;

		if (toEntityDataLinks.length > 0) {
			var fields = {};

			var fieldsToExclude = toEntityDataLinks.map((l) => {
				return l.linkField;
			});

			if (fieldsToExclude.length > 0) {
				fieldsToExclude.forEach((fieldName) => {
					fields[fieldName] = 0;
				});
			}

			option.fields = fields;
		}

		// fetch the joining entity data
		var toEntityData = Mongo.Collection.get(toEntity.entityName).find({}, option).fetch();

		// create a hashmap of the joining entity for faster search
		var toEntityDataHash = toEntityData.reduce((map, obj) => {
			if (!map[obj[groupKey]]) {
				map[obj[groupKey]] = [obj];
			} else {
				map[obj[groupKey]].push(obj);
			}
			return map;
		}, {});

		// get the collection cursor
		if (!global[fromEntity.entityName]) {
			global[fromEntity.entityName] = new Mongo.Collection(fromEntity.entityName);
		}

		/* add document references, a list of documents, to a new field of each of the document
		 * in the source entity if there is an association */
		if (toEntityDataHash) {
			Object.keys(toEntityDataHash).forEach((key) => {
				try {
					global[fromEntity.entityName].update({[`${searchKey}`]: key}, {
						$set: {
							[`${linkField}`]: toEntityDataHash[key]
						}
					}, {multi: true});
				} catch (e) {
					throw new Meteor.Error(500, 'unable to add document references', e);
				}
			});
		} else {
			throw new Meteor.Error(500, 'no document reference is available');
		}
	}
});