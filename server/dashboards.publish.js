'use strict';

Meteor.publish('dashboards', function(options, searchString) {
  if(!searchString) {
    searchString = '';
  }

  let selector = {
      'name': {
          '$regex': '.*' + searchString || '' + '.*',
          '$options': 'i'
      },
      $or: [{
          $and: [{
              'public': true
          }, {
              'public': {
                  $exists: true
              }
          }]
      }, {
          $and: [{
              ownerId: this.userId
          }, {
              ownerId: {
                  $exists: true
              }
          }]
      }]
  };

  Counts.publish(this, 'numberOfDashboards', Dashboards.find(selector), {noReady: true});
  return Dashboards.find(selector, options);
});

Meteor.publish('trailDashboards', function(options, searchString, trailId) {
  if(!searchString) {
    searchString = '';
  }

  let selector = {
    'name': {
      '$regex': '.*' + searchString || '' + '.*',
      '$options': 'i'
    },
    'trailId': trailId,
    $or: [{
        $and: [{
            ownerId: this.userId
        }, {
            ownerId: {
                $exists: true
            }
        }]
    }]
  };

  Counts.publish(this, 'numberOfTrailDashboards', Dashboards.find(selector), {noReady: true});
  return Dashboards.find(selector, options);
});