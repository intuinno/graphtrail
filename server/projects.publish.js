'use strict';

Meteor.publish('projects', function(options, searchString) {
    if (!searchString) {
        searchString = '';
    }

    let selector = {
        'name': {
            '$regex': '.*' + searchString || '' + '.*',
            '$options': 'i'
        },
        $or: [{
            $and: [{
                'public': true
            }, {
                'public': {
                    $exists: true
                }
            }]
        }, {
            $and: [{
                ownerId: this.userId
            }, {
                ownerId: {
                    $exists: true
                }
            }]
        }, {
            $and: [{
                invited: this.userId
            }, {
                invited: {
                    $exists: true
                }
            }]
        }]
    };

    Counts.publish(this, 'numberOfProjects', Projects.find(selector), {
        noReady: true
    });
    
    return Projects.find(selector, options);
});
