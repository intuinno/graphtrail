'use strict';

Meteor.publish('slideshows', function(options, searchString) {
    if(!searchString) {
        searchString = '';
    }

    let selector = {
        'name': {
            '$regex': '.*' + searchString || '' + '.*',
            '$options': 'i'
        },
        $or: [{
            $and: [{
                'public': true
            }, {
                'public': {
                    $exists: true
                }
            }]
        }, {
            $and: [{
                ownerId: this.userId
            }, {
                ownerId: {
                    $exists: true
                }
            }]
        }]
    };

    Counts.publish(this, 'numberOfSlideshows', Slideshows.find(selector), {noReady: true});
    return Slideshows.find(selector, options);
});

Meteor.publish('trailSlideshows', function(options, searchString, trailId) {
    if(!searchString) {
        searchString = '';
    }

    let selector = {
        'name': {
            '$regex': '.*' + searchString || '' + '.*',
            '$options': 'i'
        },
        'trailId': trailId
    };

    Counts.publish(this, 'numberOfTrailSlideshows', Slideshows.find(selector), {noReady: true});
    return Slideshows.find(selector, options);
});
