'use strict';

Meteor.publish('trails', function(options, searchString) {
    if (!searchString) {
        searchString = '';
    }

    let selector = {
        'name': {
            '$regex': '.*' + searchString || '' + '.*',
            '$options': 'i'
        },
        $or: [{
            $and: [{
                'public': true
            }, {
                'public': {
                    $exists: true
                }
            }]
        }, {
            $and: [{
                ownerId: this.userId
            }, {
                ownerId: {
                    $exists: true
                }
            }]
        }]
    };

    Counts.publish(this, 'numberOfTrails', Trails.find(selector), {
        noReady: true
    });
    
    return Trails.find(selector, options);
});
