/* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 */
getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

/* http://stackoverflow.com/questions/9716468/is-there-any-function-like-isnumeric-in-javascript-to-validate-numbers
 */
isNumeric = (n) => {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

/* deep copy method,
http://stackoverflow.com/questions/728360/how-do-i-correctly-clone-a-javascript-object
*/
clone = (obj) => {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
};

getSample = (data, sampleSize) => {
    let size = data.length;
    let sample = [];

    if (size <= 10) {
        return data;
    } else if ((size > 10) && (size <= 100)) {
        sample = sample.concat(data.slice(0, sampleSize));
        sample = sample.concat(data.slice(size - sampleSize, size));
        return sample;
    } else if ((size > 100) && (size <= 1000)) {
        sampleSize *= sampleSize;
        sample = sample.concat(data.slice(0, sampleSize));
        sample = sample.concat(data.slice(size - sampleSize, size));
        return sample;
    } else if ((size > 1000) && (size <= 10000)) {
        sampleSize *= sampleSize * sampleSize;
        sample = sample.concat(data.slice(0, sampleSize));
        sample = sample.concat(data.slice(size - sampleSize, size));
        return sample;
    } else { // > 10000

    }
}
